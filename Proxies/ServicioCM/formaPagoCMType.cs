﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioCM.formaPagoCMType
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioCM
{
  [GeneratedCode("System.Xml", "4.6.79.0")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://ws.recaudos.esb.ccb.org.co")]
  [Serializable]
  public class formaPagoCMType : INotifyPropertyChanged
  {
    private string idFormaPagoField;
    private Decimal valorPagoField;
    private string idBancoField;
    private string idTipoTarjetaField;
    private string numDocumentoField;
    private string numAutorizaField;
    private string idMonedaField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
    public string idFormaPago
    {
      get
      {
        return this.idFormaPagoField;
      }
      set
      {
        this.idFormaPagoField = value;
        this.RaisePropertyChanged(nameof (idFormaPago));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
    public Decimal valorPago
    {
      get
      {
        return this.valorPagoField;
      }
      set
      {
        this.valorPagoField = value;
        this.RaisePropertyChanged(nameof (valorPago));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 2)]
    public string idBanco
    {
      get
      {
        return this.idBancoField;
      }
      set
      {
        this.idBancoField = value;
        this.RaisePropertyChanged(nameof (idBanco));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 3)]
    public string idTipoTarjeta
    {
      get
      {
        return this.idTipoTarjetaField;
      }
      set
      {
        this.idTipoTarjetaField = value;
        this.RaisePropertyChanged(nameof (idTipoTarjeta));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 4)]
    public string numDocumento
    {
      get
      {
        return this.numDocumentoField;
      }
      set
      {
        this.numDocumentoField = value;
        this.RaisePropertyChanged(nameof (numDocumento));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 5)]
    public string numAutoriza
    {
      get
      {
        return this.numAutorizaField;
      }
      set
      {
        this.numAutorizaField = value;
        this.RaisePropertyChanged(nameof (numAutoriza));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 6)]
    public string idMoneda
    {
      get
      {
        return this.idMonedaField;
      }
      set
      {
        this.idMonedaField = value;
        this.RaisePropertyChanged(nameof (idMoneda));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

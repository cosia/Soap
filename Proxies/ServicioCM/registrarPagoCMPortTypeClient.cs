﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioCM.registrarPagoCMPortTypeClient
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioCM
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class registrarPagoCMPortTypeClient : ClientBase<registrarPagoCMPortType>, registrarPagoCMPortType
  {
    public registrarPagoCMPortTypeClient()
    {
    }

    public registrarPagoCMPortTypeClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public registrarPagoCMPortTypeClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public registrarPagoCMPortTypeClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public registrarPagoCMPortTypeClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    registrarPagoCM_Output registrarPagoCMPortType.registrarPagoCM(registrarPagoCM_Input request)
    {
      return this.Channel.registrarPagoCM(request);
    }

    public registrarPagoCMResponseType registrarPagoCM(registrarPagoCMType registrarPagoCM1)
    {
      return ((registrarPagoCMPortType) this).registrarPagoCM(new registrarPagoCM_Input()
      {
        registrarPagoCM = registrarPagoCM1
      }).registrarPagoCMResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<registrarPagoCM_Output> registrarPagoCMPortType.registrarPagoCMAsync(registrarPagoCM_Input request)
    {
      return this.Channel.registrarPagoCMAsync(request);
    }

    public Task<registrarPagoCM_Output> registrarPagoCMAsync(registrarPagoCMType registrarPagoCM)
    {
      return ((registrarPagoCMPortType) this).registrarPagoCMAsync(new registrarPagoCM_Input()
      {
        registrarPagoCM = registrarPagoCM
      });
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioMPN.registrarPagoMPNPortTypeChannel
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioMPN
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public interface registrarPagoMPNPortTypeChannel : registrarPagoMPNPortType, IClientChannel, IContextChannel, IChannel, ICommunicationObject, IExtensibleObject<IContextChannel>, IDisposable
  {
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioMPN.registrarPagoMPNPortTypeClient
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioMPN
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class registrarPagoMPNPortTypeClient : ClientBase<registrarPagoMPNPortType>, registrarPagoMPNPortType
  {
    public registrarPagoMPNPortTypeClient()
    {
    }

    public registrarPagoMPNPortTypeClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public registrarPagoMPNPortTypeClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public registrarPagoMPNPortTypeClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public registrarPagoMPNPortTypeClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    registrarPagoMPN_Output registrarPagoMPNPortType.registrarPagoMPN(registrarPagoMPN_Input request)
    {
      return this.Channel.registrarPagoMPN(request);
    }

    public registrarPagoMPNResponseType registrarPagoMPN(registrarPagoMPNType registrarPagoMPN1)
    {
      return ((registrarPagoMPNPortType) this).registrarPagoMPN(new registrarPagoMPN_Input()
      {
        registrarPagoMPN = registrarPagoMPN1
      }).registrarPagoMPNResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<registrarPagoMPN_Output> registrarPagoMPNPortType.registrarPagoMPNAsync(registrarPagoMPN_Input request)
    {
      return this.Channel.registrarPagoMPNAsync(request);
    }

    public Task<registrarPagoMPN_Output> registrarPagoMPNAsync(registrarPagoMPNType registrarPagoMPN)
    {
      return ((registrarPagoMPNPortType) this).registrarPagoMPNAsync(new registrarPagoMPN_Input()
      {
        registrarPagoMPN = registrarPagoMPN
      });
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioMPN.registrarPagoMPNPortType
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ServiceModel;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioMPN
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [ServiceContract(ConfigurationName = "Proxies.ServicioMPN.registrarPagoMPNPortType", Namespace = "http://ws.recaudos.esb.ccb.org.co")]
  public interface registrarPagoMPNPortType
  {
    [OperationContract(Action = "", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    registrarPagoMPN_Output registrarPagoMPN(registrarPagoMPN_Input request);

    [OperationContract(Action = "", ReplyAction = "*")]
    Task<registrarPagoMPN_Output> registrarPagoMPNAsync(registrarPagoMPN_Input request);
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioVigencia.ObtenerFechaVigenciaSEI
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ServiceModel;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioVigencia
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [ServiceContract(ConfigurationName = "Proxies.ServicioVigencia.ObtenerFechaVigenciaSEI", Namespace = "http://ws.pago.orden.vigencia.fecha.obtener.recaudos.sirep2.ccb.org.co")]
  public interface ObtenerFechaVigenciaSEI
  {
    [OperationContract(Action = "obtenerFechaVigenciaOrdenPago", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    obtenerFechaVigenciaResponse obtenerFechaVigenciaOrdenPago(obtenerFechaVigenciaRequest request);

    [OperationContract(Action = "obtenerFechaVigenciaOrdenPago", ReplyAction = "*")]
    Task<obtenerFechaVigenciaResponse> obtenerFechaVigenciaOrdenPagoAsync(obtenerFechaVigenciaRequest request);
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioVigencia.obtenerFechaVigenciaResponse
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioVigencia
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [MessageContract(IsWrapped = true, WrapperName = "obtenerFechaVigenciaResponse", WrapperNamespace = "http://model.pago.orden.vigencia.fecha.obtener.www.ccb.org.co")]
  public class obtenerFechaVigenciaResponse
  {
    [MessageBodyMember(Namespace = "http://model.pago.orden.vigencia.fecha.obtener.www.ccb.org.co", Order = 0)]
    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string fecVigencia;
    [MessageBodyMember(Namespace = "http://model.pago.orden.vigencia.fecha.obtener.www.ccb.org.co", Order = 1)]
    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string codigoError;
    [MessageBodyMember(Namespace = "http://model.pago.orden.vigencia.fecha.obtener.www.ccb.org.co", Order = 2)]
    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string mensajeError;

    public obtenerFechaVigenciaResponse()
    {
    }

    public obtenerFechaVigenciaResponse(string fecVigencia, string codigoError, string mensajeError)
    {
      this.fecVigencia = fecVigencia;
      this.codigoError = codigoError;
      this.mensajeError = mensajeError;
    }
  }
}

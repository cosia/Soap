﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioVigencia.obtenerFechaVigenciaRequest
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioVigencia
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [MessageContract(IsWrapped = true, WrapperName = "obtenerFechaVigencia", WrapperNamespace = "http://model.pago.orden.vigencia.fecha.obtener.www.ccb.org.co")]
  public class obtenerFechaVigenciaRequest
  {
    [MessageBodyMember(Namespace = "http://model.pago.orden.vigencia.fecha.obtener.www.ccb.org.co", Order = 0)]
    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string idServicioNegocio;
    [MessageBodyMember(Namespace = "http://model.pago.orden.vigencia.fecha.obtener.www.ccb.org.co", Order = 1)]
    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string fecDocumento;
    [MessageBodyMember(Namespace = "http://model.pago.orden.vigencia.fecha.obtener.www.ccb.org.co", Order = 2)]
    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ctrEspecial;
    [MessageBodyMember(Namespace = "http://model.pago.orden.vigencia.fecha.obtener.www.ccb.org.co", Order = 3)]
    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string idOrigenDocumento;
    [MessageBodyMember(Namespace = "http://model.pago.orden.vigencia.fecha.obtener.www.ccb.org.co", Order = 4)]
    [XmlElement("actos", Form = XmlSchemaForm.Unqualified)]
    public ActoDTO[] actos;

    public obtenerFechaVigenciaRequest()
    {
    }

    public obtenerFechaVigenciaRequest(string idServicioNegocio, string fecDocumento, string ctrEspecial, string idOrigenDocumento, ActoDTO[] actos)
    {
      this.idServicioNegocio = idServicioNegocio;
      this.fecDocumento = fecDocumento;
      this.ctrEspecial = ctrEspecial;
      this.idOrigenDocumento = idOrigenDocumento;
      this.actos = actos;
    }
  }
}

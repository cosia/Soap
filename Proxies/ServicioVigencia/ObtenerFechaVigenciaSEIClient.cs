﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioVigencia.ObtenerFechaVigenciaSEIClient
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioVigencia
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class ObtenerFechaVigenciaSEIClient : ClientBase<ObtenerFechaVigenciaSEI>, ObtenerFechaVigenciaSEI
  {
    public ObtenerFechaVigenciaSEIClient()
    {
    }

    public ObtenerFechaVigenciaSEIClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public ObtenerFechaVigenciaSEIClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public ObtenerFechaVigenciaSEIClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public ObtenerFechaVigenciaSEIClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    public obtenerFechaVigenciaResponse obtenerFechaVigenciaOrdenPago(obtenerFechaVigenciaRequest request)
    {
      return this.Channel.obtenerFechaVigenciaOrdenPago(request);
    }

    public Task<obtenerFechaVigenciaResponse> obtenerFechaVigenciaOrdenPagoAsync(obtenerFechaVigenciaRequest request)
    {
      return this.Channel.obtenerFechaVigenciaOrdenPagoAsync(request);
    }
  }
}

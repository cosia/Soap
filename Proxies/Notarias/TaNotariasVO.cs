﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.TaNotariasVO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://ta.model.common.sirep2.ccb.org.co")]
  [Serializable]
  public class TaNotariasVO : INotifyPropertyChanged
  {
    private string idOrigenField;
    private string idMunicipioField;
    private string nitNotariaField;
    private string dirNotariaField;
    private string numTelField;
    private int? numClienteField;

    [SoapElement(IsNullable = true)]
    public string idOrigen
    {
      get
      {
        return this.idOrigenField;
      }
      set
      {
        this.idOrigenField = value;
        this.RaisePropertyChanged(nameof (idOrigen));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idMunicipio
    {
      get
      {
        return this.idMunicipioField;
      }
      set
      {
        this.idMunicipioField = value;
        this.RaisePropertyChanged(nameof (idMunicipio));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nitNotaria
    {
      get
      {
        return this.nitNotariaField;
      }
      set
      {
        this.nitNotariaField = value;
        this.RaisePropertyChanged(nameof (nitNotaria));
      }
    }

    [SoapElement(IsNullable = true)]
    public string dirNotaria
    {
      get
      {
        return this.dirNotariaField;
      }
      set
      {
        this.dirNotariaField = value;
        this.RaisePropertyChanged(nameof (dirNotaria));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numTel
    {
      get
      {
        return this.numTelField;
      }
      set
      {
        this.numTelField = value;
        this.RaisePropertyChanged(nameof (numTel));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? numCliente
    {
      get
      {
        return this.numClienteField;
      }
      set
      {
        this.numClienteField = value;
        this.RaisePropertyChanged(nameof (numCliente));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

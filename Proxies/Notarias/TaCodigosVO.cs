﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.TaCodigosVO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://ta.model.common.sirep2.ccb.org.co")]
  [Serializable]
  public class TaCodigosVO : INotifyPropertyChanged
  {
    private string idCodigoField;
    private string nomCodigoField;
    private int? ctrVigenteField;
    private string idTablaField;
    private string idCodigoCortoField;
    private string claseJavaField;

    [SoapElement(IsNullable = true)]
    public string idCodigo
    {
      get
      {
        return this.idCodigoField;
      }
      set
      {
        this.idCodigoField = value;
        this.RaisePropertyChanged(nameof (idCodigo));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nomCodigo
    {
      get
      {
        return this.nomCodigoField;
      }
      set
      {
        this.nomCodigoField = value;
        this.RaisePropertyChanged(nameof (nomCodigo));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrVigente
    {
      get
      {
        return this.ctrVigenteField;
      }
      set
      {
        this.ctrVigenteField = value;
        this.RaisePropertyChanged(nameof (ctrVigente));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idTabla
    {
      get
      {
        return this.idTablaField;
      }
      set
      {
        this.idTablaField = value;
        this.RaisePropertyChanged(nameof (idTabla));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idCodigoCorto
    {
      get
      {
        return this.idCodigoCortoField;
      }
      set
      {
        this.idCodigoCortoField = value;
        this.RaisePropertyChanged(nameof (idCodigoCorto));
      }
    }

    [SoapElement(IsNullable = true)]
    public string claseJava
    {
      get
      {
        return this.claseJavaField;
      }
      set
      {
        this.claseJavaField = value;
        this.RaisePropertyChanged(nameof (claseJava));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

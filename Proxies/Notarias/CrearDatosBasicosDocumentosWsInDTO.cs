﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.CrearDatosBasicosDocumentosWsInDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.not.is.sirep2.ccb.org.co")]
  [Serializable]
  public class CrearDatosBasicosDocumentosWsInDTO : INotifyPropertyChanged
  {
    private string idRequisitoDIANField;
    private int? ctrInscripcionField;
    private int? idImpuestoField;
    private string idOrigenDocField;
    private string idTipoDocField;
    private string numMatriculaField;
    private string numDocField;
    private DateTime? fecDocField;
    private string idMunicipioDocField;
    private int? cntEstablecimientosField;
    private double? vlrActivosField;
    private string idActoField;
    private string idServicioField;
    private string idLibroField;
    private string descActoField;
    private EstablecimientoDTO[] establecimientosField;
    private double? cuantiaField;

    [SoapElement(IsNullable = true)]
    public string idRequisitoDIAN
    {
      get
      {
        return this.idRequisitoDIANField;
      }
      set
      {
        this.idRequisitoDIANField = value;
        this.RaisePropertyChanged(nameof (idRequisitoDIAN));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrInscripcion
    {
      get
      {
        return this.ctrInscripcionField;
      }
      set
      {
        this.ctrInscripcionField = value;
        this.RaisePropertyChanged(nameof (ctrInscripcion));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? idImpuesto
    {
      get
      {
        return this.idImpuestoField;
      }
      set
      {
        this.idImpuestoField = value;
        this.RaisePropertyChanged(nameof (idImpuesto));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idOrigenDoc
    {
      get
      {
        return this.idOrigenDocField;
      }
      set
      {
        this.idOrigenDocField = value;
        this.RaisePropertyChanged(nameof (idOrigenDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idTipoDoc
    {
      get
      {
        return this.idTipoDocField;
      }
      set
      {
        this.idTipoDocField = value;
        this.RaisePropertyChanged(nameof (idTipoDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numMatricula
    {
      get
      {
        return this.numMatriculaField;
      }
      set
      {
        this.numMatriculaField = value;
        this.RaisePropertyChanged(nameof (numMatricula));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numDoc
    {
      get
      {
        return this.numDocField;
      }
      set
      {
        this.numDocField = value;
        this.RaisePropertyChanged(nameof (numDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public DateTime? fecDoc
    {
      get
      {
        return this.fecDocField;
      }
      set
      {
        this.fecDocField = value;
        this.RaisePropertyChanged(nameof (fecDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idMunicipioDoc
    {
      get
      {
        return this.idMunicipioDocField;
      }
      set
      {
        this.idMunicipioDocField = value;
        this.RaisePropertyChanged(nameof (idMunicipioDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? cntEstablecimientos
    {
      get
      {
        return this.cntEstablecimientosField;
      }
      set
      {
        this.cntEstablecimientosField = value;
        this.RaisePropertyChanged(nameof (cntEstablecimientos));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? vlrActivos
    {
      get
      {
        return this.vlrActivosField;
      }
      set
      {
        this.vlrActivosField = value;
        this.RaisePropertyChanged(nameof (vlrActivos));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idActo
    {
      get
      {
        return this.idActoField;
      }
      set
      {
        this.idActoField = value;
        this.RaisePropertyChanged(nameof (idActo));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idServicio
    {
      get
      {
        return this.idServicioField;
      }
      set
      {
        this.idServicioField = value;
        this.RaisePropertyChanged(nameof (idServicio));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idLibro
    {
      get
      {
        return this.idLibroField;
      }
      set
      {
        this.idLibroField = value;
        this.RaisePropertyChanged(nameof (idLibro));
      }
    }

    [SoapElement(IsNullable = true)]
    public string descActo
    {
      get
      {
        return this.descActoField;
      }
      set
      {
        this.descActoField = value;
        this.RaisePropertyChanged(nameof (descActo));
      }
    }

    [SoapElement(IsNullable = true)]
    public EstablecimientoDTO[] establecimientos
    {
      get
      {
        return this.establecimientosField;
      }
      set
      {
        this.establecimientosField = value;
        this.RaisePropertyChanged(nameof (establecimientos));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? cuantia
    {
      get
      {
        return this.cuantiaField;
      }
      set
      {
        this.cuantiaField = value;
        this.RaisePropertyChanged(nameof (cuantia));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

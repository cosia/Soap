﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.RmDireccionesVO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://rm.model.common.sirep2.ccb.org.co")]
  [Serializable]
  public class RmDireccionesVO : INotifyPropertyChanged
  {
    private int? numClienteField;
    private string idTipoDirField;
    private string direccionField;
    private string idMunipField;
    private string idPaisField;
    private string idZonaPostalField;
    private string idZonaGeogrField;
    private string numTel1Field;
    private string numTel2Field;
    private string numFaxField;
    private string numAaField;
    private string emailField;
    private string dirUrlField;
    private string numMovilField;
    private int? ctrMensajesField;
    private string barrioTextualField;
    private string idBarrioField;

    [SoapElement(IsNullable = true)]
    public int? numCliente
    {
      get
      {
        return this.numClienteField;
      }
      set
      {
        this.numClienteField = value;
        this.RaisePropertyChanged(nameof (numCliente));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idTipoDir
    {
      get
      {
        return this.idTipoDirField;
      }
      set
      {
        this.idTipoDirField = value;
        this.RaisePropertyChanged(nameof (idTipoDir));
      }
    }

    [SoapElement(IsNullable = true)]
    public string direccion
    {
      get
      {
        return this.direccionField;
      }
      set
      {
        this.direccionField = value;
        this.RaisePropertyChanged(nameof (direccion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idMunip
    {
      get
      {
        return this.idMunipField;
      }
      set
      {
        this.idMunipField = value;
        this.RaisePropertyChanged(nameof (idMunip));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idPais
    {
      get
      {
        return this.idPaisField;
      }
      set
      {
        this.idPaisField = value;
        this.RaisePropertyChanged(nameof (idPais));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idZonaPostal
    {
      get
      {
        return this.idZonaPostalField;
      }
      set
      {
        this.idZonaPostalField = value;
        this.RaisePropertyChanged(nameof (idZonaPostal));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idZonaGeogr
    {
      get
      {
        return this.idZonaGeogrField;
      }
      set
      {
        this.idZonaGeogrField = value;
        this.RaisePropertyChanged(nameof (idZonaGeogr));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numTel1
    {
      get
      {
        return this.numTel1Field;
      }
      set
      {
        this.numTel1Field = value;
        this.RaisePropertyChanged(nameof (numTel1));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numTel2
    {
      get
      {
        return this.numTel2Field;
      }
      set
      {
        this.numTel2Field = value;
        this.RaisePropertyChanged(nameof (numTel2));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numFax
    {
      get
      {
        return this.numFaxField;
      }
      set
      {
        this.numFaxField = value;
        this.RaisePropertyChanged(nameof (numFax));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numAa
    {
      get
      {
        return this.numAaField;
      }
      set
      {
        this.numAaField = value;
        this.RaisePropertyChanged(nameof (numAa));
      }
    }

    [SoapElement(IsNullable = true)]
    public string email
    {
      get
      {
        return this.emailField;
      }
      set
      {
        this.emailField = value;
        this.RaisePropertyChanged(nameof (email));
      }
    }

    [SoapElement(IsNullable = true)]
    public string dirUrl
    {
      get
      {
        return this.dirUrlField;
      }
      set
      {
        this.dirUrlField = value;
        this.RaisePropertyChanged(nameof (dirUrl));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numMovil
    {
      get
      {
        return this.numMovilField;
      }
      set
      {
        this.numMovilField = value;
        this.RaisePropertyChanged(nameof (numMovil));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrMensajes
    {
      get
      {
        return this.ctrMensajesField;
      }
      set
      {
        this.ctrMensajesField = value;
        this.RaisePropertyChanged(nameof (ctrMensajes));
      }
    }

    [SoapElement(IsNullable = true)]
    public string barrioTextual
    {
      get
      {
        return this.barrioTextualField;
      }
      set
      {
        this.barrioTextualField = value;
        this.RaisePropertyChanged(nameof (barrioTextual));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idBarrio
    {
      get
      {
        return this.idBarrioField;
      }
      set
      {
        this.idBarrioField = value;
        this.RaisePropertyChanged(nameof (idBarrio));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

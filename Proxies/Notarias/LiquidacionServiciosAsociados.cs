﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.LiquidacionServiciosAsociados
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.id.sirep2.ccb.org.co")]
  [Serializable]
  public class LiquidacionServiciosAsociados : INotifyPropertyChanged
  {
    private string idServicioField;
    private double? valorServicioField;
    private int? itemField;
    private int? cantidadServiciosField;
    private int? idImpuestoField;
    private double? vrActivosField;
    private double? vrCuantiaField;

    [SoapElement(IsNullable = true)]
    public string idServicio
    {
      get
      {
        return this.idServicioField;
      }
      set
      {
        this.idServicioField = value;
        this.RaisePropertyChanged(nameof (idServicio));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? valorServicio
    {
      get
      {
        return this.valorServicioField;
      }
      set
      {
        this.valorServicioField = value;
        this.RaisePropertyChanged(nameof (valorServicio));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? item
    {
      get
      {
        return this.itemField;
      }
      set
      {
        this.itemField = value;
        this.RaisePropertyChanged(nameof (item));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? cantidadServicios
    {
      get
      {
        return this.cantidadServiciosField;
      }
      set
      {
        this.cantidadServiciosField = value;
        this.RaisePropertyChanged(nameof (cantidadServicios));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? idImpuesto
    {
      get
      {
        return this.idImpuestoField;
      }
      set
      {
        this.idImpuestoField = value;
        this.RaisePropertyChanged(nameof (idImpuesto));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? vrActivos
    {
      get
      {
        return this.vrActivosField;
      }
      set
      {
        this.vrActivosField = value;
        this.RaisePropertyChanged(nameof (vrActivos));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? vrCuantia
    {
      get
      {
        return this.vrCuantiaField;
      }
      set
      {
        this.vrCuantiaField = value;
        this.RaisePropertyChanged(nameof (vrCuantia));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

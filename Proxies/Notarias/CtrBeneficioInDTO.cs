﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.CtrBeneficioInDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.cs.sirep2.ccb.org.co")]
  [Serializable]
  public class CtrBeneficioInDTO : INotifyPropertyChanged
  {
    private ValidarCiiuOutDTO[] ciiusField;
    private bool? seAcogeField;
    private string nombreField;
    private string tipoIdField;
    private string numIdField;
    private string servicioNegocioField;
    private string idLibroField;
    private string idActoField;
    private double? activosField;
    private int? numEmpleadosField;

    [SoapElement(IsNullable = true)]
    public ValidarCiiuOutDTO[] ciius
    {
      get
      {
        return this.ciiusField;
      }
      set
      {
        this.ciiusField = value;
        this.RaisePropertyChanged(nameof (ciius));
      }
    }

    [SoapElement(IsNullable = true)]
    public bool? seAcoge
    {
      get
      {
        return this.seAcogeField;
      }
      set
      {
        this.seAcogeField = value;
        this.RaisePropertyChanged(nameof (seAcoge));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nombre
    {
      get
      {
        return this.nombreField;
      }
      set
      {
        this.nombreField = value;
        this.RaisePropertyChanged(nameof (nombre));
      }
    }

    [SoapElement(IsNullable = true)]
    public string tipoId
    {
      get
      {
        return this.tipoIdField;
      }
      set
      {
        this.tipoIdField = value;
        this.RaisePropertyChanged(nameof (tipoId));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numId
    {
      get
      {
        return this.numIdField;
      }
      set
      {
        this.numIdField = value;
        this.RaisePropertyChanged(nameof (numId));
      }
    }

    [SoapElement(IsNullable = true)]
    public string servicioNegocio
    {
      get
      {
        return this.servicioNegocioField;
      }
      set
      {
        this.servicioNegocioField = value;
        this.RaisePropertyChanged(nameof (servicioNegocio));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idLibro
    {
      get
      {
        return this.idLibroField;
      }
      set
      {
        this.idLibroField = value;
        this.RaisePropertyChanged(nameof (idLibro));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idActo
    {
      get
      {
        return this.idActoField;
      }
      set
      {
        this.idActoField = value;
        this.RaisePropertyChanged(nameof (idActo));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? activos
    {
      get
      {
        return this.activosField;
      }
      set
      {
        this.activosField = value;
        this.RaisePropertyChanged(nameof (activos));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? numEmpleados
    {
      get
      {
        return this.numEmpleadosField;
      }
      set
      {
        this.numEmpleadosField = value;
        this.RaisePropertyChanged(nameof (numEmpleados));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

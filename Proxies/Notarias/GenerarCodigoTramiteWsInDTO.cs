﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.GenerarCodigoTramiteWsInDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.not.is.sirep2.ccb.org.co")]
  [Serializable]
  public class GenerarCodigoTramiteWsInDTO : INotifyPropertyChanged
  {
    private int? cntServiciosField;
    private int? ctrReliquidacionField;
    private string idServicioField;
    private double? vlrServicioField;
    private int? ctrInscripcionField;
    private string idOrigenDocField;
    private string idTipoDocField;
    private string numMatriculaField;
    private string numDocField;
    private DateTime? fecDocField;
    private string idMunicipioDocField;
    private int? cntEstablecimientosField;
    private double? vlrActivosField;
    private double? vlrCuantiaField;
    private double? vlrBaseLiquidacionField;
    private string idActoField;
    private string idLibroField;
    private string idServicioNegocioField;
    private LiquidacionServiciosAsociados[] liquidacionEstablecimientosField;

    [SoapElement(IsNullable = true)]
    public int? cntServicios
    {
      get
      {
        return this.cntServiciosField;
      }
      set
      {
        this.cntServiciosField = value;
        this.RaisePropertyChanged(nameof (cntServicios));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrReliquidacion
    {
      get
      {
        return this.ctrReliquidacionField;
      }
      set
      {
        this.ctrReliquidacionField = value;
        this.RaisePropertyChanged(nameof (ctrReliquidacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idServicio
    {
      get
      {
        return this.idServicioField;
      }
      set
      {
        this.idServicioField = value;
        this.RaisePropertyChanged(nameof (idServicio));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? vlrServicio
    {
      get
      {
        return this.vlrServicioField;
      }
      set
      {
        this.vlrServicioField = value;
        this.RaisePropertyChanged(nameof (vlrServicio));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrInscripcion
    {
      get
      {
        return this.ctrInscripcionField;
      }
      set
      {
        this.ctrInscripcionField = value;
        this.RaisePropertyChanged(nameof (ctrInscripcion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idOrigenDoc
    {
      get
      {
        return this.idOrigenDocField;
      }
      set
      {
        this.idOrigenDocField = value;
        this.RaisePropertyChanged(nameof (idOrigenDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idTipoDoc
    {
      get
      {
        return this.idTipoDocField;
      }
      set
      {
        this.idTipoDocField = value;
        this.RaisePropertyChanged(nameof (idTipoDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numMatricula
    {
      get
      {
        return this.numMatriculaField;
      }
      set
      {
        this.numMatriculaField = value;
        this.RaisePropertyChanged(nameof (numMatricula));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numDoc
    {
      get
      {
        return this.numDocField;
      }
      set
      {
        this.numDocField = value;
        this.RaisePropertyChanged(nameof (numDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public DateTime? fecDoc
    {
      get
      {
        return this.fecDocField;
      }
      set
      {
        this.fecDocField = value;
        this.RaisePropertyChanged(nameof (fecDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idMunicipioDoc
    {
      get
      {
        return this.idMunicipioDocField;
      }
      set
      {
        this.idMunicipioDocField = value;
        this.RaisePropertyChanged(nameof (idMunicipioDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? cntEstablecimientos
    {
      get
      {
        return this.cntEstablecimientosField;
      }
      set
      {
        this.cntEstablecimientosField = value;
        this.RaisePropertyChanged(nameof (cntEstablecimientos));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? vlrActivos
    {
      get
      {
        return this.vlrActivosField;
      }
      set
      {
        this.vlrActivosField = value;
        this.RaisePropertyChanged(nameof (vlrActivos));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? vlrCuantia
    {
      get
      {
        return this.vlrCuantiaField;
      }
      set
      {
        this.vlrCuantiaField = value;
        this.RaisePropertyChanged(nameof (vlrCuantia));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? vlrBaseLiquidacion
    {
      get
      {
        return this.vlrBaseLiquidacionField;
      }
      set
      {
        this.vlrBaseLiquidacionField = value;
        this.RaisePropertyChanged(nameof (vlrBaseLiquidacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idActo
    {
      get
      {
        return this.idActoField;
      }
      set
      {
        this.idActoField = value;
        this.RaisePropertyChanged(nameof (idActo));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idLibro
    {
      get
      {
        return this.idLibroField;
      }
      set
      {
        this.idLibroField = value;
        this.RaisePropertyChanged(nameof (idLibro));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idServicioNegocio
    {
      get
      {
        return this.idServicioNegocioField;
      }
      set
      {
        this.idServicioNegocioField = value;
        this.RaisePropertyChanged(nameof (idServicioNegocio));
      }
    }

    [SoapElement(IsNullable = true)]
    public LiquidacionServiciosAsociados[] liquidacionEstablecimientos
    {
      get
      {
        return this.liquidacionEstablecimientosField;
      }
      set
      {
        this.liquidacionEstablecimientosField = value;
        this.RaisePropertyChanged(nameof (liquidacionEstablecimientos));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

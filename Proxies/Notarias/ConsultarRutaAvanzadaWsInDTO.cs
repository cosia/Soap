﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.ConsultarRutaAvanzadaWsInDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.not.is.sirep2.ccb.org.co")]
  [Serializable]
  public class ConsultarRutaAvanzadaWsInDTO : INotifyPropertyChanged
  {
    private DateTime? fecIniField;
    private DateTime? fecFinField;
    private string ctrCoberturaField;
    private string idSucursalField;
    private string idServicioNegocioField;
    private string idUsuarioAsigField;
    private string idOrigenDocField;
    private string idTipoDocField;
    private string idEstadoField;

    [SoapElement(IsNullable = true)]
    public DateTime? fecIni
    {
      get
      {
        return this.fecIniField;
      }
      set
      {
        this.fecIniField = value;
        this.RaisePropertyChanged(nameof (fecIni));
      }
    }

    [SoapElement(IsNullable = true)]
    public DateTime? fecFin
    {
      get
      {
        return this.fecFinField;
      }
      set
      {
        this.fecFinField = value;
        this.RaisePropertyChanged(nameof (fecFin));
      }
    }

    [SoapElement(IsNullable = true)]
    public string ctrCobertura
    {
      get
      {
        return this.ctrCoberturaField;
      }
      set
      {
        this.ctrCoberturaField = value;
        this.RaisePropertyChanged(nameof (ctrCobertura));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idSucursal
    {
      get
      {
        return this.idSucursalField;
      }
      set
      {
        this.idSucursalField = value;
        this.RaisePropertyChanged(nameof (idSucursal));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idServicioNegocio
    {
      get
      {
        return this.idServicioNegocioField;
      }
      set
      {
        this.idServicioNegocioField = value;
        this.RaisePropertyChanged(nameof (idServicioNegocio));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idUsuarioAsig
    {
      get
      {
        return this.idUsuarioAsigField;
      }
      set
      {
        this.idUsuarioAsigField = value;
        this.RaisePropertyChanged(nameof (idUsuarioAsig));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idOrigenDoc
    {
      get
      {
        return this.idOrigenDocField;
      }
      set
      {
        this.idOrigenDocField = value;
        this.RaisePropertyChanged(nameof (idOrigenDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idTipoDoc
    {
      get
      {
        return this.idTipoDocField;
      }
      set
      {
        this.idTipoDocField = value;
        this.RaisePropertyChanged(nameof (idTipoDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idEstado
    {
      get
      {
        return this.idEstadoField;
      }
      set
      {
        this.idEstadoField = value;
        this.RaisePropertyChanged(nameof (idEstado));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

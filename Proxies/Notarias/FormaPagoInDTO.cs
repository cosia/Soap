﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.FormaPagoInDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.in.sirep2.ccb.org.co")]
  [Serializable]
  public class FormaPagoInDTO : INotifyPropertyChanged
  {
    private string fechaConsignacionField;
    private string idBancoField;
    private string idFormaPagoField;
    private string idMonedaField;
    private string idTipoTarjetaField;
    private string nombreBancoField;
    private string nombreCortoFormaPagoField;
    private string nombreFormaPagoField;
    private string nombreMonedaField;
    private string nombreTipoTarjetaField;
    private string numAutorizaField;
    private string numDocumentoField;
    private double? vrPagoField;
    private double? vrSaldoField;
    private double? vrDevolucionField;
    private double? vrBaseField;
    private double? vrIvaField;
    private string reciboRBMField;
    private string numeroRRNField;
    private double? porcPagadoField;
    private int? itemField;

    [SoapElement(IsNullable = true)]
    public string fechaConsignacion
    {
      get
      {
        return this.fechaConsignacionField;
      }
      set
      {
        this.fechaConsignacionField = value;
        this.RaisePropertyChanged(nameof (fechaConsignacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idBanco
    {
      get
      {
        return this.idBancoField;
      }
      set
      {
        this.idBancoField = value;
        this.RaisePropertyChanged(nameof (idBanco));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idFormaPago
    {
      get
      {
        return this.idFormaPagoField;
      }
      set
      {
        this.idFormaPagoField = value;
        this.RaisePropertyChanged(nameof (idFormaPago));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idMoneda
    {
      get
      {
        return this.idMonedaField;
      }
      set
      {
        this.idMonedaField = value;
        this.RaisePropertyChanged(nameof (idMoneda));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idTipoTarjeta
    {
      get
      {
        return this.idTipoTarjetaField;
      }
      set
      {
        this.idTipoTarjetaField = value;
        this.RaisePropertyChanged(nameof (idTipoTarjeta));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nombreBanco
    {
      get
      {
        return this.nombreBancoField;
      }
      set
      {
        this.nombreBancoField = value;
        this.RaisePropertyChanged(nameof (nombreBanco));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nombreCortoFormaPago
    {
      get
      {
        return this.nombreCortoFormaPagoField;
      }
      set
      {
        this.nombreCortoFormaPagoField = value;
        this.RaisePropertyChanged(nameof (nombreCortoFormaPago));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nombreFormaPago
    {
      get
      {
        return this.nombreFormaPagoField;
      }
      set
      {
        this.nombreFormaPagoField = value;
        this.RaisePropertyChanged(nameof (nombreFormaPago));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nombreMoneda
    {
      get
      {
        return this.nombreMonedaField;
      }
      set
      {
        this.nombreMonedaField = value;
        this.RaisePropertyChanged(nameof (nombreMoneda));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nombreTipoTarjeta
    {
      get
      {
        return this.nombreTipoTarjetaField;
      }
      set
      {
        this.nombreTipoTarjetaField = value;
        this.RaisePropertyChanged(nameof (nombreTipoTarjeta));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numAutoriza
    {
      get
      {
        return this.numAutorizaField;
      }
      set
      {
        this.numAutorizaField = value;
        this.RaisePropertyChanged(nameof (numAutoriza));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numDocumento
    {
      get
      {
        return this.numDocumentoField;
      }
      set
      {
        this.numDocumentoField = value;
        this.RaisePropertyChanged(nameof (numDocumento));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? vrPago
    {
      get
      {
        return this.vrPagoField;
      }
      set
      {
        this.vrPagoField = value;
        this.RaisePropertyChanged(nameof (vrPago));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? vrSaldo
    {
      get
      {
        return this.vrSaldoField;
      }
      set
      {
        this.vrSaldoField = value;
        this.RaisePropertyChanged(nameof (vrSaldo));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? vrDevolucion
    {
      get
      {
        return this.vrDevolucionField;
      }
      set
      {
        this.vrDevolucionField = value;
        this.RaisePropertyChanged(nameof (vrDevolucion));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? vrBase
    {
      get
      {
        return this.vrBaseField;
      }
      set
      {
        this.vrBaseField = value;
        this.RaisePropertyChanged(nameof (vrBase));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? vrIva
    {
      get
      {
        return this.vrIvaField;
      }
      set
      {
        this.vrIvaField = value;
        this.RaisePropertyChanged(nameof (vrIva));
      }
    }

    [SoapElement(IsNullable = true)]
    public string reciboRBM
    {
      get
      {
        return this.reciboRBMField;
      }
      set
      {
        this.reciboRBMField = value;
        this.RaisePropertyChanged(nameof (reciboRBM));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numeroRRN
    {
      get
      {
        return this.numeroRRNField;
      }
      set
      {
        this.numeroRRNField = value;
        this.RaisePropertyChanged(nameof (numeroRRN));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? porcPagado
    {
      get
      {
        return this.porcPagadoField;
      }
      set
      {
        this.porcPagadoField = value;
        this.RaisePropertyChanged(nameof (porcPagado));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? item
    {
      get
      {
        return this.itemField;
      }
      set
      {
        this.itemField = value;
        this.RaisePropertyChanged(nameof (item));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

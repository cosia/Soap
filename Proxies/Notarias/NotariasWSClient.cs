﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.NotariasWSClient
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class NotariasWSClient : ClientBase<NotariasWS>, NotariasWS
  {
    public NotariasWSClient()
    {
    }

    public NotariasWSClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public NotariasWSClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public NotariasWSClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public NotariasWSClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    public TaLibrosComercioVO[] findAllTaLibrosComercio()
    {
      return this.Channel.findAllTaLibrosComercio();
    }

    public Task<TaLibrosComercioVO[]> findAllTaLibrosComercioAsync()
    {
      return this.Channel.findAllTaLibrosComercioAsync();
    }

    public TaCodigosVO findTaCodigosById(string idCodigo)
    {
      return this.Channel.findTaCodigosById(idCodigo);
    }

    public Task<TaCodigosVO> findTaCodigosByIdAsync(string idCodigo)
    {
      return this.Channel.findTaCodigosByIdAsync(idCodigo);
    }

    public ResultadoDTO crearDatosBasicosCliente(DatosBasicosClienteDTO datosBasicosClienteDTO)
    {
      return this.Channel.crearDatosBasicosCliente(datosBasicosClienteDTO);
    }

    public Task<ResultadoDTO> crearDatosBasicosClienteAsync(DatosBasicosClienteDTO datosBasicosClienteDTO)
    {
      return this.Channel.crearDatosBasicosClienteAsync(datosBasicosClienteDTO);
    }

    public string abrirCajaInicioDia(string idUsuario)
    {
      return this.Channel.abrirCajaInicioDia(idUsuario);
    }

    public Task<string> abrirCajaInicioDiaAsync(string idUsuario)
    {
      return this.Channel.abrirCajaInicioDiaAsync(idUsuario);
    }

    public TaCodigosVO[] findAllTaCodigos()
    {
      return this.Channel.findAllTaCodigos();
    }

    public Task<TaCodigosVO[]> findAllTaCodigosAsync()
    {
      return this.Channel.findAllTaCodigosAsync();
    }

    public bool reingresosNotarias(string numTramite)
    {
      return this.Channel.reingresosNotarias(numTramite);
    }

    public Task<bool> reingresosNotariasAsync(string numTramite)
    {
      return this.Channel.reingresosNotariasAsync(numTramite);
    }

    public ConsultarRutaWsOutDTO[] consultarRuta(ConsultarRutaInDTO consultarRutaInDTO)
    {
      return this.Channel.consultarRuta(consultarRutaInDTO);
    }

    public Task<ConsultarRutaWsOutDTO[]> consultarRutaAsync(ConsultarRutaInDTO consultarRutaInDTO)
    {
      return this.Channel.consultarRutaAsync(consultarRutaInDTO);
    }

    public string generarCodigoTramite(GenerarCodigoTramiteWsInDTO generarCodigoTramiteInDTO)
    {
      return this.Channel.generarCodigoTramite(generarCodigoTramiteInDTO);
    }

    public Task<string> generarCodigoTramiteAsync(GenerarCodigoTramiteWsInDTO generarCodigoTramiteInDTO)
    {
      return this.Channel.generarCodigoTramiteAsync(generarCodigoTramiteInDTO);
    }

    public ConsultarTramiteWsOutDTO consultarTramite(string numTramite)
    {
      return this.Channel.consultarTramite(numTramite);
    }

    public Task<ConsultarTramiteWsOutDTO> consultarTramiteAsync(string numTramite)
    {
      return this.Channel.consultarTramiteAsync(numTramite);
    }

    public DatosBasicosClienteDTO consultarDatosBasicosCliente(ConsultarDatosBasicosClienteInDTO consultarDatosBasicosClienteInDTO)
    {
      return this.Channel.consultarDatosBasicosCliente(consultarDatosBasicosClienteInDTO);
    }

    public Task<DatosBasicosClienteDTO> consultarDatosBasicosClienteAsync(ConsultarDatosBasicosClienteInDTO consultarDatosBasicosClienteInDTO)
    {
      return this.Channel.consultarDatosBasicosClienteAsync(consultarDatosBasicosClienteInDTO);
    }

    public LiquidarWsOutDTO liquidarServicios(LiquidarWsInDTO liquidarWsInDTO)
    {
      return this.Channel.liquidarServicios(liquidarWsInDTO);
    }

    public Task<LiquidarWsOutDTO> liquidarServiciosAsync(LiquidarWsInDTO liquidarWsInDTO)
    {
      return this.Channel.liquidarServiciosAsync(liquidarWsInDTO);
    }

    public ConsultarDatosNotariaOutDTO consultarDatosNotaria(string idUsuario)
    {
      return this.Channel.consultarDatosNotaria(idUsuario);
    }

    public Task<ConsultarDatosNotariaOutDTO> consultarDatosNotariaAsync(string idUsuario)
    {
      return this.Channel.consultarDatosNotariaAsync(idUsuario);
    }

    public ConsultarRutaAvanzadaWsOutDTO[] consultarRutaAvanzada(ConsultarRutaAvanzadaWsInDTO consultarRutaAvanzadaWsInDTO)
    {
      return this.Channel.consultarRutaAvanzada(consultarRutaAvanzadaWsInDTO);
    }

    public Task<ConsultarRutaAvanzadaWsOutDTO[]> consultarRutaAvanzadaAsync(ConsultarRutaAvanzadaWsInDTO consultarRutaAvanzadaWsInDTO)
    {
      return this.Channel.consultarRutaAvanzadaAsync(consultarRutaAvanzadaWsInDTO);
    }

    public void cambiarEstadoDocumentoDigitalizado(string numTramite, bool bandera)
    {
      this.Channel.cambiarEstadoDocumentoDigitalizado(numTramite, bandera);
    }

    public Task cambiarEstadoDocumentoDigitalizadoAsync(string numTramite, bool bandera)
    {
      return this.Channel.cambiarEstadoDocumentoDigitalizadoAsync(numTramite, bandera);
    }

    public TaLibrosActosOutDTO[] findTaLibrosActos()
    {
      return this.Channel.findTaLibrosActos();
    }

    public Task<TaLibrosActosOutDTO[]> findTaLibrosActosAsync()
    {
      return this.Channel.findTaLibrosActosAsync();
    }

    public TaIdentificacionesVO findTaIdentificacionesById(string idClase)
    {
      return this.Channel.findTaIdentificacionesById(idClase);
    }

    public Task<TaIdentificacionesVO> findTaIdentificacionesByIdAsync(string idClase)
    {
      return this.Channel.findTaIdentificacionesByIdAsync(idClase);
    }

    public TaIdentificacionesVO[] findAllTaIdentificaciones()
    {
      return this.Channel.findAllTaIdentificaciones();
    }

    public Task<TaIdentificacionesVO[]> findAllTaIdentificacionesAsync()
    {
      return this.Channel.findAllTaIdentificacionesAsync();
    }

    public byte[] generarReciboCaja(string numRecibo)
    {
      return this.Channel.generarReciboCaja(numRecibo);
    }

    public Task<byte[]> generarReciboCajaAsync(string numRecibo)
    {
      return this.Channel.generarReciboCajaAsync(numRecibo);
    }

    public MatriculadosDTO[] buscarInscrito(BuscarInscritosInDTO buscarInscritosInDTO)
    {
      return this.Channel.buscarInscrito(buscarInscritosInDTO);
    }

    public Task<MatriculadosDTO[]> buscarInscritoAsync(BuscarInscritosInDTO buscarInscritosInDTO)
    {
      return this.Channel.buscarInscritoAsync(buscarInscritosInDTO);
    }

    public TaTipoDocumentosVO[] findAllTaTipoDocumentos()
    {
      return this.Channel.findAllTaTipoDocumentos();
    }

    public Task<TaTipoDocumentosVO[]> findAllTaTipoDocumentosAsync()
    {
      return this.Channel.findAllTaTipoDocumentosAsync();
    }

    public ConsultarIntransferenciasOutDTO[] consultarInTransferencias(DatosInTransferenciasDTO datosInTransferenciasDTO)
    {
      return this.Channel.consultarInTransferencias(datosInTransferenciasDTO);
    }

    public Task<ConsultarIntransferenciasOutDTO[]> consultarInTransferenciasAsync(DatosInTransferenciasDTO datosInTransferenciasDTO)
    {
      return this.Channel.consultarInTransferenciasAsync(datosInTransferenciasDTO);
    }

    public DatosBasicosClienteDTO consultarDatosBasicosClienteByNumCliente(string numCliente)
    {
      return this.Channel.consultarDatosBasicosClienteByNumCliente(numCliente);
    }

    public Task<DatosBasicosClienteDTO> consultarDatosBasicosClienteByNumClienteAsync(string numCliente)
    {
      return this.Channel.consultarDatosBasicosClienteByNumClienteAsync(numCliente);
    }

    public ConsultarDatosCierreOutDTO[] consultarDatosCierre(DatosCierreInDTO datosCierreInDTO)
    {
      return this.Channel.consultarDatosCierre(datosCierreInDTO);
    }

    public Task<ConsultarDatosCierreOutDTO[]> consultarDatosCierreAsync(DatosCierreInDTO datosCierreInDTO)
    {
      return this.Channel.consultarDatosCierreAsync(datosCierreInDTO);
    }

    public byte[] generarCartaDevolucion(GenerarCartaDevolucionInDTO generarCartaDevolucionInDTO)
    {
      return this.Channel.generarCartaDevolucion(generarCartaDevolucionInDTO);
    }

    public Task<byte[]> generarCartaDevolucionAsync(GenerarCartaDevolucionInDTO generarCartaDevolucionInDTO)
    {
      return this.Channel.generarCartaDevolucionAsync(generarCartaDevolucionInDTO);
    }

    public ConsultarRutaTramiteWsOutDTO consultarRutaTramite(ConsultarRutaTramiteInDTO consultarRutaTramiteInDTO)
    {
      return this.Channel.consultarRutaTramite(consultarRutaTramiteInDTO);
    }

    public Task<ConsultarRutaTramiteWsOutDTO> consultarRutaTramiteAsync(ConsultarRutaTramiteInDTO consultarRutaTramiteInDTO)
    {
      return this.Channel.consultarRutaTramiteAsync(consultarRutaTramiteInDTO);
    }

    public void cambiarEstado(string idSucursal, string idUsuario, string numTramite, string siguienteEstado)
    {
      this.Channel.cambiarEstado(idSucursal, idUsuario, numTramite, siguienteEstado);
    }

    public Task cambiarEstadoAsync(string idSucursal, string idUsuario, string numTramite, string siguienteEstado)
    {
      return this.Channel.cambiarEstadoAsync(idSucursal, idUsuario, numTramite, siguienteEstado);
    }

    public string cerrarCaja(CerrarCajaInDTO cerrarCajaInDTO)
    {
      return this.Channel.cerrarCaja(cerrarCajaInDTO);
    }

    public Task<string> cerrarCajaAsync(CerrarCajaInDTO cerrarCajaInDTO)
    {
      return this.Channel.cerrarCajaAsync(cerrarCajaInDTO);
    }

    public ResultadoDTO actualizarDatosBasicosCliente(DatosBasicosClienteDTO datosBasicosClienteDTO)
    {
      return this.Channel.actualizarDatosBasicosCliente(datosBasicosClienteDTO);
    }

    public Task<ResultadoDTO> actualizarDatosBasicosClienteAsync(DatosBasicosClienteDTO datosBasicosClienteDTO)
    {
      return this.Channel.actualizarDatosBasicosClienteAsync(datosBasicosClienteDTO);
    }

    public RegistrarPagoWSOutDTO registrarPago(RegistrarPagoWSInDTO registrarPago1, string usuario, string clave)
    {
      return this.Channel.registrarPago(registrarPago1, usuario, clave);
    }

    public Task<RegistrarPagoWSOutDTO> registrarPagoAsync(RegistrarPagoWSInDTO registrarPago, string usuario, string clave)
    {
      return this.Channel.registrarPagoAsync(registrarPago, usuario, clave);
    }

    public TaCodigosVO[] findTaCodigos(TaCodigosVO taCodigosVO)
    {
      return this.Channel.findTaCodigos(taCodigosVO);
    }

    public Task<TaCodigosVO[]> findTaCodigosAsync(TaCodigosVO taCodigosVO)
    {
      return this.Channel.findTaCodigosAsync(taCodigosVO);
    }

    public bool obtenerBenficio(CtrBeneficioInDTO @in)
    {
      return this.Channel.obtenerBenficio(@in);
    }

    public Task<bool> obtenerBenficioAsync(CtrBeneficioInDTO @in)
    {
      return this.Channel.obtenerBenficioAsync(@in);
    }

    public ReingresarTramiteOutDTO reingresarTramite(ReingresarTramiteInDTO reingresarTramiteInDTO)
    {
      return this.Channel.reingresarTramite(reingresarTramiteInDTO);
    }

    public Task<ReingresarTramiteOutDTO> reingresarTramiteAsync(ReingresarTramiteInDTO reingresarTramiteInDTO)
    {
      return this.Channel.reingresarTramiteAsync(reingresarTramiteInDTO);
    }
  }
}

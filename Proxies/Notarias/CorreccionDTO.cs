﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.CorreccionDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.tr.sirep2.ccb.org.co")]
  [Serializable]
  public class CorreccionDTO : INotifyPropertyChanged
  {
    private string txtSolicitudField;
    private string txtRespuestaField;

    [SoapElement(IsNullable = true)]
    public string txtSolicitud
    {
      get
      {
        return this.txtSolicitudField;
      }
      set
      {
        this.txtSolicitudField = value;
        this.RaisePropertyChanged(nameof (txtSolicitud));
      }
    }

    [SoapElement(IsNullable = true)]
    public string txtRespuesta
    {
      get
      {
        return this.txtRespuestaField;
      }
      set
      {
        this.txtRespuestaField = value;
        this.RaisePropertyChanged(nameof (txtRespuesta));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

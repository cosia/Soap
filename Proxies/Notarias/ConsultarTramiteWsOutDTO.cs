﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.ConsultarTramiteWsOutDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.not.is.sirep2.ccb.org.co")]
  [Serializable]
  public class ConsultarTramiteWsOutDTO : INotifyPropertyChanged
  {
    private int? cntServicioField;
    private int? ctrReliquidacionField;
    private string desIdCamaraField;
    private string desIdClaseDocField;
    private string desIdEstadoField;
    private string desIdOrigenDocField;
    private string desIdServicioNegocioField;
    private string desIdUsuarioAsigField;
    private DateTime? fecDocField;
    private DateTime? fecRadicaField;
    private DateTime? horRadicaField;
    private string nombreField;
    private string numDocField;
    private string numMatriculaField;
    private string numProponenteField;
    private string numReciboField;
    private string numTramiteField;
    private string observacionesField;
    private double? vlrReliquidacionField;
    private double? vlrTotalField;
    private string idEstadoField;
    private int? numClienteField;

    [SoapElement(IsNullable = true)]
    public int? cntServicio
    {
      get
      {
        return this.cntServicioField;
      }
      set
      {
        this.cntServicioField = value;
        this.RaisePropertyChanged(nameof (cntServicio));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrReliquidacion
    {
      get
      {
        return this.ctrReliquidacionField;
      }
      set
      {
        this.ctrReliquidacionField = value;
        this.RaisePropertyChanged(nameof (ctrReliquidacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdCamara
    {
      get
      {
        return this.desIdCamaraField;
      }
      set
      {
        this.desIdCamaraField = value;
        this.RaisePropertyChanged(nameof (desIdCamara));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdClaseDoc
    {
      get
      {
        return this.desIdClaseDocField;
      }
      set
      {
        this.desIdClaseDocField = value;
        this.RaisePropertyChanged(nameof (desIdClaseDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdEstado
    {
      get
      {
        return this.desIdEstadoField;
      }
      set
      {
        this.desIdEstadoField = value;
        this.RaisePropertyChanged(nameof (desIdEstado));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdOrigenDoc
    {
      get
      {
        return this.desIdOrigenDocField;
      }
      set
      {
        this.desIdOrigenDocField = value;
        this.RaisePropertyChanged(nameof (desIdOrigenDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdServicioNegocio
    {
      get
      {
        return this.desIdServicioNegocioField;
      }
      set
      {
        this.desIdServicioNegocioField = value;
        this.RaisePropertyChanged(nameof (desIdServicioNegocio));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdUsuarioAsig
    {
      get
      {
        return this.desIdUsuarioAsigField;
      }
      set
      {
        this.desIdUsuarioAsigField = value;
        this.RaisePropertyChanged(nameof (desIdUsuarioAsig));
      }
    }

    [SoapElement(IsNullable = true)]
    public DateTime? fecDoc
    {
      get
      {
        return this.fecDocField;
      }
      set
      {
        this.fecDocField = value;
        this.RaisePropertyChanged(nameof (fecDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public DateTime? fecRadica
    {
      get
      {
        return this.fecRadicaField;
      }
      set
      {
        this.fecRadicaField = value;
        this.RaisePropertyChanged(nameof (fecRadica));
      }
    }

    [SoapElement(IsNullable = true)]
    public DateTime? horRadica
    {
      get
      {
        return this.horRadicaField;
      }
      set
      {
        this.horRadicaField = value;
        this.RaisePropertyChanged(nameof (horRadica));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nombre
    {
      get
      {
        return this.nombreField;
      }
      set
      {
        this.nombreField = value;
        this.RaisePropertyChanged(nameof (nombre));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numDoc
    {
      get
      {
        return this.numDocField;
      }
      set
      {
        this.numDocField = value;
        this.RaisePropertyChanged(nameof (numDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numMatricula
    {
      get
      {
        return this.numMatriculaField;
      }
      set
      {
        this.numMatriculaField = value;
        this.RaisePropertyChanged(nameof (numMatricula));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numProponente
    {
      get
      {
        return this.numProponenteField;
      }
      set
      {
        this.numProponenteField = value;
        this.RaisePropertyChanged(nameof (numProponente));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numRecibo
    {
      get
      {
        return this.numReciboField;
      }
      set
      {
        this.numReciboField = value;
        this.RaisePropertyChanged(nameof (numRecibo));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numTramite
    {
      get
      {
        return this.numTramiteField;
      }
      set
      {
        this.numTramiteField = value;
        this.RaisePropertyChanged(nameof (numTramite));
      }
    }

    [SoapElement(IsNullable = true)]
    public string observaciones
    {
      get
      {
        return this.observacionesField;
      }
      set
      {
        this.observacionesField = value;
        this.RaisePropertyChanged(nameof (observaciones));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? vlrReliquidacion
    {
      get
      {
        return this.vlrReliquidacionField;
      }
      set
      {
        this.vlrReliquidacionField = value;
        this.RaisePropertyChanged(nameof (vlrReliquidacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? vlrTotal
    {
      get
      {
        return this.vlrTotalField;
      }
      set
      {
        this.vlrTotalField = value;
        this.RaisePropertyChanged(nameof (vlrTotal));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idEstado
    {
      get
      {
        return this.idEstadoField;
      }
      set
      {
        this.idEstadoField = value;
        this.RaisePropertyChanged(nameof (idEstado));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? numCliente
    {
      get
      {
        return this.numClienteField;
      }
      set
      {
        this.numClienteField = value;
        this.RaisePropertyChanged(nameof (numCliente));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

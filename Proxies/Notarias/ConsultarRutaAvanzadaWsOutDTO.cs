﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.ConsultarRutaAvanzadaWsOutDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.not.is.sirep2.ccb.org.co")]
  [Serializable]
  public class ConsultarRutaAvanzadaWsOutDTO : INotifyPropertyChanged
  {
    private string numTramiteField;
    private string desIdServicioNegocioField;
    private DateTime? fecRadicaField;
    private DateTime? horRadicaField;
    private string desIdSucursalUltField;
    private string desIdEstadoUltField;
    private string desIdUsuarioAsigField;
    private string desIdOrigenDocField;
    private string desIdTipoDocField;

    [SoapElement(IsNullable = true)]
    public string numTramite
    {
      get
      {
        return this.numTramiteField;
      }
      set
      {
        this.numTramiteField = value;
        this.RaisePropertyChanged(nameof (numTramite));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdServicioNegocio
    {
      get
      {
        return this.desIdServicioNegocioField;
      }
      set
      {
        this.desIdServicioNegocioField = value;
        this.RaisePropertyChanged(nameof (desIdServicioNegocio));
      }
    }

    [SoapElement(IsNullable = true)]
    public DateTime? fecRadica
    {
      get
      {
        return this.fecRadicaField;
      }
      set
      {
        this.fecRadicaField = value;
        this.RaisePropertyChanged(nameof (fecRadica));
      }
    }

    [SoapElement(IsNullable = true)]
    public DateTime? horRadica
    {
      get
      {
        return this.horRadicaField;
      }
      set
      {
        this.horRadicaField = value;
        this.RaisePropertyChanged(nameof (horRadica));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdSucursalUlt
    {
      get
      {
        return this.desIdSucursalUltField;
      }
      set
      {
        this.desIdSucursalUltField = value;
        this.RaisePropertyChanged(nameof (desIdSucursalUlt));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdEstadoUlt
    {
      get
      {
        return this.desIdEstadoUltField;
      }
      set
      {
        this.desIdEstadoUltField = value;
        this.RaisePropertyChanged(nameof (desIdEstadoUlt));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdUsuarioAsig
    {
      get
      {
        return this.desIdUsuarioAsigField;
      }
      set
      {
        this.desIdUsuarioAsigField = value;
        this.RaisePropertyChanged(nameof (desIdUsuarioAsig));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdOrigenDoc
    {
      get
      {
        return this.desIdOrigenDocField;
      }
      set
      {
        this.desIdOrigenDocField = value;
        this.RaisePropertyChanged(nameof (desIdOrigenDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdTipoDoc
    {
      get
      {
        return this.desIdTipoDocField;
      }
      set
      {
        this.desIdTipoDocField = value;
        this.RaisePropertyChanged(nameof (desIdTipoDoc));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

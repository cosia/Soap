﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.ValidarCiiuOutDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.cac.sirep2.ccb.org.co")]
  [Serializable]
  public class ValidarCiiuOutDTO : INotifyPropertyChanged
  {
    private string codigoField;
    private string descripcionField;
    private bool? validoDIANField;
    private bool? validoSHField;
    private int? codigoSHDField;
    private int? itemField;
    private int[] posiblesValoresSHField;

    [SoapElement(IsNullable = true)]
    public string codigo
    {
      get
      {
        return this.codigoField;
      }
      set
      {
        this.codigoField = value;
        this.RaisePropertyChanged(nameof (codigo));
      }
    }

    [SoapElement(IsNullable = true)]
    public string descripcion
    {
      get
      {
        return this.descripcionField;
      }
      set
      {
        this.descripcionField = value;
        this.RaisePropertyChanged(nameof (descripcion));
      }
    }

    [SoapElement(IsNullable = true)]
    public bool? validoDIAN
    {
      get
      {
        return this.validoDIANField;
      }
      set
      {
        this.validoDIANField = value;
        this.RaisePropertyChanged(nameof (validoDIAN));
      }
    }

    [SoapElement(IsNullable = true)]
    public bool? validoSH
    {
      get
      {
        return this.validoSHField;
      }
      set
      {
        this.validoSHField = value;
        this.RaisePropertyChanged(nameof (validoSH));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? codigoSHD
    {
      get
      {
        return this.codigoSHDField;
      }
      set
      {
        this.codigoSHDField = value;
        this.RaisePropertyChanged(nameof (codigoSHD));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? item
    {
      get
      {
        return this.itemField;
      }
      set
      {
        this.itemField = value;
        this.RaisePropertyChanged(nameof (item));
      }
    }

    [SoapElement(IsNullable = true)]
    public int[] posiblesValoresSH
    {
      get
      {
        return this.posiblesValoresSHField;
      }
      set
      {
        this.posiblesValoresSHField = value;
        this.RaisePropertyChanged(nameof (posiblesValoresSH));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

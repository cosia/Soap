﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.CerrarCajaInDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.in.sirep2.ccb.org.co")]
  [Serializable]
  public class CerrarCajaInDTO : INotifyPropertyChanged
  {
    private string observacionesField;
    private string idServicioField;
    private double? vrServicioField;
    private string idMonedaField;
    private UsuariosDTO usuarioDTOField;

    [SoapElement(IsNullable = true)]
    public string observaciones
    {
      get
      {
        return this.observacionesField;
      }
      set
      {
        this.observacionesField = value;
        this.RaisePropertyChanged(nameof (observaciones));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idServicio
    {
      get
      {
        return this.idServicioField;
      }
      set
      {
        this.idServicioField = value;
        this.RaisePropertyChanged(nameof (idServicio));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? vrServicio
    {
      get
      {
        return this.vrServicioField;
      }
      set
      {
        this.vrServicioField = value;
        this.RaisePropertyChanged(nameof (vrServicio));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idMoneda
    {
      get
      {
        return this.idMonedaField;
      }
      set
      {
        this.idMonedaField = value;
        this.RaisePropertyChanged(nameof (idMoneda));
      }
    }

    [SoapElement(IsNullable = true)]
    public UsuariosDTO usuarioDTO
    {
      get
      {
        return this.usuarioDTOField;
      }
      set
      {
        this.usuarioDTOField = value;
        this.RaisePropertyChanged(nameof (usuarioDTO));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

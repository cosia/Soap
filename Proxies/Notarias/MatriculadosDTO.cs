﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.MatriculadosDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.mpn.sirep2.ccb.org.co")]
  [Serializable]
  public class MatriculadosDTO : INotifyPropertyChanged
  {
    private bool? afiliadoField;
    private string categoriaField;
    private string codigoCamaraField;
    private string codigoClaseIdentificacionField;
    private string desCategoriaField;
    private string descripcionCamaraField;
    private string descripcionClaseIdentificacionField;
    private string desEstadoMatField;
    private string desIdClaseField;
    private string desOrganizacionField;
    private string desPropiedadField;
    private string desSociedadField;
    private string digitoVerificacionField;
    private string estadoMatriculaField;
    private string estadoProponenteField;
    private string idPropiedadField;
    private string matriculaField;
    private string nombreField;
    private int? numClienteField;
    private string numeroIdentificacionField;
    private string numeroInscripcionField;
    private string organizacionField;
    private string rue_codigo_camara_iField;
    private string rue_desc_camaraField;
    private string rue_desc_camara_iField;
    private string rue_desc_municipio_comercialField;
    private string rue_direccion_comercialField;
    private string rue_municipio_comercialField;
    private string rue_telefono_comercialField;
    private Decimal? rue_valor_activos_sin_ajusteField;
    private string siglaField;
    private int? tipoSociedadField;
    private string ultAnoRenovaField;

    [SoapElement(IsNullable = true)]
    public bool? afiliado
    {
      get
      {
        return this.afiliadoField;
      }
      set
      {
        this.afiliadoField = value;
        this.RaisePropertyChanged(nameof (afiliado));
      }
    }

    [SoapElement(IsNullable = true)]
    public string categoria
    {
      get
      {
        return this.categoriaField;
      }
      set
      {
        this.categoriaField = value;
        this.RaisePropertyChanged(nameof (categoria));
      }
    }

    [SoapElement(IsNullable = true)]
    public string codigoCamara
    {
      get
      {
        return this.codigoCamaraField;
      }
      set
      {
        this.codigoCamaraField = value;
        this.RaisePropertyChanged(nameof (codigoCamara));
      }
    }

    [SoapElement(IsNullable = true)]
    public string codigoClaseIdentificacion
    {
      get
      {
        return this.codigoClaseIdentificacionField;
      }
      set
      {
        this.codigoClaseIdentificacionField = value;
        this.RaisePropertyChanged(nameof (codigoClaseIdentificacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desCategoria
    {
      get
      {
        return this.desCategoriaField;
      }
      set
      {
        this.desCategoriaField = value;
        this.RaisePropertyChanged(nameof (desCategoria));
      }
    }

    [SoapElement(IsNullable = true)]
    public string descripcionCamara
    {
      get
      {
        return this.descripcionCamaraField;
      }
      set
      {
        this.descripcionCamaraField = value;
        this.RaisePropertyChanged(nameof (descripcionCamara));
      }
    }

    [SoapElement(IsNullable = true)]
    public string descripcionClaseIdentificacion
    {
      get
      {
        return this.descripcionClaseIdentificacionField;
      }
      set
      {
        this.descripcionClaseIdentificacionField = value;
        this.RaisePropertyChanged(nameof (descripcionClaseIdentificacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desEstadoMat
    {
      get
      {
        return this.desEstadoMatField;
      }
      set
      {
        this.desEstadoMatField = value;
        this.RaisePropertyChanged(nameof (desEstadoMat));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdClase
    {
      get
      {
        return this.desIdClaseField;
      }
      set
      {
        this.desIdClaseField = value;
        this.RaisePropertyChanged(nameof (desIdClase));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desOrganizacion
    {
      get
      {
        return this.desOrganizacionField;
      }
      set
      {
        this.desOrganizacionField = value;
        this.RaisePropertyChanged(nameof (desOrganizacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desPropiedad
    {
      get
      {
        return this.desPropiedadField;
      }
      set
      {
        this.desPropiedadField = value;
        this.RaisePropertyChanged(nameof (desPropiedad));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desSociedad
    {
      get
      {
        return this.desSociedadField;
      }
      set
      {
        this.desSociedadField = value;
        this.RaisePropertyChanged(nameof (desSociedad));
      }
    }

    [SoapElement(IsNullable = true)]
    public string digitoVerificacion
    {
      get
      {
        return this.digitoVerificacionField;
      }
      set
      {
        this.digitoVerificacionField = value;
        this.RaisePropertyChanged(nameof (digitoVerificacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string estadoMatricula
    {
      get
      {
        return this.estadoMatriculaField;
      }
      set
      {
        this.estadoMatriculaField = value;
        this.RaisePropertyChanged(nameof (estadoMatricula));
      }
    }

    [SoapElement(IsNullable = true)]
    public string estadoProponente
    {
      get
      {
        return this.estadoProponenteField;
      }
      set
      {
        this.estadoProponenteField = value;
        this.RaisePropertyChanged(nameof (estadoProponente));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idPropiedad
    {
      get
      {
        return this.idPropiedadField;
      }
      set
      {
        this.idPropiedadField = value;
        this.RaisePropertyChanged(nameof (idPropiedad));
      }
    }

    [SoapElement(IsNullable = true)]
    public string matricula
    {
      get
      {
        return this.matriculaField;
      }
      set
      {
        this.matriculaField = value;
        this.RaisePropertyChanged(nameof (matricula));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nombre
    {
      get
      {
        return this.nombreField;
      }
      set
      {
        this.nombreField = value;
        this.RaisePropertyChanged(nameof (nombre));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? numCliente
    {
      get
      {
        return this.numClienteField;
      }
      set
      {
        this.numClienteField = value;
        this.RaisePropertyChanged(nameof (numCliente));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numeroIdentificacion
    {
      get
      {
        return this.numeroIdentificacionField;
      }
      set
      {
        this.numeroIdentificacionField = value;
        this.RaisePropertyChanged(nameof (numeroIdentificacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numeroInscripcion
    {
      get
      {
        return this.numeroInscripcionField;
      }
      set
      {
        this.numeroInscripcionField = value;
        this.RaisePropertyChanged(nameof (numeroInscripcion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string organizacion
    {
      get
      {
        return this.organizacionField;
      }
      set
      {
        this.organizacionField = value;
        this.RaisePropertyChanged(nameof (organizacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string rue_codigo_camara_i
    {
      get
      {
        return this.rue_codigo_camara_iField;
      }
      set
      {
        this.rue_codigo_camara_iField = value;
        this.RaisePropertyChanged(nameof (rue_codigo_camara_i));
      }
    }

    [SoapElement(IsNullable = true)]
    public string rue_desc_camara
    {
      get
      {
        return this.rue_desc_camaraField;
      }
      set
      {
        this.rue_desc_camaraField = value;
        this.RaisePropertyChanged(nameof (rue_desc_camara));
      }
    }

    [SoapElement(IsNullable = true)]
    public string rue_desc_camara_i
    {
      get
      {
        return this.rue_desc_camara_iField;
      }
      set
      {
        this.rue_desc_camara_iField = value;
        this.RaisePropertyChanged(nameof (rue_desc_camara_i));
      }
    }

    [SoapElement(IsNullable = true)]
    public string rue_desc_municipio_comercial
    {
      get
      {
        return this.rue_desc_municipio_comercialField;
      }
      set
      {
        this.rue_desc_municipio_comercialField = value;
        this.RaisePropertyChanged(nameof (rue_desc_municipio_comercial));
      }
    }

    [SoapElement(IsNullable = true)]
    public string rue_direccion_comercial
    {
      get
      {
        return this.rue_direccion_comercialField;
      }
      set
      {
        this.rue_direccion_comercialField = value;
        this.RaisePropertyChanged(nameof (rue_direccion_comercial));
      }
    }

    [SoapElement(IsNullable = true)]
    public string rue_municipio_comercial
    {
      get
      {
        return this.rue_municipio_comercialField;
      }
      set
      {
        this.rue_municipio_comercialField = value;
        this.RaisePropertyChanged(nameof (rue_municipio_comercial));
      }
    }

    [SoapElement(IsNullable = true)]
    public string rue_telefono_comercial
    {
      get
      {
        return this.rue_telefono_comercialField;
      }
      set
      {
        this.rue_telefono_comercialField = value;
        this.RaisePropertyChanged(nameof (rue_telefono_comercial));
      }
    }

    [SoapElement(IsNullable = true)]
    public Decimal? rue_valor_activos_sin_ajuste
    {
      get
      {
        return this.rue_valor_activos_sin_ajusteField;
      }
      set
      {
        this.rue_valor_activos_sin_ajusteField = value;
        this.RaisePropertyChanged(nameof (rue_valor_activos_sin_ajuste));
      }
    }

    [SoapElement(IsNullable = true)]
    public string sigla
    {
      get
      {
        return this.siglaField;
      }
      set
      {
        this.siglaField = value;
        this.RaisePropertyChanged(nameof (sigla));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? tipoSociedad
    {
      get
      {
        return this.tipoSociedadField;
      }
      set
      {
        this.tipoSociedadField = value;
        this.RaisePropertyChanged(nameof (tipoSociedad));
      }
    }

    [SoapElement(IsNullable = true)]
    public string ultAnoRenova
    {
      get
      {
        return this.ultAnoRenovaField;
      }
      set
      {
        this.ultAnoRenovaField = value;
        this.RaisePropertyChanged(nameof (ultAnoRenova));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.ReingresarTramiteInDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.not.is.sirep2.ccb.org.co")]
  [Serializable]
  public class ReingresarTramiteInDTO : INotifyPropertyChanged
  {
    private string numTramiteField;
    private string claveField;
    private string usuarioField;

    [SoapElement(IsNullable = true)]
    public string numTramite
    {
      get
      {
        return this.numTramiteField;
      }
      set
      {
        this.numTramiteField = value;
        this.RaisePropertyChanged(nameof (numTramite));
      }
    }

    [SoapElement(IsNullable = true)]
    public string clave
    {
      get
      {
        return this.claveField;
      }
      set
      {
        this.claveField = value;
        this.RaisePropertyChanged(nameof (clave));
      }
    }

    [SoapElement(IsNullable = true)]
    public string usuario
    {
      get
      {
        return this.usuarioField;
      }
      set
      {
        this.usuarioField = value;
        this.RaisePropertyChanged(nameof (usuario));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

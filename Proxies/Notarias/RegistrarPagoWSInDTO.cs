﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.RegistrarPagoWSInDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.not.is.sirep2.ccb.org.co")]
  [Serializable]
  public class RegistrarPagoWSInDTO : INotifyPropertyChanged
  {
    private RegistrarPagoCajaWsDTO datosRegistrarPagoField;
    private string idOperadorField;
    private string idSucursalField;
    private CrearDatosBasicosDocumentosWsInDTO[] librosActosSeleccionadosField;
    private int? idContactoField;
    private string imagenField;

    [SoapElement(IsNullable = true)]
    public RegistrarPagoCajaWsDTO datosRegistrarPago
    {
      get
      {
        return this.datosRegistrarPagoField;
      }
      set
      {
        this.datosRegistrarPagoField = value;
        this.RaisePropertyChanged(nameof (datosRegistrarPago));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idOperador
    {
      get
      {
        return this.idOperadorField;
      }
      set
      {
        this.idOperadorField = value;
        this.RaisePropertyChanged(nameof (idOperador));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idSucursal
    {
      get
      {
        return this.idSucursalField;
      }
      set
      {
        this.idSucursalField = value;
        this.RaisePropertyChanged(nameof (idSucursal));
      }
    }

    [SoapElement(IsNullable = true)]
    public CrearDatosBasicosDocumentosWsInDTO[] librosActosSeleccionados
    {
      get
      {
        return this.librosActosSeleccionadosField;
      }
      set
      {
        this.librosActosSeleccionadosField = value;
        this.RaisePropertyChanged(nameof (librosActosSeleccionados));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? idContacto
    {
      get
      {
        return this.idContactoField;
      }
      set
      {
        this.idContactoField = value;
        this.RaisePropertyChanged(nameof (idContacto));
      }
    }

    [SoapElement(IsNullable = true)]
    public string imagen
    {
      get
      {
        return this.imagenField;
      }
      set
      {
        this.imagenField = value;
        this.RaisePropertyChanged(nameof (imagen));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

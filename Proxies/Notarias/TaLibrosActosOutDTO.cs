﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.TaLibrosActosOutDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.dataservices.cache.common.sirep2.ccb.org.co")]
  [Serializable]
  public class TaLibrosActosOutDTO : INotifyPropertyChanged
  {
    private int? ctrConstitucionField;
    private int? ctrInscripcionesField;
    private string idActoField;
    private string idGrupoActoField;
    private string idLibroField;
    private string nomActoField;
    private string nomCodigoField;
    private TaImpuestosOutDTO[] impuestosField;

    [SoapElement(IsNullable = true)]
    public int? ctrConstitucion
    {
      get
      {
        return this.ctrConstitucionField;
      }
      set
      {
        this.ctrConstitucionField = value;
        this.RaisePropertyChanged(nameof (ctrConstitucion));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrInscripciones
    {
      get
      {
        return this.ctrInscripcionesField;
      }
      set
      {
        this.ctrInscripcionesField = value;
        this.RaisePropertyChanged(nameof (ctrInscripciones));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idActo
    {
      get
      {
        return this.idActoField;
      }
      set
      {
        this.idActoField = value;
        this.RaisePropertyChanged(nameof (idActo));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idGrupoActo
    {
      get
      {
        return this.idGrupoActoField;
      }
      set
      {
        this.idGrupoActoField = value;
        this.RaisePropertyChanged(nameof (idGrupoActo));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idLibro
    {
      get
      {
        return this.idLibroField;
      }
      set
      {
        this.idLibroField = value;
        this.RaisePropertyChanged(nameof (idLibro));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nomActo
    {
      get
      {
        return this.nomActoField;
      }
      set
      {
        this.nomActoField = value;
        this.RaisePropertyChanged(nameof (nomActo));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nomCodigo
    {
      get
      {
        return this.nomCodigoField;
      }
      set
      {
        this.nomCodigoField = value;
        this.RaisePropertyChanged(nameof (nomCodigo));
      }
    }

    [SoapElement(IsNullable = true)]
    public TaImpuestosOutDTO[] impuestos
    {
      get
      {
        return this.impuestosField;
      }
      set
      {
        this.impuestosField = value;
        this.RaisePropertyChanged(nameof (impuestos));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

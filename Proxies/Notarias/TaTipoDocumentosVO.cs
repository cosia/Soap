﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.TaTipoDocumentosVO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://ta.model.common.sirep2.ccb.org.co")]
  [Serializable]
  public class TaTipoDocumentosVO : INotifyPropertyChanged
  {
    private string idTipoDocField;
    private string nomDocumentoField;
    private int? ctrVigenteField;
    private int? ctrNumeroField;

    [SoapElement(IsNullable = true)]
    public string idTipoDoc
    {
      get
      {
        return this.idTipoDocField;
      }
      set
      {
        this.idTipoDocField = value;
        this.RaisePropertyChanged(nameof (idTipoDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nomDocumento
    {
      get
      {
        return this.nomDocumentoField;
      }
      set
      {
        this.nomDocumentoField = value;
        this.RaisePropertyChanged(nameof (nomDocumento));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrVigente
    {
      get
      {
        return this.ctrVigenteField;
      }
      set
      {
        this.ctrVigenteField = value;
        this.RaisePropertyChanged(nameof (ctrVigente));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrNumero
    {
      get
      {
        return this.ctrNumeroField;
      }
      set
      {
        this.ctrNumeroField = value;
        this.RaisePropertyChanged(nameof (ctrNumero));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

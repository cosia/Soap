﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.EstadoWsDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.not.is.sirep2.ccb.org.co")]
  [Serializable]
  public class EstadoWsDTO : INotifyPropertyChanged
  {
    private string idEstadoField;
    private string desIdEstadoField;
    private DateTime? fecEstadoField;
    private DateTime? horEstadoField;
    private string desIdUsuarioField;
    private string desIdSucursalField;

    [SoapElement(IsNullable = true)]
    public string idEstado
    {
      get
      {
        return this.idEstadoField;
      }
      set
      {
        this.idEstadoField = value;
        this.RaisePropertyChanged(nameof (idEstado));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdEstado
    {
      get
      {
        return this.desIdEstadoField;
      }
      set
      {
        this.desIdEstadoField = value;
        this.RaisePropertyChanged(nameof (desIdEstado));
      }
    }

    [SoapElement(IsNullable = true)]
    public DateTime? fecEstado
    {
      get
      {
        return this.fecEstadoField;
      }
      set
      {
        this.fecEstadoField = value;
        this.RaisePropertyChanged(nameof (fecEstado));
      }
    }

    [SoapElement(IsNullable = true)]
    public DateTime? horEstado
    {
      get
      {
        return this.horEstadoField;
      }
      set
      {
        this.horEstadoField = value;
        this.RaisePropertyChanged(nameof (horEstado));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdUsuario
    {
      get
      {
        return this.desIdUsuarioField;
      }
      set
      {
        this.desIdUsuarioField = value;
        this.RaisePropertyChanged(nameof (desIdUsuario));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdSucursal
    {
      get
      {
        return this.desIdSucursalField;
      }
      set
      {
        this.desIdSucursalField = value;
        this.RaisePropertyChanged(nameof (desIdSucursal));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

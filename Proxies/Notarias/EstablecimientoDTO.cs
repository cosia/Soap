﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.EstablecimientoDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.id.sirep2.ccb.org.co")]
  [Serializable]
  public class EstablecimientoDTO : INotifyPropertyChanged
  {
    private int? acogeBeneficioField;
    private int? ctrBeneficioField;
    private int? cntPersonalField;
    private int? itemField;
    private string numTramiteField;
    private double? valorComercialField;

    [SoapElement(IsNullable = true)]
    public int? acogeBeneficio
    {
      get
      {
        return this.acogeBeneficioField;
      }
      set
      {
        this.acogeBeneficioField = value;
        this.RaisePropertyChanged(nameof (acogeBeneficio));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrBeneficio
    {
      get
      {
        return this.ctrBeneficioField;
      }
      set
      {
        this.ctrBeneficioField = value;
        this.RaisePropertyChanged(nameof (ctrBeneficio));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? cntPersonal
    {
      get
      {
        return this.cntPersonalField;
      }
      set
      {
        this.cntPersonalField = value;
        this.RaisePropertyChanged(nameof (cntPersonal));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? item
    {
      get
      {
        return this.itemField;
      }
      set
      {
        this.itemField = value;
        this.RaisePropertyChanged(nameof (item));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numTramite
    {
      get
      {
        return this.numTramiteField;
      }
      set
      {
        this.numTramiteField = value;
        this.RaisePropertyChanged(nameof (numTramite));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? valorComercial
    {
      get
      {
        return this.valorComercialField;
      }
      set
      {
        this.valorComercialField = value;
        this.RaisePropertyChanged(nameof (valorComercial));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

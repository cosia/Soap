﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.DatosCierreInDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.in.sirep2.ccb.org.co")]
  [Serializable]
  public class DatosCierreInDTO : INotifyPropertyChanged
  {
    private string fechaField;
    private string idUsuarioField;
    private string idSucursalField;

    [SoapElement(IsNullable = true)]
    public string fecha
    {
      get
      {
        return this.fechaField;
      }
      set
      {
        this.fechaField = value;
        this.RaisePropertyChanged(nameof (fecha));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idUsuario
    {
      get
      {
        return this.idUsuarioField;
      }
      set
      {
        this.idUsuarioField = value;
        this.RaisePropertyChanged(nameof (idUsuario));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idSucursal
    {
      get
      {
        return this.idSucursalField;
      }
      set
      {
        this.idSucursalField = value;
        this.RaisePropertyChanged(nameof (idSucursal));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.TaIdentificacionesVO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://ta.model.common.sirep2.ccb.org.co")]
  [Serializable]
  public class TaIdentificacionesVO : INotifyPropertyChanged
  {
    private string idClaseField;
    private string nomIdField;
    private string nomCortoField;
    private int? ctrPerNatField;
    private int? ctrVigenteField;

    [SoapElement(IsNullable = true)]
    public string idClase
    {
      get
      {
        return this.idClaseField;
      }
      set
      {
        this.idClaseField = value;
        this.RaisePropertyChanged(nameof (idClase));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nomId
    {
      get
      {
        return this.nomIdField;
      }
      set
      {
        this.nomIdField = value;
        this.RaisePropertyChanged(nameof (nomId));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nomCorto
    {
      get
      {
        return this.nomCortoField;
      }
      set
      {
        this.nomCortoField = value;
        this.RaisePropertyChanged(nameof (nomCorto));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrPerNat
    {
      get
      {
        return this.ctrPerNatField;
      }
      set
      {
        this.ctrPerNatField = value;
        this.RaisePropertyChanged(nameof (ctrPerNat));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrVigente
    {
      get
      {
        return this.ctrVigenteField;
      }
      set
      {
        this.ctrVigenteField = value;
        this.RaisePropertyChanged(nameof (ctrVigente));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

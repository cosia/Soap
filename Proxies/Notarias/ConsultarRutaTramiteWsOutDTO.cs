﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.ConsultarRutaTramiteWsOutDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.not.is.sirep2.ccb.org.co")]
  [Serializable]
  public class ConsultarRutaTramiteWsOutDTO : INotifyPropertyChanged
  {
    private string numTramiteField;
    private string numMatriculaField;
    private string nombreField;
    private string desIdEstadoMatField;
    private string idServicioNegocioField;
    private string desIdClaseField;
    private string numIdField;
    private string numProponenteField;
    private string desIdEstadoPropField;
    private string numReciboField;
    private string desIdServicioNegocioField;
    private string desIdSucursalRadicaField;
    private string numDocField;
    private DateTime? fecDocField;
    private string desIdTipoDocField;
    private string desIdOrigenField;
    private string desIdMunicipioDocField;
    private string numHorasTramiteField;
    private int? ctrIndicadorField;
    private int? ctrDigitalField;
    private EstadoWsDTO[] estadosField;
    private CorreccionDTO correccionField;
    private string idEstadoUltField;
    private string idUsuarioAsigField;

    [SoapElement(IsNullable = true)]
    public string numTramite
    {
      get
      {
        return this.numTramiteField;
      }
      set
      {
        this.numTramiteField = value;
        this.RaisePropertyChanged(nameof (numTramite));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numMatricula
    {
      get
      {
        return this.numMatriculaField;
      }
      set
      {
        this.numMatriculaField = value;
        this.RaisePropertyChanged(nameof (numMatricula));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nombre
    {
      get
      {
        return this.nombreField;
      }
      set
      {
        this.nombreField = value;
        this.RaisePropertyChanged(nameof (nombre));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdEstadoMat
    {
      get
      {
        return this.desIdEstadoMatField;
      }
      set
      {
        this.desIdEstadoMatField = value;
        this.RaisePropertyChanged(nameof (desIdEstadoMat));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idServicioNegocio
    {
      get
      {
        return this.idServicioNegocioField;
      }
      set
      {
        this.idServicioNegocioField = value;
        this.RaisePropertyChanged(nameof (idServicioNegocio));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdClase
    {
      get
      {
        return this.desIdClaseField;
      }
      set
      {
        this.desIdClaseField = value;
        this.RaisePropertyChanged(nameof (desIdClase));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numId
    {
      get
      {
        return this.numIdField;
      }
      set
      {
        this.numIdField = value;
        this.RaisePropertyChanged(nameof (numId));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numProponente
    {
      get
      {
        return this.numProponenteField;
      }
      set
      {
        this.numProponenteField = value;
        this.RaisePropertyChanged(nameof (numProponente));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdEstadoProp
    {
      get
      {
        return this.desIdEstadoPropField;
      }
      set
      {
        this.desIdEstadoPropField = value;
        this.RaisePropertyChanged(nameof (desIdEstadoProp));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numRecibo
    {
      get
      {
        return this.numReciboField;
      }
      set
      {
        this.numReciboField = value;
        this.RaisePropertyChanged(nameof (numRecibo));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdServicioNegocio
    {
      get
      {
        return this.desIdServicioNegocioField;
      }
      set
      {
        this.desIdServicioNegocioField = value;
        this.RaisePropertyChanged(nameof (desIdServicioNegocio));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdSucursalRadica
    {
      get
      {
        return this.desIdSucursalRadicaField;
      }
      set
      {
        this.desIdSucursalRadicaField = value;
        this.RaisePropertyChanged(nameof (desIdSucursalRadica));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numDoc
    {
      get
      {
        return this.numDocField;
      }
      set
      {
        this.numDocField = value;
        this.RaisePropertyChanged(nameof (numDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public DateTime? fecDoc
    {
      get
      {
        return this.fecDocField;
      }
      set
      {
        this.fecDocField = value;
        this.RaisePropertyChanged(nameof (fecDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdTipoDoc
    {
      get
      {
        return this.desIdTipoDocField;
      }
      set
      {
        this.desIdTipoDocField = value;
        this.RaisePropertyChanged(nameof (desIdTipoDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdOrigen
    {
      get
      {
        return this.desIdOrigenField;
      }
      set
      {
        this.desIdOrigenField = value;
        this.RaisePropertyChanged(nameof (desIdOrigen));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdMunicipioDoc
    {
      get
      {
        return this.desIdMunicipioDocField;
      }
      set
      {
        this.desIdMunicipioDocField = value;
        this.RaisePropertyChanged(nameof (desIdMunicipioDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numHorasTramite
    {
      get
      {
        return this.numHorasTramiteField;
      }
      set
      {
        this.numHorasTramiteField = value;
        this.RaisePropertyChanged(nameof (numHorasTramite));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrIndicador
    {
      get
      {
        return this.ctrIndicadorField;
      }
      set
      {
        this.ctrIndicadorField = value;
        this.RaisePropertyChanged(nameof (ctrIndicador));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrDigital
    {
      get
      {
        return this.ctrDigitalField;
      }
      set
      {
        this.ctrDigitalField = value;
        this.RaisePropertyChanged(nameof (ctrDigital));
      }
    }

    [SoapElement(IsNullable = true)]
    public EstadoWsDTO[] estados
    {
      get
      {
        return this.estadosField;
      }
      set
      {
        this.estadosField = value;
        this.RaisePropertyChanged(nameof (estados));
      }
    }

    [SoapElement(IsNullable = true)]
    public CorreccionDTO correccion
    {
      get
      {
        return this.correccionField;
      }
      set
      {
        this.correccionField = value;
        this.RaisePropertyChanged(nameof (correccion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idEstadoUlt
    {
      get
      {
        return this.idEstadoUltField;
      }
      set
      {
        this.idEstadoUltField = value;
        this.RaisePropertyChanged(nameof (idEstadoUlt));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idUsuarioAsig
    {
      get
      {
        return this.idUsuarioAsigField;
      }
      set
      {
        this.idUsuarioAsigField = value;
        this.RaisePropertyChanged(nameof (idUsuarioAsig));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

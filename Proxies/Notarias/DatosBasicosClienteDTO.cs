﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.DatosBasicosClienteDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.cli.sirep2.ccb.org.co")]
  [Serializable]
  public class DatosBasicosClienteDTO : INotifyPropertyChanged
  {
    private CcContactosVO contactoField;
    private int? ctrChequeDevueltoField;
    private int? ctrCopropiedadField;
    private string ctrPropMatriculadoField;
    private string descEstadoInscripcionField;
    private string descEstadoMatriculaField;
    private string descIdClaseField;
    private RmDireccionesVO direccionField;
    private DireccionVO[] direccionesField;
    private string fechaExpField;
    private string idCategoriaField;
    private string idClaseField;
    private string idEstadoInscripcionField;
    private string idEstadoMatriculaField;
    private string idMunipExpField;
    private string idNacionalidadField;
    private string idOrganizacionField;
    private string idPaisDocField;
    private string nombreCompletoField;
    private int? numClienteField;
    private int? numClientePadreField;
    private string numIdClientePadreField;
    private string idClaseClientePadreField;
    private string numIdField;
    private string numInscripcionField;
    private string numMatriculaField;
    private string numRadicaDianField;
    private string numRutField;
    private string primerApellidoField;
    private string primerNombreField;
    private string segundoApellidoField;
    private string segundoNombreField;
    private string siglaField;
    private int? tipoClienteField;
    private ResultadoDTO[] vinculosField;
    private string digitoVerificacionField;
    private string ultAnoRenovaField;

    [SoapElement(IsNullable = true)]
    public CcContactosVO contacto
    {
      get
      {
        return this.contactoField;
      }
      set
      {
        this.contactoField = value;
        this.RaisePropertyChanged(nameof (contacto));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrChequeDevuelto
    {
      get
      {
        return this.ctrChequeDevueltoField;
      }
      set
      {
        this.ctrChequeDevueltoField = value;
        this.RaisePropertyChanged(nameof (ctrChequeDevuelto));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrCopropiedad
    {
      get
      {
        return this.ctrCopropiedadField;
      }
      set
      {
        this.ctrCopropiedadField = value;
        this.RaisePropertyChanged(nameof (ctrCopropiedad));
      }
    }

    [SoapElement(IsNullable = true)]
    public string ctrPropMatriculado
    {
      get
      {
        return this.ctrPropMatriculadoField;
      }
      set
      {
        this.ctrPropMatriculadoField = value;
        this.RaisePropertyChanged(nameof (ctrPropMatriculado));
      }
    }

    [SoapElement(IsNullable = true)]
    public string descEstadoInscripcion
    {
      get
      {
        return this.descEstadoInscripcionField;
      }
      set
      {
        this.descEstadoInscripcionField = value;
        this.RaisePropertyChanged(nameof (descEstadoInscripcion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string descEstadoMatricula
    {
      get
      {
        return this.descEstadoMatriculaField;
      }
      set
      {
        this.descEstadoMatriculaField = value;
        this.RaisePropertyChanged(nameof (descEstadoMatricula));
      }
    }

    [SoapElement(IsNullable = true)]
    public string descIdClase
    {
      get
      {
        return this.descIdClaseField;
      }
      set
      {
        this.descIdClaseField = value;
        this.RaisePropertyChanged(nameof (descIdClase));
      }
    }

    [SoapElement(IsNullable = true)]
    public RmDireccionesVO direccion
    {
      get
      {
        return this.direccionField;
      }
      set
      {
        this.direccionField = value;
        this.RaisePropertyChanged(nameof (direccion));
      }
    }

    [SoapElement(IsNullable = true)]
    public DireccionVO[] direcciones
    {
      get
      {
        return this.direccionesField;
      }
      set
      {
        this.direccionesField = value;
        this.RaisePropertyChanged(nameof (direcciones));
      }
    }

    [SoapElement(IsNullable = true)]
    public string fechaExp
    {
      get
      {
        return this.fechaExpField;
      }
      set
      {
        this.fechaExpField = value;
        this.RaisePropertyChanged(nameof (fechaExp));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idCategoria
    {
      get
      {
        return this.idCategoriaField;
      }
      set
      {
        this.idCategoriaField = value;
        this.RaisePropertyChanged(nameof (idCategoria));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idClase
    {
      get
      {
        return this.idClaseField;
      }
      set
      {
        this.idClaseField = value;
        this.RaisePropertyChanged(nameof (idClase));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idEstadoInscripcion
    {
      get
      {
        return this.idEstadoInscripcionField;
      }
      set
      {
        this.idEstadoInscripcionField = value;
        this.RaisePropertyChanged(nameof (idEstadoInscripcion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idEstadoMatricula
    {
      get
      {
        return this.idEstadoMatriculaField;
      }
      set
      {
        this.idEstadoMatriculaField = value;
        this.RaisePropertyChanged(nameof (idEstadoMatricula));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idMunipExp
    {
      get
      {
        return this.idMunipExpField;
      }
      set
      {
        this.idMunipExpField = value;
        this.RaisePropertyChanged(nameof (idMunipExp));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idNacionalidad
    {
      get
      {
        return this.idNacionalidadField;
      }
      set
      {
        this.idNacionalidadField = value;
        this.RaisePropertyChanged(nameof (idNacionalidad));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idOrganizacion
    {
      get
      {
        return this.idOrganizacionField;
      }
      set
      {
        this.idOrganizacionField = value;
        this.RaisePropertyChanged(nameof (idOrganizacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idPaisDoc
    {
      get
      {
        return this.idPaisDocField;
      }
      set
      {
        this.idPaisDocField = value;
        this.RaisePropertyChanged(nameof (idPaisDoc));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nombreCompleto
    {
      get
      {
        return this.nombreCompletoField;
      }
      set
      {
        this.nombreCompletoField = value;
        this.RaisePropertyChanged(nameof (nombreCompleto));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? numCliente
    {
      get
      {
        return this.numClienteField;
      }
      set
      {
        this.numClienteField = value;
        this.RaisePropertyChanged(nameof (numCliente));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? numClientePadre
    {
      get
      {
        return this.numClientePadreField;
      }
      set
      {
        this.numClientePadreField = value;
        this.RaisePropertyChanged(nameof (numClientePadre));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numIdClientePadre
    {
      get
      {
        return this.numIdClientePadreField;
      }
      set
      {
        this.numIdClientePadreField = value;
        this.RaisePropertyChanged(nameof (numIdClientePadre));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idClaseClientePadre
    {
      get
      {
        return this.idClaseClientePadreField;
      }
      set
      {
        this.idClaseClientePadreField = value;
        this.RaisePropertyChanged(nameof (idClaseClientePadre));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numId
    {
      get
      {
        return this.numIdField;
      }
      set
      {
        this.numIdField = value;
        this.RaisePropertyChanged(nameof (numId));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numInscripcion
    {
      get
      {
        return this.numInscripcionField;
      }
      set
      {
        this.numInscripcionField = value;
        this.RaisePropertyChanged(nameof (numInscripcion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numMatricula
    {
      get
      {
        return this.numMatriculaField;
      }
      set
      {
        this.numMatriculaField = value;
        this.RaisePropertyChanged(nameof (numMatricula));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numRadicaDian
    {
      get
      {
        return this.numRadicaDianField;
      }
      set
      {
        this.numRadicaDianField = value;
        this.RaisePropertyChanged(nameof (numRadicaDian));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numRut
    {
      get
      {
        return this.numRutField;
      }
      set
      {
        this.numRutField = value;
        this.RaisePropertyChanged(nameof (numRut));
      }
    }

    [SoapElement(IsNullable = true)]
    public string primerApellido
    {
      get
      {
        return this.primerApellidoField;
      }
      set
      {
        this.primerApellidoField = value;
        this.RaisePropertyChanged(nameof (primerApellido));
      }
    }

    [SoapElement(IsNullable = true)]
    public string primerNombre
    {
      get
      {
        return this.primerNombreField;
      }
      set
      {
        this.primerNombreField = value;
        this.RaisePropertyChanged(nameof (primerNombre));
      }
    }

    [SoapElement(IsNullable = true)]
    public string segundoApellido
    {
      get
      {
        return this.segundoApellidoField;
      }
      set
      {
        this.segundoApellidoField = value;
        this.RaisePropertyChanged(nameof (segundoApellido));
      }
    }

    [SoapElement(IsNullable = true)]
    public string segundoNombre
    {
      get
      {
        return this.segundoNombreField;
      }
      set
      {
        this.segundoNombreField = value;
        this.RaisePropertyChanged(nameof (segundoNombre));
      }
    }

    [SoapElement(IsNullable = true)]
    public string sigla
    {
      get
      {
        return this.siglaField;
      }
      set
      {
        this.siglaField = value;
        this.RaisePropertyChanged(nameof (sigla));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? tipoCliente
    {
      get
      {
        return this.tipoClienteField;
      }
      set
      {
        this.tipoClienteField = value;
        this.RaisePropertyChanged(nameof (tipoCliente));
      }
    }

    [SoapElement(IsNullable = true)]
    public ResultadoDTO[] vinculos
    {
      get
      {
        return this.vinculosField;
      }
      set
      {
        this.vinculosField = value;
        this.RaisePropertyChanged(nameof (vinculos));
      }
    }

    [SoapElement(IsNullable = true)]
    public string digitoVerificacion
    {
      get
      {
        return this.digitoVerificacionField;
      }
      set
      {
        this.digitoVerificacionField = value;
        this.RaisePropertyChanged(nameof (digitoVerificacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string ultAnoRenova
    {
      get
      {
        return this.ultAnoRenovaField;
      }
      set
      {
        this.ultAnoRenovaField = value;
        this.RaisePropertyChanged(nameof (ultAnoRenova));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

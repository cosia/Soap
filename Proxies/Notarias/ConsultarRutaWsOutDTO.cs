﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.ConsultarRutaWsOutDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.not.is.sirep2.ccb.org.co")]
  [Serializable]
  public class ConsultarRutaWsOutDTO : INotifyPropertyChanged
  {
    private string numTramiteField;
    private string desServicioNegocioField;
    private string idServicioNegocioField;
    private DateTime? fecRadicaField;
    private DateTime? horRadicaField;
    private string desIdSucursalRadicaField;
    private string desIdEstadoUltField;
    private int? ctrIndicadorField;
    private string numHorasField;

    [SoapElement(IsNullable = true)]
    public string numTramite
    {
      get
      {
        return this.numTramiteField;
      }
      set
      {
        this.numTramiteField = value;
        this.RaisePropertyChanged(nameof (numTramite));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desServicioNegocio
    {
      get
      {
        return this.desServicioNegocioField;
      }
      set
      {
        this.desServicioNegocioField = value;
        this.RaisePropertyChanged(nameof (desServicioNegocio));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idServicioNegocio
    {
      get
      {
        return this.idServicioNegocioField;
      }
      set
      {
        this.idServicioNegocioField = value;
        this.RaisePropertyChanged(nameof (idServicioNegocio));
      }
    }

    [SoapElement(IsNullable = true)]
    public DateTime? fecRadica
    {
      get
      {
        return this.fecRadicaField;
      }
      set
      {
        this.fecRadicaField = value;
        this.RaisePropertyChanged(nameof (fecRadica));
      }
    }

    [SoapElement(IsNullable = true)]
    public DateTime? horRadica
    {
      get
      {
        return this.horRadicaField;
      }
      set
      {
        this.horRadicaField = value;
        this.RaisePropertyChanged(nameof (horRadica));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdSucursalRadica
    {
      get
      {
        return this.desIdSucursalRadicaField;
      }
      set
      {
        this.desIdSucursalRadicaField = value;
        this.RaisePropertyChanged(nameof (desIdSucursalRadica));
      }
    }

    [SoapElement(IsNullable = true)]
    public string desIdEstadoUlt
    {
      get
      {
        return this.desIdEstadoUltField;
      }
      set
      {
        this.desIdEstadoUltField = value;
        this.RaisePropertyChanged(nameof (desIdEstadoUlt));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrIndicador
    {
      get
      {
        return this.ctrIndicadorField;
      }
      set
      {
        this.ctrIndicadorField = value;
        this.RaisePropertyChanged(nameof (ctrIndicador));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numHoras
    {
      get
      {
        return this.numHorasField;
      }
      set
      {
        this.numHorasField = value;
        this.RaisePropertyChanged(nameof (numHoras));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

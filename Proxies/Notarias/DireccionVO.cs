﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.DireccionVO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.cli.sirep2.ccb.org.co")]
  [Serializable]
  public class DireccionVO : INotifyPropertyChanged
  {
    private string descIdMunipField;
    private string descIdPaisField;
    private string descTipoDirField;
    private RmDireccionesVO direccionField;

    [SoapElement(IsNullable = true)]
    public string descIdMunip
    {
      get
      {
        return this.descIdMunipField;
      }
      set
      {
        this.descIdMunipField = value;
        this.RaisePropertyChanged(nameof (descIdMunip));
      }
    }

    [SoapElement(IsNullable = true)]
    public string descIdPais
    {
      get
      {
        return this.descIdPaisField;
      }
      set
      {
        this.descIdPaisField = value;
        this.RaisePropertyChanged(nameof (descIdPais));
      }
    }

    [SoapElement(IsNullable = true)]
    public string descTipoDir
    {
      get
      {
        return this.descTipoDirField;
      }
      set
      {
        this.descTipoDirField = value;
        this.RaisePropertyChanged(nameof (descTipoDir));
      }
    }

    [SoapElement(IsNullable = true)]
    public RmDireccionesVO direccion
    {
      get
      {
        return this.direccionField;
      }
      set
      {
        this.direccionField = value;
        this.RaisePropertyChanged(nameof (direccion));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

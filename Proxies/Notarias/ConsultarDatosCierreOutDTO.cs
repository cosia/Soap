﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.ConsultarDatosCierreOutDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.in.sirep2.ccb.org.co")]
  [Serializable]
  public class ConsultarDatosCierreOutDTO : INotifyPropertyChanged
  {
    private string txtObservacionesField;
    private string idMonedaField;
    private string idOperadorField;
    private FormaPagoInDTO[] pagosField;
    private double? totalDevolucionesField;
    private double? totalCompensacionesField;
    private double? totalEntregadoField;
    private double? totalEntregadoChequeField;
    private double? totalEntregadoEfectivoField;
    private double? totalRecaudadoField;
    private double? saldoChequeField;
    private double? vueltasChequesField;

    [SoapElement(IsNullable = true)]
    public string txtObservaciones
    {
      get
      {
        return this.txtObservacionesField;
      }
      set
      {
        this.txtObservacionesField = value;
        this.RaisePropertyChanged(nameof (txtObservaciones));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idMoneda
    {
      get
      {
        return this.idMonedaField;
      }
      set
      {
        this.idMonedaField = value;
        this.RaisePropertyChanged(nameof (idMoneda));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idOperador
    {
      get
      {
        return this.idOperadorField;
      }
      set
      {
        this.idOperadorField = value;
        this.RaisePropertyChanged(nameof (idOperador));
      }
    }

    [SoapElement(IsNullable = true)]
    public FormaPagoInDTO[] pagos
    {
      get
      {
        return this.pagosField;
      }
      set
      {
        this.pagosField = value;
        this.RaisePropertyChanged(nameof (pagos));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? totalDevoluciones
    {
      get
      {
        return this.totalDevolucionesField;
      }
      set
      {
        this.totalDevolucionesField = value;
        this.RaisePropertyChanged(nameof (totalDevoluciones));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? totalCompensaciones
    {
      get
      {
        return this.totalCompensacionesField;
      }
      set
      {
        this.totalCompensacionesField = value;
        this.RaisePropertyChanged(nameof (totalCompensaciones));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? totalEntregado
    {
      get
      {
        return this.totalEntregadoField;
      }
      set
      {
        this.totalEntregadoField = value;
        this.RaisePropertyChanged(nameof (totalEntregado));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? totalEntregadoCheque
    {
      get
      {
        return this.totalEntregadoChequeField;
      }
      set
      {
        this.totalEntregadoChequeField = value;
        this.RaisePropertyChanged(nameof (totalEntregadoCheque));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? totalEntregadoEfectivo
    {
      get
      {
        return this.totalEntregadoEfectivoField;
      }
      set
      {
        this.totalEntregadoEfectivoField = value;
        this.RaisePropertyChanged(nameof (totalEntregadoEfectivo));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? totalRecaudado
    {
      get
      {
        return this.totalRecaudadoField;
      }
      set
      {
        this.totalRecaudadoField = value;
        this.RaisePropertyChanged(nameof (totalRecaudado));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? saldoCheque
    {
      get
      {
        return this.saldoChequeField;
      }
      set
      {
        this.saldoChequeField = value;
        this.RaisePropertyChanged(nameof (saldoCheque));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? vueltasCheques
    {
      get
      {
        return this.vueltasChequesField;
      }
      set
      {
        this.vueltasChequesField = value;
        this.RaisePropertyChanged(nameof (vueltasCheques));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

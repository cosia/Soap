﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.DatosInTransferenciasDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.in.sirep2.ccb.org.co")]
  [Serializable]
  public class DatosInTransferenciasDTO : INotifyPropertyChanged
  {
    private string idMunicipioField;
    private string idOrigenField;
    private string fecTransferenciaField;
    private string numTransferenciaField;
    private double? valorField;
    private int? ctrPagoField;
    private string fecOperacionField;

    [SoapElement(IsNullable = true)]
    public string idMunicipio
    {
      get
      {
        return this.idMunicipioField;
      }
      set
      {
        this.idMunicipioField = value;
        this.RaisePropertyChanged(nameof (idMunicipio));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idOrigen
    {
      get
      {
        return this.idOrigenField;
      }
      set
      {
        this.idOrigenField = value;
        this.RaisePropertyChanged(nameof (idOrigen));
      }
    }

    [SoapElement(IsNullable = true)]
    public string fecTransferencia
    {
      get
      {
        return this.fecTransferenciaField;
      }
      set
      {
        this.fecTransferenciaField = value;
        this.RaisePropertyChanged(nameof (fecTransferencia));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numTransferencia
    {
      get
      {
        return this.numTransferenciaField;
      }
      set
      {
        this.numTransferenciaField = value;
        this.RaisePropertyChanged(nameof (numTransferencia));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? valor
    {
      get
      {
        return this.valorField;
      }
      set
      {
        this.valorField = value;
        this.RaisePropertyChanged(nameof (valor));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrPago
    {
      get
      {
        return this.ctrPagoField;
      }
      set
      {
        this.ctrPagoField = value;
        this.RaisePropertyChanged(nameof (ctrPago));
      }
    }

    [SoapElement(IsNullable = true)]
    public string fecOperacion
    {
      get
      {
        return this.fecOperacionField;
      }
      set
      {
        this.fecOperacionField = value;
        this.RaisePropertyChanged(nameof (fecOperacion));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

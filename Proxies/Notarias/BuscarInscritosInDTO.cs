﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.BuscarInscritosInDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.mpn.sirep2.ccb.org.co")]
  [Serializable]
  public class BuscarInscritosInDTO : INotifyPropertyChanged
  {
    private string noInscripcionField;
    private string estadoField;
    private string registroField;
    private string matriculaField;
    private string identifcacionField;
    private string nombreField;
    private string tipoIdentificacionField;
    private string camaraField;
    private string tipoRegistroField;
    private string palabraClave1Field;

    [SoapElement(IsNullable = true)]
    public string noInscripcion
    {
      get
      {
        return this.noInscripcionField;
      }
      set
      {
        this.noInscripcionField = value;
        this.RaisePropertyChanged(nameof (noInscripcion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string estado
    {
      get
      {
        return this.estadoField;
      }
      set
      {
        this.estadoField = value;
        this.RaisePropertyChanged(nameof (estado));
      }
    }

    [SoapElement(IsNullable = true)]
    public string registro
    {
      get
      {
        return this.registroField;
      }
      set
      {
        this.registroField = value;
        this.RaisePropertyChanged(nameof (registro));
      }
    }

    [SoapElement(IsNullable = true)]
    public string matricula
    {
      get
      {
        return this.matriculaField;
      }
      set
      {
        this.matriculaField = value;
        this.RaisePropertyChanged(nameof (matricula));
      }
    }

    [SoapElement(IsNullable = true)]
    public string identifcacion
    {
      get
      {
        return this.identifcacionField;
      }
      set
      {
        this.identifcacionField = value;
        this.RaisePropertyChanged(nameof (identifcacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nombre
    {
      get
      {
        return this.nombreField;
      }
      set
      {
        this.nombreField = value;
        this.RaisePropertyChanged(nameof (nombre));
      }
    }

    [SoapElement(IsNullable = true)]
    public string tipoIdentificacion
    {
      get
      {
        return this.tipoIdentificacionField;
      }
      set
      {
        this.tipoIdentificacionField = value;
        this.RaisePropertyChanged(nameof (tipoIdentificacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string camara
    {
      get
      {
        return this.camaraField;
      }
      set
      {
        this.camaraField = value;
        this.RaisePropertyChanged(nameof (camara));
      }
    }

    [SoapElement(IsNullable = true)]
    public string tipoRegistro
    {
      get
      {
        return this.tipoRegistroField;
      }
      set
      {
        this.tipoRegistroField = value;
        this.RaisePropertyChanged(nameof (tipoRegistro));
      }
    }

    [SoapElement(IsNullable = true)]
    public string palabraClave1
    {
      get
      {
        return this.palabraClave1Field;
      }
      set
      {
        this.palabraClave1Field = value;
        this.RaisePropertyChanged(nameof (palabraClave1));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

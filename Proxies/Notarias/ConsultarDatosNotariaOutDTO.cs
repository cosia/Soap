﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.ConsultarDatosNotariaOutDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.not.is.sirep2.ccb.org.co")]
  [Serializable]
  public class ConsultarDatosNotariaOutDTO : INotifyPropertyChanged
  {
    private string idOperadorField;
    private string nombreField;
    private TaNotariasVO notariaField;

    [SoapElement(IsNullable = true)]
    public string idOperador
    {
      get
      {
        return this.idOperadorField;
      }
      set
      {
        this.idOperadorField = value;
        this.RaisePropertyChanged(nameof (idOperador));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nombre
    {
      get
      {
        return this.nombreField;
      }
      set
      {
        this.nombreField = value;
        this.RaisePropertyChanged(nameof (nombre));
      }
    }

    [SoapElement(IsNullable = true)]
    public TaNotariasVO notaria
    {
      get
      {
        return this.notariaField;
      }
      set
      {
        this.notariaField = value;
        this.RaisePropertyChanged(nameof (notaria));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

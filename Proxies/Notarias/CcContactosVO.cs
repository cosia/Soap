﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.CcContactosVO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://cc.model.common.sirep2.ccb.org.co")]
  [Serializable]
  public class CcContactosVO : INotifyPropertyChanged
  {
    private int? numClienteField;
    private int? codContactoField;
    private string nombreField;
    private string nombre1Field;
    private string nombre2Field;
    private string apellido1Field;
    private string apellido2Field;
    private string direccionField;
    private string telefonoField;
    private string emailField;
    private string idTipoContactoField;
    private string idMunicipioField;
    private string numFaxField;
    private string cargoField;
    private string idPaisField;

    [SoapElement(IsNullable = true)]
    public int? numCliente
    {
      get
      {
        return this.numClienteField;
      }
      set
      {
        this.numClienteField = value;
        this.RaisePropertyChanged(nameof (numCliente));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? codContacto
    {
      get
      {
        return this.codContactoField;
      }
      set
      {
        this.codContactoField = value;
        this.RaisePropertyChanged(nameof (codContacto));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nombre
    {
      get
      {
        return this.nombreField;
      }
      set
      {
        this.nombreField = value;
        this.RaisePropertyChanged(nameof (nombre));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nombre1
    {
      get
      {
        return this.nombre1Field;
      }
      set
      {
        this.nombre1Field = value;
        this.RaisePropertyChanged(nameof (nombre1));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nombre2
    {
      get
      {
        return this.nombre2Field;
      }
      set
      {
        this.nombre2Field = value;
        this.RaisePropertyChanged(nameof (nombre2));
      }
    }

    [SoapElement(IsNullable = true)]
    public string apellido1
    {
      get
      {
        return this.apellido1Field;
      }
      set
      {
        this.apellido1Field = value;
        this.RaisePropertyChanged(nameof (apellido1));
      }
    }

    [SoapElement(IsNullable = true)]
    public string apellido2
    {
      get
      {
        return this.apellido2Field;
      }
      set
      {
        this.apellido2Field = value;
        this.RaisePropertyChanged(nameof (apellido2));
      }
    }

    [SoapElement(IsNullable = true)]
    public string direccion
    {
      get
      {
        return this.direccionField;
      }
      set
      {
        this.direccionField = value;
        this.RaisePropertyChanged(nameof (direccion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string telefono
    {
      get
      {
        return this.telefonoField;
      }
      set
      {
        this.telefonoField = value;
        this.RaisePropertyChanged(nameof (telefono));
      }
    }

    [SoapElement(IsNullable = true)]
    public string email
    {
      get
      {
        return this.emailField;
      }
      set
      {
        this.emailField = value;
        this.RaisePropertyChanged(nameof (email));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idTipoContacto
    {
      get
      {
        return this.idTipoContactoField;
      }
      set
      {
        this.idTipoContactoField = value;
        this.RaisePropertyChanged(nameof (idTipoContacto));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idMunicipio
    {
      get
      {
        return this.idMunicipioField;
      }
      set
      {
        this.idMunicipioField = value;
        this.RaisePropertyChanged(nameof (idMunicipio));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numFax
    {
      get
      {
        return this.numFaxField;
      }
      set
      {
        this.numFaxField = value;
        this.RaisePropertyChanged(nameof (numFax));
      }
    }

    [SoapElement(IsNullable = true)]
    public string cargo
    {
      get
      {
        return this.cargoField;
      }
      set
      {
        this.cargoField = value;
        this.RaisePropertyChanged(nameof (cargo));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idPais
    {
      get
      {
        return this.idPaisField;
      }
      set
      {
        this.idPaisField = value;
        this.RaisePropertyChanged(nameof (idPais));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

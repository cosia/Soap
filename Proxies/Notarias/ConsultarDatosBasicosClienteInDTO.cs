﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.ConsultarDatosBasicosClienteInDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.cli.sirep2.ccb.org.co")]
  [Serializable]
  public class ConsultarDatosBasicosClienteInDTO : INotifyPropertyChanged
  {
    private string idClaseField;
    private int? numClienteField;
    private string numIdField;
    private string numInscripcionField;
    private string numMatriculaField;
    private string numRutField;
    private int? tipoConsultaField;

    [SoapElement(IsNullable = true)]
    public string idClase
    {
      get
      {
        return this.idClaseField;
      }
      set
      {
        this.idClaseField = value;
        this.RaisePropertyChanged(nameof (idClase));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? numCliente
    {
      get
      {
        return this.numClienteField;
      }
      set
      {
        this.numClienteField = value;
        this.RaisePropertyChanged(nameof (numCliente));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numId
    {
      get
      {
        return this.numIdField;
      }
      set
      {
        this.numIdField = value;
        this.RaisePropertyChanged(nameof (numId));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numInscripcion
    {
      get
      {
        return this.numInscripcionField;
      }
      set
      {
        this.numInscripcionField = value;
        this.RaisePropertyChanged(nameof (numInscripcion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numMatricula
    {
      get
      {
        return this.numMatriculaField;
      }
      set
      {
        this.numMatriculaField = value;
        this.RaisePropertyChanged(nameof (numMatricula));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numRut
    {
      get
      {
        return this.numRutField;
      }
      set
      {
        this.numRutField = value;
        this.RaisePropertyChanged(nameof (numRut));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? tipoConsulta
    {
      get
      {
        return this.tipoConsultaField;
      }
      set
      {
        this.tipoConsultaField = value;
        this.RaisePropertyChanged(nameof (tipoConsulta));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

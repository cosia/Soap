﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.UsuariosDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.in.sirep2.ccb.org.co")]
  [Serializable]
  public class UsuariosDTO : INotifyPropertyChanged
  {
    private double? baseCajaField;
    private int? ctrEstadoCajaField;
    private string idClaseField;
    private string idOperadorField;
    private string idSucursalField;
    private string idUsuarioField;
    private string nomIdClaseField;
    private string nomSucursalField;
    private string nomUsuarioField;
    private int? numClienteField;
    private string numIdField;

    [SoapElement(IsNullable = true)]
    public double? baseCaja
    {
      get
      {
        return this.baseCajaField;
      }
      set
      {
        this.baseCajaField = value;
        this.RaisePropertyChanged(nameof (baseCaja));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrEstadoCaja
    {
      get
      {
        return this.ctrEstadoCajaField;
      }
      set
      {
        this.ctrEstadoCajaField = value;
        this.RaisePropertyChanged(nameof (ctrEstadoCaja));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idClase
    {
      get
      {
        return this.idClaseField;
      }
      set
      {
        this.idClaseField = value;
        this.RaisePropertyChanged(nameof (idClase));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idOperador
    {
      get
      {
        return this.idOperadorField;
      }
      set
      {
        this.idOperadorField = value;
        this.RaisePropertyChanged(nameof (idOperador));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idSucursal
    {
      get
      {
        return this.idSucursalField;
      }
      set
      {
        this.idSucursalField = value;
        this.RaisePropertyChanged(nameof (idSucursal));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idUsuario
    {
      get
      {
        return this.idUsuarioField;
      }
      set
      {
        this.idUsuarioField = value;
        this.RaisePropertyChanged(nameof (idUsuario));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nomIdClase
    {
      get
      {
        return this.nomIdClaseField;
      }
      set
      {
        this.nomIdClaseField = value;
        this.RaisePropertyChanged(nameof (nomIdClase));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nomSucursal
    {
      get
      {
        return this.nomSucursalField;
      }
      set
      {
        this.nomSucursalField = value;
        this.RaisePropertyChanged(nameof (nomSucursal));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nomUsuario
    {
      get
      {
        return this.nomUsuarioField;
      }
      set
      {
        this.nomUsuarioField = value;
        this.RaisePropertyChanged(nameof (nomUsuario));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? numCliente
    {
      get
      {
        return this.numClienteField;
      }
      set
      {
        this.numClienteField = value;
        this.RaisePropertyChanged(nameof (numCliente));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numId
    {
      get
      {
        return this.numIdField;
      }
      set
      {
        this.numIdField = value;
        this.RaisePropertyChanged(nameof (numId));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.RegistrarPagoCajaWsDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.not.is.sirep2.ccb.org.co")]
  [Serializable]
  public class RegistrarPagoCajaWsDTO : INotifyPropertyChanged
  {
    private DatosBasicosClienteDTO clienteField;
    private int? ctrAnuladoField;
    private int? ctrConsignacionField;
    private int? ctrReliquidacionField;
    private int? ctrVentaIndirectaField;
    private FormaPagoInDTO[] formaPagoField;
    private string idSucursalField;
    private string idUsuarioField;
    private LiquidarWsOutDTO liquidacionesField;
    private string numFacturaField;
    private string numOfertaField;
    private string tipoTransaccionField;
    private string ventaExternaField;

    [SoapElement(IsNullable = true)]
    public DatosBasicosClienteDTO cliente
    {
      get
      {
        return this.clienteField;
      }
      set
      {
        this.clienteField = value;
        this.RaisePropertyChanged(nameof (cliente));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrAnulado
    {
      get
      {
        return this.ctrAnuladoField;
      }
      set
      {
        this.ctrAnuladoField = value;
        this.RaisePropertyChanged(nameof (ctrAnulado));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrConsignacion
    {
      get
      {
        return this.ctrConsignacionField;
      }
      set
      {
        this.ctrConsignacionField = value;
        this.RaisePropertyChanged(nameof (ctrConsignacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrReliquidacion
    {
      get
      {
        return this.ctrReliquidacionField;
      }
      set
      {
        this.ctrReliquidacionField = value;
        this.RaisePropertyChanged(nameof (ctrReliquidacion));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? ctrVentaIndirecta
    {
      get
      {
        return this.ctrVentaIndirectaField;
      }
      set
      {
        this.ctrVentaIndirectaField = value;
        this.RaisePropertyChanged(nameof (ctrVentaIndirecta));
      }
    }

    [SoapElement(IsNullable = true)]
    public FormaPagoInDTO[] formaPago
    {
      get
      {
        return this.formaPagoField;
      }
      set
      {
        this.formaPagoField = value;
        this.RaisePropertyChanged(nameof (formaPago));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idSucursal
    {
      get
      {
        return this.idSucursalField;
      }
      set
      {
        this.idSucursalField = value;
        this.RaisePropertyChanged(nameof (idSucursal));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idUsuario
    {
      get
      {
        return this.idUsuarioField;
      }
      set
      {
        this.idUsuarioField = value;
        this.RaisePropertyChanged(nameof (idUsuario));
      }
    }

    [SoapElement(IsNullable = true)]
    public LiquidarWsOutDTO liquidaciones
    {
      get
      {
        return this.liquidacionesField;
      }
      set
      {
        this.liquidacionesField = value;
        this.RaisePropertyChanged(nameof (liquidaciones));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numFactura
    {
      get
      {
        return this.numFacturaField;
      }
      set
      {
        this.numFacturaField = value;
        this.RaisePropertyChanged(nameof (numFactura));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numOferta
    {
      get
      {
        return this.numOfertaField;
      }
      set
      {
        this.numOfertaField = value;
        this.RaisePropertyChanged(nameof (numOferta));
      }
    }

    [SoapElement(IsNullable = true)]
    public string tipoTransaccion
    {
      get
      {
        return this.tipoTransaccionField;
      }
      set
      {
        this.tipoTransaccionField = value;
        this.RaisePropertyChanged(nameof (tipoTransaccion));
      }
    }

    [SoapElement(IsNullable = true)]
    public string ventaExterna
    {
      get
      {
        return this.ventaExternaField;
      }
      set
      {
        this.ventaExternaField = value;
        this.RaisePropertyChanged(nameof (ventaExterna));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

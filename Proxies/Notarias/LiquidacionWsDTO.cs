﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.Notarias.LiquidacionWsDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.Notarias
{
  [GeneratedCode("System.Xml", "4.0.30319.34234")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [SoapType(Namespace = "http://model.not.is.sirep2.ccb.org.co")]
  [Serializable]
  public class LiquidacionWsDTO : INotifyPropertyChanged
  {
    private string anoRenovaField;
    private int? cantidadField;
    private string gastoAdministrativoField;
    private string idActoField;
    private string idLibroField;
    private string idServicioField;
    private string idServicioBaseField;
    private string nombreServicioField;
    private string numMatriculaField;
    private double? valorBaseField;
    private double? valorServicioField;
    private int? itemField;

    [SoapElement(IsNullable = true)]
    public string anoRenova
    {
      get
      {
        return this.anoRenovaField;
      }
      set
      {
        this.anoRenovaField = value;
        this.RaisePropertyChanged(nameof (anoRenova));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? cantidad
    {
      get
      {
        return this.cantidadField;
      }
      set
      {
        this.cantidadField = value;
        this.RaisePropertyChanged(nameof (cantidad));
      }
    }

    [SoapElement(IsNullable = true)]
    public string gastoAdministrativo
    {
      get
      {
        return this.gastoAdministrativoField;
      }
      set
      {
        this.gastoAdministrativoField = value;
        this.RaisePropertyChanged(nameof (gastoAdministrativo));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idActo
    {
      get
      {
        return this.idActoField;
      }
      set
      {
        this.idActoField = value;
        this.RaisePropertyChanged(nameof (idActo));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idLibro
    {
      get
      {
        return this.idLibroField;
      }
      set
      {
        this.idLibroField = value;
        this.RaisePropertyChanged(nameof (idLibro));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idServicio
    {
      get
      {
        return this.idServicioField;
      }
      set
      {
        this.idServicioField = value;
        this.RaisePropertyChanged(nameof (idServicio));
      }
    }

    [SoapElement(IsNullable = true)]
    public string idServicioBase
    {
      get
      {
        return this.idServicioBaseField;
      }
      set
      {
        this.idServicioBaseField = value;
        this.RaisePropertyChanged(nameof (idServicioBase));
      }
    }

    [SoapElement(IsNullable = true)]
    public string nombreServicio
    {
      get
      {
        return this.nombreServicioField;
      }
      set
      {
        this.nombreServicioField = value;
        this.RaisePropertyChanged(nameof (nombreServicio));
      }
    }

    [SoapElement(IsNullable = true)]
    public string numMatricula
    {
      get
      {
        return this.numMatriculaField;
      }
      set
      {
        this.numMatriculaField = value;
        this.RaisePropertyChanged(nameof (numMatricula));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? valorBase
    {
      get
      {
        return this.valorBaseField;
      }
      set
      {
        this.valorBaseField = value;
        this.RaisePropertyChanged(nameof (valorBase));
      }
    }

    [SoapElement(IsNullable = true)]
    public double? valorServicio
    {
      get
      {
        return this.valorServicioField;
      }
      set
      {
        this.valorServicioField = value;
        this.RaisePropertyChanged(nameof (valorServicio));
      }
    }

    [SoapElement(IsNullable = true)]
    public int? item
    {
      get
      {
        return this.itemField;
      }
      set
      {
        this.itemField = value;
        this.RaisePropertyChanged(nameof (item));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioBus.registrarPagoMutacionPortTypeClient
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioBus
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class registrarPagoMutacionPortTypeClient : ClientBase<registrarPagoMutacionPortType>, registrarPagoMutacionPortType
  {
    public registrarPagoMutacionPortTypeClient()
    {
    }

    public registrarPagoMutacionPortTypeClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public registrarPagoMutacionPortTypeClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public registrarPagoMutacionPortTypeClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public registrarPagoMutacionPortTypeClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    registrarPagoMutacion_Output registrarPagoMutacionPortType.registrarPagoMutacion(registrarPagoMutacion_Input request)
    {
      return this.Channel.registrarPagoMutacion(request);
    }

    public registrarPagoMutacionRespType registrarPagoMutacion(registrarPagoMutacionType registrarPagoMutacion1)
    {
      return ((registrarPagoMutacionPortType) this).registrarPagoMutacion(new registrarPagoMutacion_Input()
      {
        registrarPagoMutacion = registrarPagoMutacion1
      }).registrarPagoMutacionResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<registrarPagoMutacion_Output> registrarPagoMutacionPortType.registrarPagoMutacionAsync(registrarPagoMutacion_Input request)
    {
      return this.Channel.registrarPagoMutacionAsync(request);
    }

    public Task<registrarPagoMutacion_Output> registrarPagoMutacionAsync(registrarPagoMutacionType registrarPagoMutacion)
    {
      return ((registrarPagoMutacionPortType) this).registrarPagoMutacionAsync(new registrarPagoMutacion_Input()
      {
        registrarPagoMutacion = registrarPagoMutacion
      });
    }
  }
}

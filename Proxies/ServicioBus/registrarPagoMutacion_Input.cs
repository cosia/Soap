﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioBus.registrarPagoMutacion_Input
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioBus
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [EditorBrowsable(EditorBrowsableState.Advanced)]
  [MessageContract(IsWrapped = false)]
  public class registrarPagoMutacion_Input
  {
    [MessageBodyMember(Namespace = "http://ws.recaudos.esb.ccb.org.co", Order = 0)]
    public registrarPagoMutacionType registrarPagoMutacion;

    public registrarPagoMutacion_Input()
    {
    }

    public registrarPagoMutacion_Input(registrarPagoMutacionType registrarPagoMutacion)
    {
      this.registrarPagoMutacion = registrarPagoMutacion;
    }
  }
}

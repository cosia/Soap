﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioBus.registrarPagoMutacionPortType
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ServiceModel;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioBus
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [ServiceContract(ConfigurationName = "Proxies.ServicioBus.registrarPagoMutacionPortType", Namespace = "http://ws.recaudos.esb.ccb.org.co")]
  public interface registrarPagoMutacionPortType
  {
    [OperationContract(Action = "", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    registrarPagoMutacion_Output registrarPagoMutacion(registrarPagoMutacion_Input request);

    [OperationContract(Action = "", ReplyAction = "*")]
    Task<registrarPagoMutacion_Output> registrarPagoMutacionAsync(registrarPagoMutacion_Input request);
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioBus.dataType2
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioBus
{
  [GeneratedCode("System.Xml", "4.6.79.0")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://ws.recaudos.esb.ccb.org.co")]
  [Serializable]
  public class dataType2 : INotifyPropertyChanged
  {
    private registrarPagoDTOType2 registrarPagoDTOField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
    public registrarPagoDTOType2 registrarPagoDTO
    {
      get
      {
        return this.registrarPagoDTOField;
      }
      set
      {
        this.registrarPagoDTOField = value;
        this.RaisePropertyChanged(nameof (registrarPagoDTO));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioLiquidaciones.LiquidarServiciosWSClient
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioLiquidaciones
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class LiquidarServiciosWSClient : ClientBase<LiquidarServiciosWS>, LiquidarServiciosWS
  {
    public LiquidarServiciosWSClient()
    {
    }

    public LiquidarServiciosWSClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public LiquidarServiciosWSClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public LiquidarServiciosWSClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public LiquidarServiciosWSClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    public RealizarLiquidacionResultDTO liquidarServicios(LiquidacionInDTO liquidarServiciosRequest)
    {
      return this.Channel.liquidarServicios(liquidarServiciosRequest);
    }

    public Task<RealizarLiquidacionResultDTO> liquidarServiciosAsync(LiquidacionInDTO liquidarServiciosRequest)
    {
      return this.Channel.liquidarServiciosAsync(liquidarServiciosRequest);
    }
  }
}

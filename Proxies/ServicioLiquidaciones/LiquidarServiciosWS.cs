﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioLiquidaciones.LiquidarServiciosWS
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ServiceModel;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioLiquidaciones
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [ServiceContract(ConfigurationName = "Proxies.ServicioLiquidaciones.LiquidarServiciosWS", Namespace = "http://ls.recaudos.sirep2.ccb.org.co/LiquidarServicios/")]
  public interface LiquidarServiciosWS
  {
    [OperationContract(Action = "http://ls.recaudos.sirep2.ccb.org.co/LiquidarServicios/liquidarServicios", ReplyAction = "*")]
    [XmlSerializerFormat(Style = OperationFormatStyle.Rpc, SupportFaults = true)]
    [return: MessageParameter(Name = "liquidarServiciosResponse")]
    RealizarLiquidacionResultDTO liquidarServicios(LiquidacionInDTO liquidarServiciosRequest);

    [OperationContract(Action = "http://ls.recaudos.sirep2.ccb.org.co/LiquidarServicios/liquidarServicios", ReplyAction = "*")]
    [return: MessageParameter(Name = "liquidarServiciosResponse")]
    Task<RealizarLiquidacionResultDTO> liquidarServiciosAsync(LiquidacionInDTO liquidarServiciosRequest);
  }
}

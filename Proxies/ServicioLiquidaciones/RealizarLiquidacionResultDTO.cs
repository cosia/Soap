﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioLiquidaciones.RealizarLiquidacionResultDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioLiquidaciones
{
  [GeneratedCode("System.Xml", "4.6.79.0")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://ls.recaudos.sirep2.ccb.org.co/LiquidarServicios/")]
  [Serializable]
  public class RealizarLiquidacionResultDTO : INotifyPropertyChanged
  {
    private double valorTotalField;
    private string idMonedaField;
    private string monedaField;
    private string detalleField;
    private string codigoErrorField;
    private string mensajeErrorField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
    public double valorTotal
    {
      get
      {
        return this.valorTotalField;
      }
      set
      {
        this.valorTotalField = value;
        this.RaisePropertyChanged(nameof (valorTotal));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
    public string idMoneda
    {
      get
      {
        return this.idMonedaField;
      }
      set
      {
        this.idMonedaField = value;
        this.RaisePropertyChanged(nameof (idMoneda));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 2)]
    public string moneda
    {
      get
      {
        return this.monedaField;
      }
      set
      {
        this.monedaField = value;
        this.RaisePropertyChanged(nameof (moneda));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 3)]
    public string detalle
    {
      get
      {
        return this.detalleField;
      }
      set
      {
        this.detalleField = value;
        this.RaisePropertyChanged(nameof (detalle));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 4)]
    public string codigoError
    {
      get
      {
        return this.codigoErrorField;
      }
      set
      {
        this.codigoErrorField = value;
        this.RaisePropertyChanged(nameof (codigoError));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 5)]
    public string mensajeError
    {
      get
      {
        return this.mensajeErrorField;
      }
      set
      {
        this.mensajeErrorField = value;
        this.RaisePropertyChanged(nameof (mensajeError));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

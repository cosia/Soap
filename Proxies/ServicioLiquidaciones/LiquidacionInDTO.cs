﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioLiquidaciones.LiquidacionInDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioLiquidaciones
{
  [GeneratedCode("System.Xml", "4.6.79.0")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://ls.recaudos.sirep2.ccb.org.co/LiquidarServicios/")]
  [Serializable]
  public class LiquidacionInDTO : INotifyPropertyChanged
  {
    private string servicioIdField;
    private string solicitudAplicativoIdField;
    private string usuarioIdField;
    private string detalleField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
    public string servicioId
    {
      get
      {
        return this.servicioIdField;
      }
      set
      {
        this.servicioIdField = value;
        this.RaisePropertyChanged(nameof (servicioId));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
    public string solicitudAplicativoId
    {
      get
      {
        return this.solicitudAplicativoIdField;
      }
      set
      {
        this.solicitudAplicativoIdField = value;
        this.RaisePropertyChanged(nameof (solicitudAplicativoId));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 2)]
    public string usuarioId
    {
      get
      {
        return this.usuarioIdField;
      }
      set
      {
        this.usuarioIdField = value;
        this.RaisePropertyChanged(nameof (usuarioId));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 3)]
    public string detalle
    {
      get
      {
        return this.detalleField;
      }
      set
      {
        this.detalleField = value;
        this.RaisePropertyChanged(nameof (detalle));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

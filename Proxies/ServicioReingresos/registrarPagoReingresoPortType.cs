﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioReingresos.registrarPagoReingresoPortType
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ServiceModel;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioReingresos
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [ServiceContract(ConfigurationName = "Proxies.ServicioReingresos.registrarPagoReingresoPortType", Namespace = "http://ws.recaudos.esb.ccb.org.co")]
  public interface registrarPagoReingresoPortType
  {
    [OperationContract(Action = "", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    registrarPagoReingreso_Output registrarPagoReingreso(registrarPagoReingreso_Input request);

    [OperationContract(Action = "", ReplyAction = "*")]
    Task<registrarPagoReingreso_Output> registrarPagoReingresoAsync(registrarPagoReingreso_Input request);
  }
}

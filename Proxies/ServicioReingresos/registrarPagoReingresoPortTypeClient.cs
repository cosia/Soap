﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioReingresos.registrarPagoReingresoPortTypeClient
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioReingresos
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class registrarPagoReingresoPortTypeClient : ClientBase<registrarPagoReingresoPortType>, registrarPagoReingresoPortType
  {
    public registrarPagoReingresoPortTypeClient()
    {
    }

    public registrarPagoReingresoPortTypeClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public registrarPagoReingresoPortTypeClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public registrarPagoReingresoPortTypeClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public registrarPagoReingresoPortTypeClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    registrarPagoReingreso_Output registrarPagoReingresoPortType.registrarPagoReingreso(registrarPagoReingreso_Input request)
    {
      return this.Channel.registrarPagoReingreso(request);
    }

    public registrarPagoReingresoResponseType registrarPagoReingreso(registrarPagoReingresoType registrarPagoReingreso1)
    {
      return ((registrarPagoReingresoPortType) this).registrarPagoReingreso(new registrarPagoReingreso_Input()
      {
        registrarPagoReingreso = registrarPagoReingreso1
      }).registrarPagoReingresoResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<registrarPagoReingreso_Output> registrarPagoReingresoPortType.registrarPagoReingresoAsync(registrarPagoReingreso_Input request)
    {
      return this.Channel.registrarPagoReingresoAsync(request);
    }

    public Task<registrarPagoReingreso_Output> registrarPagoReingresoAsync(registrarPagoReingresoType registrarPagoReingreso)
    {
      return ((registrarPagoReingresoPortType) this).registrarPagoReingresoAsync(new registrarPagoReingreso_Input()
      {
        registrarPagoReingreso = registrarPagoReingreso
      });
    }
  }
}

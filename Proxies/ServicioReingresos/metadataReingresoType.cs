﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioReingresos.metadataReingresoType
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioReingresos
{
  [GeneratedCode("System.Xml", "4.6.79.0")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://ws.recaudos.esb.ccb.org.co")]
  [Serializable]
  public class metadataReingresoType : INotifyPropertyChanged
  {
    private string transactionIDField;
    private string versionField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
    public string transactionID
    {
      get
      {
        return this.transactionIDField;
      }
      set
      {
        this.transactionIDField = value;
        this.RaisePropertyChanged(nameof (transactionID));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
    public string version
    {
      get
      {
        return this.versionField;
      }
      set
      {
        this.versionField = value;
        this.RaisePropertyChanged(nameof (version));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioMEC.registrarPagoMECPortTypeClient
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioMEC
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class registrarPagoMECPortTypeClient : ClientBase<registrarPagoMECPortType>, registrarPagoMECPortType
  {
    public registrarPagoMECPortTypeClient()
    {
    }

    public registrarPagoMECPortTypeClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public registrarPagoMECPortTypeClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public registrarPagoMECPortTypeClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public registrarPagoMECPortTypeClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    registrarPagoMEC_Output registrarPagoMECPortType.registrarPagoMEC(registrarPagoMEC_Input request)
    {
      return this.Channel.registrarPagoMEC(request);
    }

    public registrarPagoMECResponseType registrarPagoMEC(registrarPagoMECType registrarPagoMEC1)
    {
      return ((registrarPagoMECPortType) this).registrarPagoMEC(new registrarPagoMEC_Input()
      {
        registrarPagoMEC = registrarPagoMEC1
      }).registrarPagoMECResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<registrarPagoMEC_Output> registrarPagoMECPortType.registrarPagoMECAsync(registrarPagoMEC_Input request)
    {
      return this.Channel.registrarPagoMECAsync(request);
    }

    public Task<registrarPagoMEC_Output> registrarPagoMECAsync(registrarPagoMECType registrarPagoMEC)
    {
      return ((registrarPagoMECPortType) this).registrarPagoMECAsync(new registrarPagoMEC_Input()
      {
        registrarPagoMEC = registrarPagoMEC
      });
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioMEC.registrarPagoMECPortType
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ServiceModel;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioMEC
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [ServiceContract(ConfigurationName = "Proxies.ServicioMEC.registrarPagoMECPortType", Namespace = "http://ws.recaudos.esb.ccb.org.co")]
  public interface registrarPagoMECPortType
  {
    [OperationContract(Action = "", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    registrarPagoMEC_Output registrarPagoMEC(registrarPagoMEC_Input request);

    [OperationContract(Action = "", ReplyAction = "*")]
    Task<registrarPagoMEC_Output> registrarPagoMECAsync(registrarPagoMEC_Input request);
  }
}

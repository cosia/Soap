﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.SolicitanteDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [GeneratedCode("System.Xml", "4.0.30319.34230")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://model.ws.recaudos.sirep2.ccb.org.co")]
  [Serializable]
  public class SolicitanteDTO : INotifyPropertyChanged
  {
    private string documentoField;
    private string tipoIdentificacionField;
    private string numIdentificacionField;
    private string nombre1Field;
    private string nombre2Field;
    private string apellido1Field;
    private string apellido2Field;
    private string razonSocialField;
    private string telefonoField;
    private string emailField;
    private string idMunicipioField;
    private string direccionField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 0)]
    public string documento
    {
      get
      {
        return this.documentoField;
      }
      set
      {
        this.documentoField = value;
        this.RaisePropertyChanged(nameof (documento));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 1)]
    public string tipoIdentificacion
    {
      get
      {
        return this.tipoIdentificacionField;
      }
      set
      {
        this.tipoIdentificacionField = value;
        this.RaisePropertyChanged(nameof (tipoIdentificacion));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 2)]
    public string numIdentificacion
    {
      get
      {
        return this.numIdentificacionField;
      }
      set
      {
        this.numIdentificacionField = value;
        this.RaisePropertyChanged(nameof (numIdentificacion));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 3)]
    public string nombre1
    {
      get
      {
        return this.nombre1Field;
      }
      set
      {
        this.nombre1Field = value;
        this.RaisePropertyChanged(nameof (nombre1));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 4)]
    public string nombre2
    {
      get
      {
        return this.nombre2Field;
      }
      set
      {
        this.nombre2Field = value;
        this.RaisePropertyChanged(nameof (nombre2));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 5)]
    public string apellido1
    {
      get
      {
        return this.apellido1Field;
      }
      set
      {
        this.apellido1Field = value;
        this.RaisePropertyChanged(nameof (apellido1));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 6)]
    public string apellido2
    {
      get
      {
        return this.apellido2Field;
      }
      set
      {
        this.apellido2Field = value;
        this.RaisePropertyChanged(nameof (apellido2));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 7)]
    public string razonSocial
    {
      get
      {
        return this.razonSocialField;
      }
      set
      {
        this.razonSocialField = value;
        this.RaisePropertyChanged(nameof (razonSocial));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 8)]
    public string telefono
    {
      get
      {
        return this.telefonoField;
      }
      set
      {
        this.telefonoField = value;
        this.RaisePropertyChanged(nameof (telefono));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 9)]
    public string email
    {
      get
      {
        return this.emailField;
      }
      set
      {
        this.emailField = value;
        this.RaisePropertyChanged(nameof (email));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 10)]
    public string idMunicipio
    {
      get
      {
        return this.idMunicipioField;
      }
      set
      {
        this.idMunicipioField = value;
        this.RaisePropertyChanged(nameof (idMunicipio));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 11)]
    public string direccion
    {
      get
      {
        return this.direccionField;
      }
      set
      {
        this.direccionField = value;
        this.RaisePropertyChanged(nameof (direccion));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.LiquidarOutDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [GeneratedCode("System.Xml", "4.0.30319.34230")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://model.ws.recaudos.sirep2.ccb.org.co")]
  [Serializable]
  public class LiquidarOutDTO : INotifyPropertyChanged
  {
    private string idMonedaField;
    private string descMonedaField;
    private double? totalOperacionField;
    private LiquidacionDTO[] liquidacionField;
    private string mensajeErrorField;
    private string codigoErrorField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 0)]
    public string idMoneda
    {
      get
      {
        return this.idMonedaField;
      }
      set
      {
        this.idMonedaField = value;
        this.RaisePropertyChanged(nameof (idMoneda));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 1)]
    public string descMoneda
    {
      get
      {
        return this.descMonedaField;
      }
      set
      {
        this.descMonedaField = value;
        this.RaisePropertyChanged(nameof (descMoneda));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 2)]
    public double? totalOperacion
    {
      get
      {
        return this.totalOperacionField;
      }
      set
      {
        this.totalOperacionField = value;
        this.RaisePropertyChanged(nameof (totalOperacion));
      }
    }

    [XmlArray(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 3)]
    [XmlArrayItem(Form = XmlSchemaForm.Unqualified)]
    public LiquidacionDTO[] liquidacion
    {
      get
      {
        return this.liquidacionField;
      }
      set
      {
        this.liquidacionField = value;
        this.RaisePropertyChanged(nameof (liquidacion));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 4)]
    public string mensajeError
    {
      get
      {
        return this.mensajeErrorField;
      }
      set
      {
        this.mensajeErrorField = value;
        this.RaisePropertyChanged(nameof (mensajeError));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 5)]
    public string codigoError
    {
      get
      {
        return this.codigoErrorField;
      }
      set
      {
        this.codigoErrorField = value;
        this.RaisePropertyChanged(nameof (codigoError));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

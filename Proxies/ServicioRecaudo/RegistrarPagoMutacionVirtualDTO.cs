﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.RegistrarPagoMutacionVirtualDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [GeneratedCode("System.Xml", "4.0.30319.34230")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://model.ws.recaudos.sirep2.ccb.org.co")]
  [Serializable]
  public class RegistrarPagoMutacionVirtualDTO : INotifyPropertyChanged
  {
    private string idSucursalField;
    private string idUsuarioField;
    private string numOrdenPagoField;
    private string idSolicitudField;
    private int? numClientePagoField;
    private int? numClienteContactoField;
    private string nombreRazonSocialField;
    private DatosDocumentoMutacionDTO[] datosDocumentosField;
    private CiiuDTO[] listaCodigosCiiuField;
    private DireccionDTO[] listaDireccionesField;
    private FormaPagoVirtualDTO formaPagoField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 0)]
    public string idSucursal
    {
      get
      {
        return this.idSucursalField;
      }
      set
      {
        this.idSucursalField = value;
        this.RaisePropertyChanged(nameof (idSucursal));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 1)]
    public string idUsuario
    {
      get
      {
        return this.idUsuarioField;
      }
      set
      {
        this.idUsuarioField = value;
        this.RaisePropertyChanged(nameof (idUsuario));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 2)]
    public string numOrdenPago
    {
      get
      {
        return this.numOrdenPagoField;
      }
      set
      {
        this.numOrdenPagoField = value;
        this.RaisePropertyChanged(nameof (numOrdenPago));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 3)]
    public string idSolicitud
    {
      get
      {
        return this.idSolicitudField;
      }
      set
      {
        this.idSolicitudField = value;
        this.RaisePropertyChanged(nameof (idSolicitud));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 4)]
    public int? numClientePago
    {
      get
      {
        return this.numClientePagoField;
      }
      set
      {
        this.numClientePagoField = value;
        this.RaisePropertyChanged(nameof (numClientePago));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 5)]
    public int? numClienteContacto
    {
      get
      {
        return this.numClienteContactoField;
      }
      set
      {
        this.numClienteContactoField = value;
        this.RaisePropertyChanged(nameof (numClienteContacto));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 6)]
    public string nombreRazonSocial
    {
      get
      {
        return this.nombreRazonSocialField;
      }
      set
      {
        this.nombreRazonSocialField = value;
        this.RaisePropertyChanged(nameof (nombreRazonSocial));
      }
    }

    [XmlArray(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 7)]
    [XmlArrayItem(Form = XmlSchemaForm.Unqualified)]
    public DatosDocumentoMutacionDTO[] datosDocumentos
    {
      get
      {
        return this.datosDocumentosField;
      }
      set
      {
        this.datosDocumentosField = value;
        this.RaisePropertyChanged(nameof (datosDocumentos));
      }
    }

    [XmlArray(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 8)]
    [XmlArrayItem(Form = XmlSchemaForm.Unqualified)]
    public CiiuDTO[] listaCodigosCiiu
    {
      get
      {
        return this.listaCodigosCiiuField;
      }
      set
      {
        this.listaCodigosCiiuField = value;
        this.RaisePropertyChanged(nameof (listaCodigosCiiu));
      }
    }

    [XmlArray(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 9)]
    [XmlArrayItem(Form = XmlSchemaForm.Unqualified)]
    public DireccionDTO[] listaDirecciones
    {
      get
      {
        return this.listaDireccionesField;
      }
      set
      {
        this.listaDireccionesField = value;
        this.RaisePropertyChanged(nameof (listaDirecciones));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 10)]
    public FormaPagoVirtualDTO formaPago
    {
      get
      {
        return this.formaPagoField;
      }
      set
      {
        this.formaPagoField = value;
        this.RaisePropertyChanged(nameof (formaPago));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.OrdenPagoServiciosDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [GeneratedCode("System.Xml", "4.0.30319.34230")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://model.ws.recaudos.sirep2.ccb.org.co")]
  [Serializable]
  public class OrdenPagoServiciosDTO : INotifyPropertyChanged
  {
    private int? itemField;
    private string idServicioField;
    private string nomServicioField;
    private int? cntServicioField;
    private double? vrBaseField;
    private double? vrServicioField;
    private string idServicioBaseField;
    private string fechaBaseField;
    private string numMatriculaField;
    private int? cntPersonalField;
    private string idLibroField;
    private string idActoField;
    private int? itemPadreField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 0)]
    public int? item
    {
      get
      {
        return this.itemField;
      }
      set
      {
        this.itemField = value;
        this.RaisePropertyChanged(nameof (item));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 1)]
    public string idServicio
    {
      get
      {
        return this.idServicioField;
      }
      set
      {
        this.idServicioField = value;
        this.RaisePropertyChanged(nameof (idServicio));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 2)]
    public string nomServicio
    {
      get
      {
        return this.nomServicioField;
      }
      set
      {
        this.nomServicioField = value;
        this.RaisePropertyChanged(nameof (nomServicio));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 3)]
    public int? cntServicio
    {
      get
      {
        return this.cntServicioField;
      }
      set
      {
        this.cntServicioField = value;
        this.RaisePropertyChanged(nameof (cntServicio));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 4)]
    public double? vrBase
    {
      get
      {
        return this.vrBaseField;
      }
      set
      {
        this.vrBaseField = value;
        this.RaisePropertyChanged(nameof (vrBase));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 5)]
    public double? vrServicio
    {
      get
      {
        return this.vrServicioField;
      }
      set
      {
        this.vrServicioField = value;
        this.RaisePropertyChanged(nameof (vrServicio));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 6)]
    public string idServicioBase
    {
      get
      {
        return this.idServicioBaseField;
      }
      set
      {
        this.idServicioBaseField = value;
        this.RaisePropertyChanged(nameof (idServicioBase));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 7)]
    public string fechaBase
    {
      get
      {
        return this.fechaBaseField;
      }
      set
      {
        this.fechaBaseField = value;
        this.RaisePropertyChanged(nameof (fechaBase));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 8)]
    public string numMatricula
    {
      get
      {
        return this.numMatriculaField;
      }
      set
      {
        this.numMatriculaField = value;
        this.RaisePropertyChanged(nameof (numMatricula));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 9)]
    public int? cntPersonal
    {
      get
      {
        return this.cntPersonalField;
      }
      set
      {
        this.cntPersonalField = value;
        this.RaisePropertyChanged(nameof (cntPersonal));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 10)]
    public string idLibro
    {
      get
      {
        return this.idLibroField;
      }
      set
      {
        this.idLibroField = value;
        this.RaisePropertyChanged(nameof (idLibro));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 11)]
    public string idActo
    {
      get
      {
        return this.idActoField;
      }
      set
      {
        this.idActoField = value;
        this.RaisePropertyChanged(nameof (idActo));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 12)]
    public int? itemPadre
    {
      get
      {
        return this.itemPadreField;
      }
      set
      {
        this.itemPadreField = value;
        this.RaisePropertyChanged(nameof (itemPadre));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

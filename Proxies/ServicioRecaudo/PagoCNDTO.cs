﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.PagoCNDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [GeneratedCode("System.Xml", "4.0.30319.34230")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://model.ws.recaudos.sirep2.ccb.org.co")]
  [Serializable]
  public class PagoCNDTO : INotifyPropertyChanged
  {
    private string numReciboField;
    private string[] numTramiteField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 0)]
    public string numRecibo
    {
      get
      {
        return this.numReciboField;
      }
      set
      {
        this.numReciboField = value;
        this.RaisePropertyChanged(nameof (numRecibo));
      }
    }

    [XmlArray(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 1)]
    [XmlArrayItem(Form = XmlSchemaForm.Unqualified)]
    public string[] numTramite
    {
      get
      {
        return this.numTramiteField;
      }
      set
      {
        this.numTramiteField = value;
        this.RaisePropertyChanged(nameof (numTramite));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

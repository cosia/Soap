﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.DatosDocumentoMutacionDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [GeneratedCode("System.Xml", "4.0.30319.34230")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://model.ws.recaudos.sirep2.ccb.org.co")]
  [Serializable]
  public class DatosDocumentoMutacionDTO : INotifyPropertyChanged
  {
    private string idServicioNegocioField;
    private string origenDocumentoField;
    private string tipoDocumentoField;
    private string numeroDocumentoField;
    private string fechaDocumentoField;
    private string municipioDocumentoField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 0)]
    public string idServicioNegocio
    {
      get
      {
        return this.idServicioNegocioField;
      }
      set
      {
        this.idServicioNegocioField = value;
        this.RaisePropertyChanged(nameof (idServicioNegocio));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 1)]
    public string origenDocumento
    {
      get
      {
        return this.origenDocumentoField;
      }
      set
      {
        this.origenDocumentoField = value;
        this.RaisePropertyChanged(nameof (origenDocumento));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 2)]
    public string tipoDocumento
    {
      get
      {
        return this.tipoDocumentoField;
      }
      set
      {
        this.tipoDocumentoField = value;
        this.RaisePropertyChanged(nameof (tipoDocumento));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 3)]
    public string numeroDocumento
    {
      get
      {
        return this.numeroDocumentoField;
      }
      set
      {
        this.numeroDocumentoField = value;
        this.RaisePropertyChanged(nameof (numeroDocumento));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 4)]
    public string fechaDocumento
    {
      get
      {
        return this.fechaDocumentoField;
      }
      set
      {
        this.fechaDocumentoField = value;
        this.RaisePropertyChanged(nameof (fechaDocumento));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 5)]
    public string municipioDocumento
    {
      get
      {
        return this.municipioDocumentoField;
      }
      set
      {
        this.municipioDocumentoField = value;
        this.RaisePropertyChanged(nameof (municipioDocumento));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

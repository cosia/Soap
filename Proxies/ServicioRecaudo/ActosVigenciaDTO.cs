﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.ActosVigenciaDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [GeneratedCode("System.Xml", "4.0.30319.34230")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://model.ws.recaudos.sirep2.ccb.org.co")]
  [Serializable]
  public class ActosVigenciaDTO : INotifyPropertyChanged
  {
    private string fechaDocumentoField;
    private int? ctrEspecialField;
    private string idOrigenDocumentoField;
    private LibroActoDTO[] actosField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 0)]
    public string fechaDocumento
    {
      get
      {
        return this.fechaDocumentoField;
      }
      set
      {
        this.fechaDocumentoField = value;
        this.RaisePropertyChanged(nameof (fechaDocumento));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 1)]
    public int? ctrEspecial
    {
      get
      {
        return this.ctrEspecialField;
      }
      set
      {
        this.ctrEspecialField = value;
        this.RaisePropertyChanged(nameof (ctrEspecial));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 2)]
    public string idOrigenDocumento
    {
      get
      {
        return this.idOrigenDocumentoField;
      }
      set
      {
        this.idOrigenDocumentoField = value;
        this.RaisePropertyChanged(nameof (idOrigenDocumento));
      }
    }

    [XmlArray(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 3)]
    [XmlArrayItem(Form = XmlSchemaForm.Unqualified)]
    public LibroActoDTO[] actos
    {
      get
      {
        return this.actosField;
      }
      set
      {
        this.actosField = value;
        this.RaisePropertyChanged(nameof (actos));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

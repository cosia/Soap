﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.generarImagenesNumTramiteResponse
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [EditorBrowsable(EditorBrowsableState.Advanced)]
  [MessageContract(IsWrapped = true, WrapperName = "generarImagenesNumTramiteResponse", WrapperNamespace = "http://ws.recaudos.sirep2.ccb.org.co")]
  public class generarImagenesNumTramiteResponse
  {
    [MessageBodyMember(Namespace = "http://ws.recaudos.sirep2.ccb.org.co", Order = 0)]
    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true)]
    public ImagenTramiteDTO generarImagenesNumTramiteReturn;

    public generarImagenesNumTramiteResponse()
    {
    }

    public generarImagenesNumTramiteResponse(ImagenTramiteDTO generarImagenesNumTramiteReturn)
    {
      this.generarImagenesNumTramiteReturn = generarImagenesNumTramiteReturn;
    }
  }
}

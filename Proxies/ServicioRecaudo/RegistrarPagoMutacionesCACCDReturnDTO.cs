﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.RegistrarPagoMutacionesCACCDReturnDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [GeneratedCode("System.Xml", "4.0.30319.34230")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://model.ws.recaudos.sirep2.ccb.org.co")]
  [Serializable]
  public class RegistrarPagoMutacionesCACCDReturnDTO : INotifyPropertyChanged
  {
    private string mensajeErrorField;
    private string codeErrorField;
    private PagoCNDTO pagoCNDTOField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 0)]
    public string mensajeError
    {
      get
      {
        return this.mensajeErrorField;
      }
      set
      {
        this.mensajeErrorField = value;
        this.RaisePropertyChanged(nameof (mensajeError));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 1)]
    public string codeError
    {
      get
      {
        return this.codeErrorField;
      }
      set
      {
        this.codeErrorField = value;
        this.RaisePropertyChanged(nameof (codeError));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 2)]
    public PagoCNDTO pagoCNDTO
    {
      get
      {
        return this.pagoCNDTOField;
      }
      set
      {
        this.pagoCNDTOField = value;
        this.RaisePropertyChanged(nameof (pagoCNDTO));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

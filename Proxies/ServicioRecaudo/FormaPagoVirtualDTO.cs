﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.FormaPagoVirtualDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [GeneratedCode("System.Xml", "4.0.30319.34230")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://model.ws.recaudos.sirep2.ccb.org.co")]
  [Serializable]
  public class FormaPagoVirtualDTO : INotifyPropertyChanged
  {
    private int? itemField;
    private string idFormaPagoField;
    private double? vrPagoField;
    private string idBancoField;
    private string idTipoTarjetaField;
    private string numDocumentoField;
    private string numAutorizaField;
    private string fechaConsignacionField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 0)]
    public int? item
    {
      get
      {
        return this.itemField;
      }
      set
      {
        this.itemField = value;
        this.RaisePropertyChanged(nameof (item));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 1)]
    public string idFormaPago
    {
      get
      {
        return this.idFormaPagoField;
      }
      set
      {
        this.idFormaPagoField = value;
        this.RaisePropertyChanged(nameof (idFormaPago));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 2)]
    public double? vrPago
    {
      get
      {
        return this.vrPagoField;
      }
      set
      {
        this.vrPagoField = value;
        this.RaisePropertyChanged(nameof (vrPago));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 3)]
    public string idBanco
    {
      get
      {
        return this.idBancoField;
      }
      set
      {
        this.idBancoField = value;
        this.RaisePropertyChanged(nameof (idBanco));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 4)]
    public string idTipoTarjeta
    {
      get
      {
        return this.idTipoTarjetaField;
      }
      set
      {
        this.idTipoTarjetaField = value;
        this.RaisePropertyChanged(nameof (idTipoTarjeta));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 5)]
    public string numDocumento
    {
      get
      {
        return this.numDocumentoField;
      }
      set
      {
        this.numDocumentoField = value;
        this.RaisePropertyChanged(nameof (numDocumento));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 6)]
    public string numAutoriza
    {
      get
      {
        return this.numAutorizaField;
      }
      set
      {
        this.numAutorizaField = value;
        this.RaisePropertyChanged(nameof (numAutoriza));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 7)]
    public string fechaConsignacion
    {
      get
      {
        return this.fechaConsignacionField;
      }
      set
      {
        this.fechaConsignacionField = value;
        this.RaisePropertyChanged(nameof (fechaConsignacion));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

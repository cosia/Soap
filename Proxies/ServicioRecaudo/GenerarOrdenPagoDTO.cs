﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.GenerarOrdenPagoDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [GeneratedCode("System.Xml", "4.0.30319.34230")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://model.ws.recaudos.sirep2.ccb.org.co")]
  [Serializable]
  public class GenerarOrdenPagoDTO : INotifyPropertyChanged
  {
    private string numOrdenPagoField;
    private string idSucursalField;
    private string idUsuarioField;
    private int? numClientePagoField;
    private int? ctrReliquidacionField;
    private string fecVencimientoField;
    private string observacionField;
    private string idServicioNegocioField;
    private int? idSolicitudBPMField;
    private string numTramiteField;
    private LiquidarOutDTO datosLiquidacionField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 0)]
    public string numOrdenPago
    {
      get
      {
        return this.numOrdenPagoField;
      }
      set
      {
        this.numOrdenPagoField = value;
        this.RaisePropertyChanged(nameof (numOrdenPago));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 1)]
    public string idSucursal
    {
      get
      {
        return this.idSucursalField;
      }
      set
      {
        this.idSucursalField = value;
        this.RaisePropertyChanged(nameof (idSucursal));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 2)]
    public string idUsuario
    {
      get
      {
        return this.idUsuarioField;
      }
      set
      {
        this.idUsuarioField = value;
        this.RaisePropertyChanged(nameof (idUsuario));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 3)]
    public int? numClientePago
    {
      get
      {
        return this.numClientePagoField;
      }
      set
      {
        this.numClientePagoField = value;
        this.RaisePropertyChanged(nameof (numClientePago));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 4)]
    public int? ctrReliquidacion
    {
      get
      {
        return this.ctrReliquidacionField;
      }
      set
      {
        this.ctrReliquidacionField = value;
        this.RaisePropertyChanged(nameof (ctrReliquidacion));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 5)]
    public string fecVencimiento
    {
      get
      {
        return this.fecVencimientoField;
      }
      set
      {
        this.fecVencimientoField = value;
        this.RaisePropertyChanged(nameof (fecVencimiento));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 6)]
    public string observacion
    {
      get
      {
        return this.observacionField;
      }
      set
      {
        this.observacionField = value;
        this.RaisePropertyChanged(nameof (observacion));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 7)]
    public string idServicioNegocio
    {
      get
      {
        return this.idServicioNegocioField;
      }
      set
      {
        this.idServicioNegocioField = value;
        this.RaisePropertyChanged(nameof (idServicioNegocio));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 8)]
    public int? idSolicitudBPM
    {
      get
      {
        return this.idSolicitudBPMField;
      }
      set
      {
        this.idSolicitudBPMField = value;
        this.RaisePropertyChanged(nameof (idSolicitudBPM));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 9)]
    public string numTramite
    {
      get
      {
        return this.numTramiteField;
      }
      set
      {
        this.numTramiteField = value;
        this.RaisePropertyChanged(nameof (numTramite));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 10)]
    public LiquidarOutDTO datosLiquidacion
    {
      get
      {
        return this.datosLiquidacionField;
      }
      set
      {
        this.datosLiquidacionField = value;
        this.RaisePropertyChanged(nameof (datosLiquidacion));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

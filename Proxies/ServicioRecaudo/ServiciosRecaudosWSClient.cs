﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.ServiciosRecaudosWSClient
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class ServiciosRecaudosWSClient : ClientBase<ServiciosRecaudosWS>, ServiciosRecaudosWS
  {
    public ServiciosRecaudosWSClient()
    {
    }

    public ServiciosRecaudosWSClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public ServiciosRecaudosWSClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public ServiciosRecaudosWSClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public ServiciosRecaudosWSClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    consultarDatosSolicitudResponse ServiciosRecaudosWS.consultarDatosSolicitud(consultarDatosSolicitudRequest request)
    {
      return this.Channel.consultarDatosSolicitud(request);
    }

    public DatosSolicitudResponseDTO consultarDatosSolicitud(string numSolicitud)
    {
      return ((ServiciosRecaudosWS) this).consultarDatosSolicitud(new consultarDatosSolicitudRequest()
      {
        numSolicitud = numSolicitud
      }).consultarDatosSolicitudReturn;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<consultarDatosSolicitudResponse> ServiciosRecaudosWS.consultarDatosSolicitudAsync(consultarDatosSolicitudRequest request)
    {
      return this.Channel.consultarDatosSolicitudAsync(request);
    }

    public Task<consultarDatosSolicitudResponse> consultarDatosSolicitudAsync(string numSolicitud)
    {
      return ((ServiciosRecaudosWS) this).consultarDatosSolicitudAsync(new consultarDatosSolicitudRequest()
      {
        numSolicitud = numSolicitud
      });
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    consultarOrdenPagoResponse ServiciosRecaudosWS.consultarOrdenPago(consultarOrdenPagoRequest request)
    {
      return this.Channel.consultarOrdenPago(request);
    }

    public DatosOrdenPagoOutDTO consultarOrdenPago(string numOrdenPago)
    {
      return ((ServiciosRecaudosWS) this).consultarOrdenPago(new consultarOrdenPagoRequest()
      {
        numOrdenPago = numOrdenPago
      }).consultarOrdenPagoReturn;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<consultarOrdenPagoResponse> ServiciosRecaudosWS.consultarOrdenPagoAsync(consultarOrdenPagoRequest request)
    {
      return this.Channel.consultarOrdenPagoAsync(request);
    }

    public Task<consultarOrdenPagoResponse> consultarOrdenPagoAsync(string numOrdenPago)
    {
      return ((ServiciosRecaudosWS) this).consultarOrdenPagoAsync(new consultarOrdenPagoRequest()
      {
        numOrdenPago = numOrdenPago
      });
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    generarOrdenPagoVirtualResponse ServiciosRecaudosWS.generarOrdenPagoVirtual(generarOrdenPagoVirtualRequest request)
    {
      return this.Channel.generarOrdenPagoVirtual(request);
    }

    public GenerarOrdenPagoResponseDTO generarOrdenPagoVirtual(OrdenPagoDTO ordenPagoDTO)
    {
      return ((ServiciosRecaudosWS) this).generarOrdenPagoVirtual(new generarOrdenPagoVirtualRequest()
      {
        ordenPagoDTO = ordenPagoDTO
      }).generarOrdenPagoVirtualReturn;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<generarOrdenPagoVirtualResponse> ServiciosRecaudosWS.generarOrdenPagoVirtualAsync(generarOrdenPagoVirtualRequest request)
    {
      return this.Channel.generarOrdenPagoVirtualAsync(request);
    }

    public Task<generarOrdenPagoVirtualResponse> generarOrdenPagoVirtualAsync(OrdenPagoDTO ordenPagoDTO)
    {
      return ((ServiciosRecaudosWS) this).generarOrdenPagoVirtualAsync(new generarOrdenPagoVirtualRequest()
      {
        ordenPagoDTO = ordenPagoDTO
      });
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    registrarOrdenPagoResponse ServiciosRecaudosWS.registrarOrdenPago(registrarOrdenPagoRequest request)
    {
      return this.Channel.registrarOrdenPago(request);
    }

    public GenerarOrdenPagoResponseDTO registrarOrdenPago(GenerarOrdenPagoDTO generarOrdenPagoDTO)
    {
      return ((ServiciosRecaudosWS) this).registrarOrdenPago(new registrarOrdenPagoRequest()
      {
        generarOrdenPagoDTO = generarOrdenPagoDTO
      }).registrarOrdenPagoReturn;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<registrarOrdenPagoResponse> ServiciosRecaudosWS.registrarOrdenPagoAsync(registrarOrdenPagoRequest request)
    {
      return this.Channel.registrarOrdenPagoAsync(request);
    }

    public Task<registrarOrdenPagoResponse> registrarOrdenPagoAsync(GenerarOrdenPagoDTO generarOrdenPagoDTO)
    {
      return ((ServiciosRecaudosWS) this).registrarOrdenPagoAsync(new registrarOrdenPagoRequest()
      {
        generarOrdenPagoDTO = generarOrdenPagoDTO
      });
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    liquidarServiciosMutacionResponse ServiciosRecaudosWS.liquidarServiciosMutacion(liquidarServiciosMutacionRequest request)
    {
      return this.Channel.liquidarServiciosMutacion(request);
    }

    public LiquidarOutDTO liquidarServiciosMutacion(LiquidaMutacionDTO liquidaMutacionDTO)
    {
      return ((ServiciosRecaudosWS) this).liquidarServiciosMutacion(new liquidarServiciosMutacionRequest()
      {
        liquidaMutacionDTO = liquidaMutacionDTO
      }).liquidarServiciosMutacionReturn;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<liquidarServiciosMutacionResponse> ServiciosRecaudosWS.liquidarServiciosMutacionAsync(liquidarServiciosMutacionRequest request)
    {
      return this.Channel.liquidarServiciosMutacionAsync(request);
    }

    public Task<liquidarServiciosMutacionResponse> liquidarServiciosMutacionAsync(LiquidaMutacionDTO liquidaMutacionDTO)
    {
      return ((ServiciosRecaudosWS) this).liquidarServiciosMutacionAsync(new liquidarServiciosMutacionRequest()
      {
        liquidaMutacionDTO = liquidaMutacionDTO
      });
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    generarImagenesNumTramiteResponse ServiciosRecaudosWS.generarImagenesNumTramite(generarImagenesNumTramiteRequest request)
    {
      return this.Channel.generarImagenesNumTramite(request);
    }

    public ImagenTramiteDTO generarImagenesNumTramite(string numTramite)
    {
      return ((ServiciosRecaudosWS) this).generarImagenesNumTramite(new generarImagenesNumTramiteRequest()
      {
        numTramite = numTramite
      }).generarImagenesNumTramiteReturn;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<generarImagenesNumTramiteResponse> ServiciosRecaudosWS.generarImagenesNumTramiteAsync(generarImagenesNumTramiteRequest request)
    {
      return this.Channel.generarImagenesNumTramiteAsync(request);
    }

    public Task<generarImagenesNumTramiteResponse> generarImagenesNumTramiteAsync(string numTramite)
    {
      return ((ServiciosRecaudosWS) this).generarImagenesNumTramiteAsync(new generarImagenesNumTramiteRequest()
      {
        numTramite = numTramite
      });
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    registrarPagoVirtualMutacionesCACCDResponse ServiciosRecaudosWS.registrarPagoVirtualMutacionesCACCD(registrarPagoVirtualMutacionesCACCDRequest request)
    {
      return this.Channel.registrarPagoVirtualMutacionesCACCD(request);
    }

    public RegistrarPagoMutacionesCACCDReturnDTO registrarPagoVirtualMutacionesCACCD(RegistrarPagoMutacionVirtualDTO mutacionVirtualDTO)
    {
      return ((ServiciosRecaudosWS) this).registrarPagoVirtualMutacionesCACCD(new registrarPagoVirtualMutacionesCACCDRequest()
      {
        mutacionVirtualDTO = mutacionVirtualDTO
      }).registrarPagoVirtualMutacionesCACCDReturn;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<registrarPagoVirtualMutacionesCACCDResponse> ServiciosRecaudosWS.registrarPagoVirtualMutacionesCACCDAsync(registrarPagoVirtualMutacionesCACCDRequest request)
    {
      return this.Channel.registrarPagoVirtualMutacionesCACCDAsync(request);
    }

    public Task<registrarPagoVirtualMutacionesCACCDResponse> registrarPagoVirtualMutacionesCACCDAsync(RegistrarPagoMutacionVirtualDTO mutacionVirtualDTO)
    {
      return ((ServiciosRecaudosWS) this).registrarPagoVirtualMutacionesCACCDAsync(new registrarPagoVirtualMutacionesCACCDRequest()
      {
        mutacionVirtualDTO = mutacionVirtualDTO
      });
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    registrarPagoVirtualMutacionesCNResponse ServiciosRecaudosWS.registrarPagoVirtualMutacionesCN(registrarPagoVirtualMutacionesCNRequest request)
    {
      return this.Channel.registrarPagoVirtualMutacionesCN(request);
    }

    public registrarPagoVirtualMutacionesCNReturnDTO registrarPagoVirtualMutacionesCN(RegistrarPagoMutacionVirtualDTO mutacionVirtualDTO)
    {
      return ((ServiciosRecaudosWS) this).registrarPagoVirtualMutacionesCN(new registrarPagoVirtualMutacionesCNRequest()
      {
        mutacionVirtualDTO = mutacionVirtualDTO
      }).registrarPagoVirtualMutacionesCNReturn;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<registrarPagoVirtualMutacionesCNResponse> ServiciosRecaudosWS.registrarPagoVirtualMutacionesCNAsync(registrarPagoVirtualMutacionesCNRequest request)
    {
      return this.Channel.registrarPagoVirtualMutacionesCNAsync(request);
    }

    public Task<registrarPagoVirtualMutacionesCNResponse> registrarPagoVirtualMutacionesCNAsync(RegistrarPagoMutacionVirtualDTO mutacionVirtualDTO)
    {
      return ((ServiciosRecaudosWS) this).registrarPagoVirtualMutacionesCNAsync(new registrarPagoVirtualMutacionesCNRequest()
      {
        mutacionVirtualDTO = mutacionVirtualDTO
      });
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.ServiciosRecaudosWS
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ServiceModel;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [ServiceContract(ConfigurationName = "Proxies.ServicioRecaudo.ServiciosRecaudosWS", Namespace = "http://ws.recaudos.sirep2.ccb.org.co")]
  public interface ServiciosRecaudosWS
  {
    [OperationContract(Action = "consultarDatosSolicitud", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    [return: MessageParameter(Name = "consultarDatosSolicitudReturn")]
    consultarDatosSolicitudResponse consultarDatosSolicitud(consultarDatosSolicitudRequest request);

    [OperationContract(Action = "consultarDatosSolicitud", ReplyAction = "*")]
    Task<consultarDatosSolicitudResponse> consultarDatosSolicitudAsync(consultarDatosSolicitudRequest request);

    [OperationContract(Action = "consultarOrdenPago", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    [return: MessageParameter(Name = "consultarOrdenPagoReturn")]
    consultarOrdenPagoResponse consultarOrdenPago(consultarOrdenPagoRequest request);

    [OperationContract(Action = "consultarOrdenPago", ReplyAction = "*")]
    Task<consultarOrdenPagoResponse> consultarOrdenPagoAsync(consultarOrdenPagoRequest request);

    [OperationContract(Action = "generarOrdenPagoVirtual", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    [return: MessageParameter(Name = "generarOrdenPagoVirtualReturn")]
    generarOrdenPagoVirtualResponse generarOrdenPagoVirtual(generarOrdenPagoVirtualRequest request);

    [OperationContract(Action = "generarOrdenPagoVirtual", ReplyAction = "*")]
    Task<generarOrdenPagoVirtualResponse> generarOrdenPagoVirtualAsync(generarOrdenPagoVirtualRequest request);

    [OperationContract(Action = "registrarOrdenPago", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    [return: MessageParameter(Name = "registrarOrdenPagoReturn")]
    registrarOrdenPagoResponse registrarOrdenPago(registrarOrdenPagoRequest request);

    [OperationContract(Action = "registrarOrdenPago", ReplyAction = "*")]
    Task<registrarOrdenPagoResponse> registrarOrdenPagoAsync(registrarOrdenPagoRequest request);

    [OperationContract(Action = "liquidarServiciosMutacion", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    [return: MessageParameter(Name = "liquidarServiciosMutacionReturn")]
    liquidarServiciosMutacionResponse liquidarServiciosMutacion(liquidarServiciosMutacionRequest request);

    [OperationContract(Action = "liquidarServiciosMutacion", ReplyAction = "*")]
    Task<liquidarServiciosMutacionResponse> liquidarServiciosMutacionAsync(liquidarServiciosMutacionRequest request);

    [OperationContract(Action = "generarImagenesNumTramite", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    [return: MessageParameter(Name = "generarImagenesNumTramiteReturn")]
    generarImagenesNumTramiteResponse generarImagenesNumTramite(generarImagenesNumTramiteRequest request);

    [OperationContract(Action = "generarImagenesNumTramite", ReplyAction = "*")]
    Task<generarImagenesNumTramiteResponse> generarImagenesNumTramiteAsync(generarImagenesNumTramiteRequest request);

    [OperationContract(Action = "registrarPagoVirtualMutacionesCACCD", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    [return: MessageParameter(Name = "registrarPagoVirtualMutacionesCACCDReturn")]
    registrarPagoVirtualMutacionesCACCDResponse registrarPagoVirtualMutacionesCACCD(registrarPagoVirtualMutacionesCACCDRequest request);

    [OperationContract(Action = "registrarPagoVirtualMutacionesCACCD", ReplyAction = "*")]
    Task<registrarPagoVirtualMutacionesCACCDResponse> registrarPagoVirtualMutacionesCACCDAsync(registrarPagoVirtualMutacionesCACCDRequest request);

    [OperationContract(Action = "registrarPagoVirtualMutacionesCN", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    [return: MessageParameter(Name = "registrarPagoVirtualMutacionesCNReturn")]
    registrarPagoVirtualMutacionesCNResponse registrarPagoVirtualMutacionesCN(registrarPagoVirtualMutacionesCNRequest request);

    [OperationContract(Action = "registrarPagoVirtualMutacionesCN", ReplyAction = "*")]
    Task<registrarPagoVirtualMutacionesCNResponse> registrarPagoVirtualMutacionesCNAsync(registrarPagoVirtualMutacionesCNRequest request);
  }
}

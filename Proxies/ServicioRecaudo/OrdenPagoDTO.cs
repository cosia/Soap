﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.OrdenPagoDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [GeneratedCode("System.Xml", "4.0.30319.34230")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://model.ws.recaudos.sirep2.ccb.org.co")]
  [Serializable]
  public class OrdenPagoDTO : INotifyPropertyChanged
  {
    private string idSucursalField;
    private string idUsuarioField;
    private int? numClienteField;
    private double? vrTotalField;
    private string idMonedaField;
    private string nomMonedaField;
    private string idServicioNegocioField;
    private OrdenPagoServiciosDTO[] liquidacionServiciosField;
    private ActosVigenciaDTO actosVigenciaField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 0)]
    public string idSucursal
    {
      get
      {
        return this.idSucursalField;
      }
      set
      {
        this.idSucursalField = value;
        this.RaisePropertyChanged(nameof (idSucursal));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 1)]
    public string idUsuario
    {
      get
      {
        return this.idUsuarioField;
      }
      set
      {
        this.idUsuarioField = value;
        this.RaisePropertyChanged(nameof (idUsuario));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 2)]
    public int? numCliente
    {
      get
      {
        return this.numClienteField;
      }
      set
      {
        this.numClienteField = value;
        this.RaisePropertyChanged(nameof (numCliente));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 3)]
    public double? vrTotal
    {
      get
      {
        return this.vrTotalField;
      }
      set
      {
        this.vrTotalField = value;
        this.RaisePropertyChanged(nameof (vrTotal));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 4)]
    public string idMoneda
    {
      get
      {
        return this.idMonedaField;
      }
      set
      {
        this.idMonedaField = value;
        this.RaisePropertyChanged(nameof (idMoneda));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 5)]
    public string nomMoneda
    {
      get
      {
        return this.nomMonedaField;
      }
      set
      {
        this.nomMonedaField = value;
        this.RaisePropertyChanged(nameof (nomMoneda));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 6)]
    public string idServicioNegocio
    {
      get
      {
        return this.idServicioNegocioField;
      }
      set
      {
        this.idServicioNegocioField = value;
        this.RaisePropertyChanged(nameof (idServicioNegocio));
      }
    }

    [XmlArray(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 7)]
    [XmlArrayItem(Form = XmlSchemaForm.Unqualified)]
    public OrdenPagoServiciosDTO[] liquidacionServicios
    {
      get
      {
        return this.liquidacionServiciosField;
      }
      set
      {
        this.liquidacionServiciosField = value;
        this.RaisePropertyChanged(nameof (liquidacionServicios));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 8)]
    public ActosVigenciaDTO actosVigencia
    {
      get
      {
        return this.actosVigenciaField;
      }
      set
      {
        this.actosVigenciaField = value;
        this.RaisePropertyChanged(nameof (actosVigencia));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

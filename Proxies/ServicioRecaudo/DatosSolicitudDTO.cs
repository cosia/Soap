﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.DatosSolicitudDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [GeneratedCode("System.Xml", "4.0.30319.34230")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://model.ws.recaudos.sirep2.ccb.org.co")]
  [Serializable]
  public class DatosSolicitudDTO : INotifyPropertyChanged
  {
    private string numOrdenPagoField;
    private string idSolicitudField;
    private string fecSolicitudField;
    private DocumentoDTO[] documentosField;
    private SolicitanteDTO solicitudField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 0)]
    public string numOrdenPago
    {
      get
      {
        return this.numOrdenPagoField;
      }
      set
      {
        this.numOrdenPagoField = value;
        this.RaisePropertyChanged(nameof (numOrdenPago));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 1)]
    public string idSolicitud
    {
      get
      {
        return this.idSolicitudField;
      }
      set
      {
        this.idSolicitudField = value;
        this.RaisePropertyChanged(nameof (idSolicitud));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 2)]
    public string fecSolicitud
    {
      get
      {
        return this.fecSolicitudField;
      }
      set
      {
        this.fecSolicitudField = value;
        this.RaisePropertyChanged(nameof (fecSolicitud));
      }
    }

    [XmlArray(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 3)]
    [XmlArrayItem(Form = XmlSchemaForm.Unqualified)]
    public DocumentoDTO[] documentos
    {
      get
      {
        return this.documentosField;
      }
      set
      {
        this.documentosField = value;
        this.RaisePropertyChanged(nameof (documentos));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 4)]
    public SolicitanteDTO solicitud
    {
      get
      {
        return this.solicitudField;
      }
      set
      {
        this.solicitudField = value;
        this.RaisePropertyChanged(nameof (solicitud));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

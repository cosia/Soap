﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.FormaPagoInDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [GeneratedCode("System.Xml", "4.0.30319.34230")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://model.ws.recaudos.sirep2.ccb.org.co")]
  [Serializable]
  public class FormaPagoInDTO : INotifyPropertyChanged
  {
    private string idFormaPagoField;
    private string nombreFormaPagoField;
    private string nombreCortoFormaPagoField;
    private double? vrPagoField;
    private double? vrSaldoField;
    private double? vrDevolucionField;
    private string idBancoField;
    private string idTipoTarjetaField;
    private string nombreTipoTarjetaField;
    private string nombreBancoField;
    private string numDocumentoField;
    private string numAutorizaField;
    private string fechaConsignacionField;
    private string idMonedaField;
    private string nombreMonedaField;
    private double? vrBaseField;
    private double? vrIvaField;
    private double? porcPagadoField;
    private string reciboRBMField;
    private string numeroRRNField;
    private int? itemField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 0)]
    public string idFormaPago
    {
      get
      {
        return this.idFormaPagoField;
      }
      set
      {
        this.idFormaPagoField = value;
        this.RaisePropertyChanged(nameof (idFormaPago));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 1)]
    public string nombreFormaPago
    {
      get
      {
        return this.nombreFormaPagoField;
      }
      set
      {
        this.nombreFormaPagoField = value;
        this.RaisePropertyChanged(nameof (nombreFormaPago));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 2)]
    public string nombreCortoFormaPago
    {
      get
      {
        return this.nombreCortoFormaPagoField;
      }
      set
      {
        this.nombreCortoFormaPagoField = value;
        this.RaisePropertyChanged(nameof (nombreCortoFormaPago));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 3)]
    public double? vrPago
    {
      get
      {
        return this.vrPagoField;
      }
      set
      {
        this.vrPagoField = value;
        this.RaisePropertyChanged(nameof (vrPago));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 4)]
    public double? vrSaldo
    {
      get
      {
        return this.vrSaldoField;
      }
      set
      {
        this.vrSaldoField = value;
        this.RaisePropertyChanged(nameof (vrSaldo));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 5)]
    public double? vrDevolucion
    {
      get
      {
        return this.vrDevolucionField;
      }
      set
      {
        this.vrDevolucionField = value;
        this.RaisePropertyChanged(nameof (vrDevolucion));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 6)]
    public string idBanco
    {
      get
      {
        return this.idBancoField;
      }
      set
      {
        this.idBancoField = value;
        this.RaisePropertyChanged(nameof (idBanco));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 7)]
    public string idTipoTarjeta
    {
      get
      {
        return this.idTipoTarjetaField;
      }
      set
      {
        this.idTipoTarjetaField = value;
        this.RaisePropertyChanged(nameof (idTipoTarjeta));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 8)]
    public string nombreTipoTarjeta
    {
      get
      {
        return this.nombreTipoTarjetaField;
      }
      set
      {
        this.nombreTipoTarjetaField = value;
        this.RaisePropertyChanged(nameof (nombreTipoTarjeta));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 9)]
    public string nombreBanco
    {
      get
      {
        return this.nombreBancoField;
      }
      set
      {
        this.nombreBancoField = value;
        this.RaisePropertyChanged(nameof (nombreBanco));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 10)]
    public string numDocumento
    {
      get
      {
        return this.numDocumentoField;
      }
      set
      {
        this.numDocumentoField = value;
        this.RaisePropertyChanged(nameof (numDocumento));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 11)]
    public string numAutoriza
    {
      get
      {
        return this.numAutorizaField;
      }
      set
      {
        this.numAutorizaField = value;
        this.RaisePropertyChanged(nameof (numAutoriza));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 12)]
    public string fechaConsignacion
    {
      get
      {
        return this.fechaConsignacionField;
      }
      set
      {
        this.fechaConsignacionField = value;
        this.RaisePropertyChanged(nameof (fechaConsignacion));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 13)]
    public string idMoneda
    {
      get
      {
        return this.idMonedaField;
      }
      set
      {
        this.idMonedaField = value;
        this.RaisePropertyChanged(nameof (idMoneda));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 14)]
    public string nombreMoneda
    {
      get
      {
        return this.nombreMonedaField;
      }
      set
      {
        this.nombreMonedaField = value;
        this.RaisePropertyChanged(nameof (nombreMoneda));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 15)]
    public double? vrBase
    {
      get
      {
        return this.vrBaseField;
      }
      set
      {
        this.vrBaseField = value;
        this.RaisePropertyChanged(nameof (vrBase));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 16)]
    public double? vrIva
    {
      get
      {
        return this.vrIvaField;
      }
      set
      {
        this.vrIvaField = value;
        this.RaisePropertyChanged(nameof (vrIva));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 17)]
    public double? porcPagado
    {
      get
      {
        return this.porcPagadoField;
      }
      set
      {
        this.porcPagadoField = value;
        this.RaisePropertyChanged(nameof (porcPagado));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 18)]
    public string reciboRBM
    {
      get
      {
        return this.reciboRBMField;
      }
      set
      {
        this.reciboRBMField = value;
        this.RaisePropertyChanged(nameof (reciboRBM));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 19)]
    public string numeroRRN
    {
      get
      {
        return this.numeroRRNField;
      }
      set
      {
        this.numeroRRNField = value;
        this.RaisePropertyChanged(nameof (numeroRRN));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 20)]
    public int? item
    {
      get
      {
        return this.itemField;
      }
      set
      {
        this.itemField = value;
        this.RaisePropertyChanged(nameof (item));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

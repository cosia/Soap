﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.DireccionDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [GeneratedCode("System.Xml", "4.0.30319.34230")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://model.ws.recaudos.sirep2.ccb.org.co")]
  [Serializable]
  public class DireccionDTO : INotifyPropertyChanged
  {
    private int? numClienteField;
    private string idTipoDirField;
    private string descTipoDirField;
    private string direccionField;
    private string idMunipField;
    private string descMunicipioField;
    private string idPaisField;
    private string descPaisField;
    private string numTel1Field;
    private string numTel2Field;
    private string numTel3Field;
    private string numFaxField;
    private string emailField;
    private int? ctrMensajesField;
    private string barrioTextualField;
    private string idBarrioField;
    private string descBarrioField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 0)]
    public int? numCliente
    {
      get
      {
        return this.numClienteField;
      }
      set
      {
        this.numClienteField = value;
        this.RaisePropertyChanged(nameof (numCliente));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 1)]
    public string idTipoDir
    {
      get
      {
        return this.idTipoDirField;
      }
      set
      {
        this.idTipoDirField = value;
        this.RaisePropertyChanged(nameof (idTipoDir));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 2)]
    public string descTipoDir
    {
      get
      {
        return this.descTipoDirField;
      }
      set
      {
        this.descTipoDirField = value;
        this.RaisePropertyChanged(nameof (descTipoDir));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 3)]
    public string direccion
    {
      get
      {
        return this.direccionField;
      }
      set
      {
        this.direccionField = value;
        this.RaisePropertyChanged(nameof (direccion));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 4)]
    public string idMunip
    {
      get
      {
        return this.idMunipField;
      }
      set
      {
        this.idMunipField = value;
        this.RaisePropertyChanged(nameof (idMunip));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 5)]
    public string descMunicipio
    {
      get
      {
        return this.descMunicipioField;
      }
      set
      {
        this.descMunicipioField = value;
        this.RaisePropertyChanged(nameof (descMunicipio));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 6)]
    public string idPais
    {
      get
      {
        return this.idPaisField;
      }
      set
      {
        this.idPaisField = value;
        this.RaisePropertyChanged(nameof (idPais));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 7)]
    public string descPais
    {
      get
      {
        return this.descPaisField;
      }
      set
      {
        this.descPaisField = value;
        this.RaisePropertyChanged(nameof (descPais));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 8)]
    public string numTel1
    {
      get
      {
        return this.numTel1Field;
      }
      set
      {
        this.numTel1Field = value;
        this.RaisePropertyChanged(nameof (numTel1));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 9)]
    public string numTel2
    {
      get
      {
        return this.numTel2Field;
      }
      set
      {
        this.numTel2Field = value;
        this.RaisePropertyChanged(nameof (numTel2));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 10)]
    public string numTel3
    {
      get
      {
        return this.numTel3Field;
      }
      set
      {
        this.numTel3Field = value;
        this.RaisePropertyChanged(nameof (numTel3));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 11)]
    public string numFax
    {
      get
      {
        return this.numFaxField;
      }
      set
      {
        this.numFaxField = value;
        this.RaisePropertyChanged(nameof (numFax));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 12)]
    public string email
    {
      get
      {
        return this.emailField;
      }
      set
      {
        this.emailField = value;
        this.RaisePropertyChanged(nameof (email));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 13)]
    public int? ctrMensajes
    {
      get
      {
        return this.ctrMensajesField;
      }
      set
      {
        this.ctrMensajesField = value;
        this.RaisePropertyChanged(nameof (ctrMensajes));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 14)]
    public string barrioTextual
    {
      get
      {
        return this.barrioTextualField;
      }
      set
      {
        this.barrioTextualField = value;
        this.RaisePropertyChanged(nameof (barrioTextual));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 15)]
    public string idBarrio
    {
      get
      {
        return this.idBarrioField;
      }
      set
      {
        this.idBarrioField = value;
        this.RaisePropertyChanged(nameof (idBarrio));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 16)]
    public string descBarrio
    {
      get
      {
        return this.descBarrioField;
      }
      set
      {
        this.descBarrioField = value;
        this.RaisePropertyChanged(nameof (descBarrio));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

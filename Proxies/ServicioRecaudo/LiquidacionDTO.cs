﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo.LiquidacionDTO
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo
{
  [GeneratedCode("System.Xml", "4.0.30319.34230")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://model.ws.recaudos.sirep2.ccb.org.co")]
  [Serializable]
  public class LiquidacionDTO : INotifyPropertyChanged
  {
    private string numReciboField;
    private string idServicioField;
    private int? itemField;
    private int? itemPadreField;
    private string idServicioPadreField;
    private string nombreServicioField;
    private int? cantidadField;
    private double? valorServicioField;
    private string idGastoAdminField;
    private string gastoAdministrativoField;
    private string numMatriculaField;
    private string nombreMatriculaField;
    private string anoRenovaField;
    private double? valorBaseField;
    private double? valorActivoTotalField;
    private string idServicioBaseField;
    private string idLibroField;
    private string idActoField;
    private string numTramiteField;
    private string numNotaCreditoField;
    private string idTipoVentaField;
    private int? idDescuentoField;
    private int? idFondoField;
    private int? cntPersonalField;
    private string fechaBaseField;
    private string fechaEspecialField;
    private int? idCertificadoField;
    private FormaPagoInDTO formaPagoField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 0)]
    public string numRecibo
    {
      get
      {
        return this.numReciboField;
      }
      set
      {
        this.numReciboField = value;
        this.RaisePropertyChanged(nameof (numRecibo));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 1)]
    public string idServicio
    {
      get
      {
        return this.idServicioField;
      }
      set
      {
        this.idServicioField = value;
        this.RaisePropertyChanged(nameof (idServicio));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 2)]
    public int? item
    {
      get
      {
        return this.itemField;
      }
      set
      {
        this.itemField = value;
        this.RaisePropertyChanged(nameof (item));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 3)]
    public int? itemPadre
    {
      get
      {
        return this.itemPadreField;
      }
      set
      {
        this.itemPadreField = value;
        this.RaisePropertyChanged(nameof (itemPadre));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 4)]
    public string idServicioPadre
    {
      get
      {
        return this.idServicioPadreField;
      }
      set
      {
        this.idServicioPadreField = value;
        this.RaisePropertyChanged(nameof (idServicioPadre));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 5)]
    public string nombreServicio
    {
      get
      {
        return this.nombreServicioField;
      }
      set
      {
        this.nombreServicioField = value;
        this.RaisePropertyChanged(nameof (nombreServicio));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 6)]
    public int? cantidad
    {
      get
      {
        return this.cantidadField;
      }
      set
      {
        this.cantidadField = value;
        this.RaisePropertyChanged(nameof (cantidad));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 7)]
    public double? valorServicio
    {
      get
      {
        return this.valorServicioField;
      }
      set
      {
        this.valorServicioField = value;
        this.RaisePropertyChanged(nameof (valorServicio));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 8)]
    public string idGastoAdmin
    {
      get
      {
        return this.idGastoAdminField;
      }
      set
      {
        this.idGastoAdminField = value;
        this.RaisePropertyChanged(nameof (idGastoAdmin));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 9)]
    public string gastoAdministrativo
    {
      get
      {
        return this.gastoAdministrativoField;
      }
      set
      {
        this.gastoAdministrativoField = value;
        this.RaisePropertyChanged(nameof (gastoAdministrativo));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 10)]
    public string numMatricula
    {
      get
      {
        return this.numMatriculaField;
      }
      set
      {
        this.numMatriculaField = value;
        this.RaisePropertyChanged(nameof (numMatricula));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 11)]
    public string nombreMatricula
    {
      get
      {
        return this.nombreMatriculaField;
      }
      set
      {
        this.nombreMatriculaField = value;
        this.RaisePropertyChanged(nameof (nombreMatricula));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 12)]
    public string anoRenova
    {
      get
      {
        return this.anoRenovaField;
      }
      set
      {
        this.anoRenovaField = value;
        this.RaisePropertyChanged(nameof (anoRenova));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 13)]
    public double? valorBase
    {
      get
      {
        return this.valorBaseField;
      }
      set
      {
        this.valorBaseField = value;
        this.RaisePropertyChanged(nameof (valorBase));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 14)]
    public double? valorActivoTotal
    {
      get
      {
        return this.valorActivoTotalField;
      }
      set
      {
        this.valorActivoTotalField = value;
        this.RaisePropertyChanged(nameof (valorActivoTotal));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 15)]
    public string idServicioBase
    {
      get
      {
        return this.idServicioBaseField;
      }
      set
      {
        this.idServicioBaseField = value;
        this.RaisePropertyChanged(nameof (idServicioBase));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 16)]
    public string idLibro
    {
      get
      {
        return this.idLibroField;
      }
      set
      {
        this.idLibroField = value;
        this.RaisePropertyChanged(nameof (idLibro));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 17)]
    public string idActo
    {
      get
      {
        return this.idActoField;
      }
      set
      {
        this.idActoField = value;
        this.RaisePropertyChanged(nameof (idActo));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 18)]
    public string numTramite
    {
      get
      {
        return this.numTramiteField;
      }
      set
      {
        this.numTramiteField = value;
        this.RaisePropertyChanged(nameof (numTramite));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 19)]
    public string numNotaCredito
    {
      get
      {
        return this.numNotaCreditoField;
      }
      set
      {
        this.numNotaCreditoField = value;
        this.RaisePropertyChanged(nameof (numNotaCredito));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 20)]
    public string idTipoVenta
    {
      get
      {
        return this.idTipoVentaField;
      }
      set
      {
        this.idTipoVentaField = value;
        this.RaisePropertyChanged(nameof (idTipoVenta));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 21)]
    public int? idDescuento
    {
      get
      {
        return this.idDescuentoField;
      }
      set
      {
        this.idDescuentoField = value;
        this.RaisePropertyChanged(nameof (idDescuento));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 22)]
    public int? idFondo
    {
      get
      {
        return this.idFondoField;
      }
      set
      {
        this.idFondoField = value;
        this.RaisePropertyChanged(nameof (idFondo));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 23)]
    public int? cntPersonal
    {
      get
      {
        return this.cntPersonalField;
      }
      set
      {
        this.cntPersonalField = value;
        this.RaisePropertyChanged(nameof (cntPersonal));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 24)]
    public string fechaBase
    {
      get
      {
        return this.fechaBaseField;
      }
      set
      {
        this.fechaBaseField = value;
        this.RaisePropertyChanged(nameof (fechaBase));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 25)]
    public string fechaEspecial
    {
      get
      {
        return this.fechaEspecialField;
      }
      set
      {
        this.fechaEspecialField = value;
        this.RaisePropertyChanged(nameof (fechaEspecial));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 26)]
    public int? idCertificado
    {
      get
      {
        return this.idCertificadoField;
      }
      set
      {
        this.idCertificadoField = value;
        this.RaisePropertyChanged(nameof (idCertificado));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 27)]
    public FormaPagoInDTO formaPago
    {
      get
      {
        return this.formaPagoField;
      }
      set
      {
        this.formaPagoField = value;
        this.RaisePropertyChanged(nameof (formaPago));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

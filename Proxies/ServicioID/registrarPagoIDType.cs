﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioID.registrarPagoIDType
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioID
{
  [GeneratedCode("System.Xml", "4.6.1586.0")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://ws.recaudos.esb.ccb.org.co")]
  [Serializable]
  public class registrarPagoIDType : INotifyPropertyChanged
  {
    private metadataType metadataField;
    private dataType dataField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
    public metadataType metadata
    {
      get
      {
        return this.metadataField;
      }
      set
      {
        this.metadataField = value;
        this.RaisePropertyChanged(nameof (metadata));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
    public dataType data
    {
      get
      {
        return this.dataField;
      }
      set
      {
        this.dataField = value;
        this.RaisePropertyChanged(nameof (data));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioID.registrarPagoDTOType
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioID
{
  [GeneratedCode("System.Xml", "4.6.1586.0")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://ws.recaudos.esb.ccb.org.co")]
  [Serializable]
  public class registrarPagoDTOType : INotifyPropertyChanged
  {
    private string numOrdenPagoField;
    private string fecRecaudoField;
    private int numClientePagoField;
    private string numSolicitudField;
    private formaPagoType formaPagoField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
    public string numOrdenPago
    {
      get
      {
        return this.numOrdenPagoField;
      }
      set
      {
        this.numOrdenPagoField = value;
        this.RaisePropertyChanged(nameof (numOrdenPago));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
    public string fecRecaudo
    {
      get
      {
        return this.fecRecaudoField;
      }
      set
      {
        this.fecRecaudoField = value;
        this.RaisePropertyChanged(nameof (fecRecaudo));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 2)]
    public int numClientePago
    {
      get
      {
        return this.numClientePagoField;
      }
      set
      {
        this.numClientePagoField = value;
        this.RaisePropertyChanged(nameof (numClientePago));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 3)]
    public string numSolicitud
    {
      get
      {
        return this.numSolicitudField;
      }
      set
      {
        this.numSolicitudField = value;
        this.RaisePropertyChanged(nameof (numSolicitud));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 4)]
    public formaPagoType formaPago
    {
      get
      {
        return this.formaPagoField;
      }
      set
      {
        this.formaPagoField = value;
        this.RaisePropertyChanged(nameof (formaPago));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioID.registrarPagoPortTypeClient
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioID
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class registrarPagoPortTypeClient : ClientBase<registrarPagoPortType>, registrarPagoPortType
  {
    public registrarPagoPortTypeClient()
    {
    }

    public registrarPagoPortTypeClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public registrarPagoPortTypeClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public registrarPagoPortTypeClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public registrarPagoPortTypeClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    registrarPago_Output registrarPagoPortType.registrarPagoID(registrarPago_Input request)
    {
      return this.Channel.registrarPagoID(request);
    }

    public registrarPagoIDRespType registrarPagoID(registrarPagoIDType registrarPagoID1)
    {
      return ((registrarPagoPortType) this).registrarPagoID(new registrarPago_Input()
      {
        registrarPagoID = registrarPagoID1
      }).registrarPagoIDResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<registrarPago_Output> registrarPagoPortType.registrarPagoIDAsync(registrarPago_Input request)
    {
      return this.Channel.registrarPagoIDAsync(request);
    }

    public Task<registrarPago_Output> registrarPagoIDAsync(registrarPagoIDType registrarPagoID)
    {
      return ((registrarPagoPortType) this).registrarPagoIDAsync(new registrarPago_Input()
      {
        registrarPagoID = registrarPagoID
      });
    }
  }
}

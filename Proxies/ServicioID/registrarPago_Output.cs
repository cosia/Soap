﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Proxies.ServicioID.registrarPago_Output
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AgentesExternos.Sirepx.Soap.Proxies.ServicioID
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [EditorBrowsable(EditorBrowsableState.Advanced)]
  [MessageContract(IsWrapped = false)]
  public class registrarPago_Output
  {
    [MessageBodyMember(Namespace = "http://ws.recaudos.esb.ccb.org.co", Order = 0)]
    public registrarPagoIDRespType registrarPagoIDResponse;

    public registrarPago_Output()
    {
    }

    public registrarPago_Output(registrarPagoIDRespType registrarPagoIDResponse)
    {
      this.registrarPagoIDResponse = registrarPagoIDResponse;
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.ServicioExternoBusPagos
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using AgentesExternos.Sirepx.Dto;
using AgentesExternos.Sirepx.Soap.Nucleo;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioBus;
using System;

namespace AgentesExternos.Sirepx.Soap
{
  public class ServicioExternoBusPagos : IServicioExternoBusPagos
  {
    public RegistroPagoVirtualDto RegistrarPago(DatosRegistroPagoDto datosRegistroPagoDto)
    {
      registrarPagoMutacionType registrarPagoMutacion1 = new registrarPagoMutacionType()
      {
        data = new dataType2()
        {
          registrarPagoDTO = new registrarPagoDTOType2()
          {
            numOrdenPago = datosRegistroPagoDto.OrdenPago,
            fecRecaudo = datosRegistroPagoDto.FechaRecuado,
            numClientePago = Convert.ToInt32(datosRegistroPagoDto.NumeroClientePago),
            numSolicitud = datosRegistroPagoDto.NumeroSolicitud,
            formaPago = new formaPagoType2()
            {
              idFormaPago = datosRegistroPagoDto.FormaPagoId,
              valorPago = Convert.ToDecimal(datosRegistroPagoDto.ValorPago),
              idBanco = datosRegistroPagoDto.BancoId ?? string.Empty,
              idTipoTarjeta = datosRegistroPagoDto.TipoTarjetaId,
              numDocumento = datosRegistroPagoDto.NumeroDocumento,
              numAutoriza = datosRegistroPagoDto.NumeroAutorizacion,
              idMoneda = datosRegistroPagoDto.MonedaId
            }
          }
        },
        metadata = new metadataType2()
      };
      if (!string.IsNullOrEmpty(datosRegistroPagoDto.NumeroRecibo))
        registrarPagoMutacion1.data.registrarPagoDTO.numRecibo = datosRegistroPagoDto.NumeroRecibo;
      registrarPagoMutacionRespType mutacionRespType = ClienteSoap.ObtenerClienteBus().registrarPagoMutacion(registrarPagoMutacion1);
      return new RegistroPagoVirtualDto()
      {
        CodigoError = mutacionRespType.data.registrarPagoOutDTO.resultado.codigoError,
        DescripcionError = mutacionRespType.data.registrarPagoOutDTO.resultado.mensajeError
      };
    }
  }
}

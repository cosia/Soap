﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.ServicioExternoBusID
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using AgentesExternos.Sirepx.Dto;
using AgentesExternos.Sirepx.Dto.ID;
using AgentesExternos.Sirepx.Soap.Extensiones;
using AgentesExternos.Sirepx.Soap.Nucleo;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioID;
using System;
using System.Net.Http;
using System.Text;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap
{
  public class ServicioExternoBusID : IServicioExternoBusID
  {
    public RegistroPagoVirtualDto RegistrarPago(RegistrarPagoIDDto registrarPagoIdDto)
    {
      registrarPagoIDRespType registrarPagoIdRespType = ClienteSoap.ObtenerClienteID().registrarPagoID(new registrarPagoIDType()
      {
        data = new dataType()
        {
          registrarPagoDTO = new registrarPagoDTOType()
          {
            fecRecaudo = registrarPagoIdDto.FechaRecuado,
            formaPago = new formaPagoType()
            {
              idFormaPago = registrarPagoIdDto.FormaPagoId,
              valorPago = Convert.ToDecimal(registrarPagoIdDto.ValorPago),
              idBanco = registrarPagoIdDto.BancoId ?? string.Empty,
              idTipoTarjeta = registrarPagoIdDto.TipoTarjetaId,
              numDocumento = registrarPagoIdDto.NumeroDocumento,
              numAutoriza = registrarPagoIdDto.NumeroAutorizacion,
              idMoneda = registrarPagoIdDto.MonedaId
            },
            numClientePago = Convert.ToInt32(registrarPagoIdDto.NumeroClientePago),
            numOrdenPago = registrarPagoIdDto.OrdenPago,
            numSolicitud = registrarPagoIdDto.NumeroSolicitud
          }
        },
        metadata = new metadataType()
      });
      return new RegistroPagoVirtualDto()
      {
        CodigoError = registrarPagoIdRespType.data.registrarPagoOutDTO.resultado.codigoError,
        DescripcionError = registrarPagoIdRespType.data.registrarPagoOutDTO.resultado.mensajeError
      };
    }

    public void RegistrarPago()
    {
      RegistrarPagoDTO origen = new RegistrarPagoDTO();
      origen.Data = new DataDTO()
      {
        RegistrarPagoInDTO = new RegistrarPagoInDTO()
        {
          FechaRecaudo = "2015-10-23",
          FormaPago = new FormaPagoDTO()
          {
            FormaPagoId = "313",
            BancoId = "",
            MonedaId = "10001",
            NumeroAutorizacion = "21910",
            NumeroDocumento = "51111*****1111",
            TipoTarjetaId = "313",
            ValorPago = new Decimal(117760)
          },
          NumeroClientePago = 3214511,
          NumeroSolicitud = "666",
          OrdenPago = "0005002873"
        }
      };
      origen.Metadata = new MetadataDTO();
      XmlSerializerNamespaces espaciosDeNombre = new XmlSerializerNamespaces();
      espaciosDeNombre.Add("", "");
      string content = string.Format("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws.recaudos.esb.ccb.org.co\">\r\n               <soapenv:Header/>\r\n               <soapenv:Body>\r\n                  {0}\r\n               </soapenv:Body>\r\n            </soapenv:Envelope>", (object) SerializadorXml<RegistrarPagoDTO>.Serializar(origen, espaciosDeNombre));
      using (HttpClient httpClient = new HttpClient())
      {
        httpClient.DefaultRequestHeaders.Add("SOAPAction", "http://ws.recaudos.esb.ccb.org.co");
        StringContent stringContent = new StringContent(content, Encoding.UTF8, "text/xml");
        using (HttpResponseMessage result = httpClient.PostAsync(new Uri("http://172.16.1.33:7802/esb/service/PUP/"), (HttpContent) stringContent).Result)
          Console.WriteLine(result.Content.ReadAsStringAsync().Result);
      }
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Nucleo.ClienteSoap
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using AgentesExternos.Sirepx.Soap.Extensiones;
using AgentesExternos.Sirepx.Soap.Proxies.Notarias;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioBus;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioCM;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioID;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioLiquidaciones;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioMEC;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioMPN;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioReingresos;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioVigencia;
using System.ServiceModel.Channels;

namespace AgentesExternos.Sirepx.Soap.Nucleo
{
  public class ClienteSoap
  {
    public static ServiciosRecaudosWSClient ObtenerCliente()
    {
      return new ServiciosRecaudosWSClient();
    }

    public static registrarPagoMutacionPortTypeClient ObtenerClienteBus()
    {
      CustomBinding customBinding = new CustomBinding(new BindingElement[2]
      {
        (BindingElement) new CustomTextMessageBindingElement("iso-8859-1", "text/xml", MessageVersion.Soap11),
        (BindingElement) new HttpTransportBindingElement()
      });
      registrarPagoMutacionPortTypeClient mutacionPortTypeClient = new registrarPagoMutacionPortTypeClient();
      mutacionPortTypeClient.Endpoint.Binding = (Binding) customBinding;
      return mutacionPortTypeClient;
    }

    public static registrarPagoCMPortTypeClient ObtenerClienteCM()
    {
      CustomBinding customBinding = new CustomBinding(new BindingElement[2]
      {
        (BindingElement) new CustomTextMessageBindingElement("iso-8859-1", "text/xml", MessageVersion.Soap11),
        (BindingElement) new HttpTransportBindingElement()
      });
      registrarPagoCMPortTypeClient cmPortTypeClient = new registrarPagoCMPortTypeClient();
      cmPortTypeClient.Endpoint.Binding = (Binding) customBinding;
      return cmPortTypeClient;
    }

    public static registrarPagoPortTypeClient ObtenerClienteID()
    {
      CustomBinding customBinding = new CustomBinding(new BindingElement[2]
      {
        (BindingElement) new CustomTextMessageBindingElement("iso-8859-1", "text/xml", MessageVersion.Soap11),
        (BindingElement) new HttpTransportBindingElement()
      });
      registrarPagoPortTypeClient pagoPortTypeClient = new registrarPagoPortTypeClient();
      pagoPortTypeClient.Endpoint.Binding = (Binding) customBinding;
      return pagoPortTypeClient;
    }

    public static LiquidarServiciosWSClient ObtenerClienteLiquidaciones()
    {
      return new LiquidarServiciosWSClient();
    }

    public static registrarPagoMECPortTypeClient ObtenerClienteMEC()
    {
      CustomBinding customBinding = new CustomBinding(new BindingElement[2]
      {
        (BindingElement) new CustomTextMessageBindingElement("iso-8859-1", "text/xml", MessageVersion.Soap11),
        (BindingElement) new HttpTransportBindingElement()
      });
      registrarPagoMECPortTypeClient mecPortTypeClient = new registrarPagoMECPortTypeClient();
      mecPortTypeClient.Endpoint.Binding = (Binding) customBinding;
      return mecPortTypeClient;
    }

    public static registrarPagoMPNPortTypeClient ObtenerClienteMPN()
    {
      CustomBinding customBinding = new CustomBinding(new BindingElement[2]
      {
        (BindingElement) new CustomTextMessageBindingElement("iso-8859-1", "text/xml", MessageVersion.Soap11),
        (BindingElement) new HttpTransportBindingElement()
      });
      registrarPagoMPNPortTypeClient mpnPortTypeClient = new registrarPagoMPNPortTypeClient();
      mpnPortTypeClient.Endpoint.Binding = (Binding) customBinding;
      return mpnPortTypeClient;
    }

    public static NotariasWSClient ObtenerClienteNotarias()
    {
      return new NotariasWSClient();
    }

    public static registrarPagoReingresoPortTypeClient ObtenerClienteReingresos()
    {
      CustomBinding customBinding = new CustomBinding(new BindingElement[2]
      {
        (BindingElement) new CustomTextMessageBindingElement("iso-8859-1", "text/xml", MessageVersion.Soap11),
        (BindingElement) new HttpTransportBindingElement()
      });
      registrarPagoReingresoPortTypeClient reingresoPortTypeClient = new registrarPagoReingresoPortTypeClient();
      reingresoPortTypeClient.Endpoint.Binding = (Binding) customBinding;
      return reingresoPortTypeClient;
    }

    public static ObtenerFechaVigenciaSEIClient ObtenerClienteVigencia()
    {
      return new ObtenerFechaVigenciaSEIClient();
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.ServicioExternoSirepLiquidaciones
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using AgentesExternos.Sirepx.Dto.CM;
using AgentesExternos.Sirepx.Dto.ID;
using AgentesExternos.Sirepx.Dto.MEC;
using AgentesExternos.Sirepx.Dto.MPN;
using AgentesExternos.Sirepx.Soap.Extensiones;
using AgentesExternos.Sirepx.Soap.Nucleo;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioLiquidaciones;
using System;
using System.Text;
using System.Xml;

namespace AgentesExternos.Sirepx.Soap
{
  public class ServicioExternoSirepLiquidaciones : IServicioExternoSirepLiquidaciones
  {
    public LiquidarCMOutDTO LiquidarCM(LiquidarCMDTO dto)
    {
      RealizarLiquidacionResultDTO liquidacionResultDto = ClienteSoap.ObtenerClienteLiquidaciones().liquidarServicios(new LiquidacionInDTO()
      {
        servicioId = dto.ServicioNegocioId,
        solicitudAplicativoId = dto.SolicitudAplicativoId,
        usuarioId = dto.UsuarioId,
        detalle = dto.Detalle
      });
      if (liquidacionResultDto == null)
        return (LiquidarCMOutDTO) null;
      if (liquidacionResultDto.codigoError != "0000")
        return new LiquidarCMOutDTO()
        {
          MensajeError = liquidacionResultDto.mensajeError
        };
      LiquidacionSirepCMDTO liquidacionSirepCmdto = SerializadorXml<LiquidacionSirepCMDTO>.Deserializar(string.Format("<Liquidacion>{0}</Liquidacion>", (object) liquidacionResultDto.detalle.Trim()), Encoding.UTF8, (XmlReaderSettings) null);
      return new LiquidarCMOutDTO()
      {
        IdMoneda = liquidacionResultDto.idMoneda,
        DescMoneda = liquidacionResultDto.moneda,
        TotalOperacion = (Decimal) liquidacionResultDto.valorTotal,
        LiquidacionSirep = liquidacionSirepCmdto,
        MensajeError = liquidacionResultDto.mensajeError,
        Detalle = liquidacionResultDto.detalle
      };
    }

    public LiquidarIDOutDTO LiquidarID(LiquidarIDDTO dto)
    {
      RealizarLiquidacionResultDTO liquidacionResultDto = ClienteSoap.ObtenerClienteLiquidaciones().liquidarServicios(new LiquidacionInDTO()
      {
        servicioId = dto.ServicioNegocioId,
        solicitudAplicativoId = dto.SolicitudAplicativoId,
        usuarioId = dto.UsuarioId,
        detalle = dto.Detalle
      });
      if (liquidacionResultDto == null)
        return (LiquidarIDOutDTO) null;
      if (liquidacionResultDto.codigoError != "0000")
        return new LiquidarIDOutDTO()
        {
          MensajeError = liquidacionResultDto.mensajeError
        };
      LiquidacionSirepIDDTO liquidacionSirepIddto = SerializadorXml<LiquidacionSirepIDDTO>.Deserializar(string.Format("<Liquidacion>{0}</Liquidacion>", (object) liquidacionResultDto.detalle.Trim()), Encoding.UTF8, (XmlReaderSettings) null);
      return new LiquidarIDOutDTO()
      {
        IdMoneda = liquidacionResultDto.idMoneda,
        DescMoneda = liquidacionResultDto.moneda,
        TotalOperacion = (Decimal) liquidacionResultDto.valorTotal,
        LiquidacionSirepDto = liquidacionSirepIddto,
        MensajeError = liquidacionResultDto.mensajeError,
        Detalle = liquidacionResultDto.detalle
      };
    }

    public LiquidarMECOutDTO LiquidarMEC(LiquidarMECDTO dto)
    {
      RealizarLiquidacionResultDTO liquidacionResultDto = ClienteSoap.ObtenerClienteLiquidaciones().liquidarServicios(new LiquidacionInDTO()
      {
        servicioId = dto.ServicioNegocioId,
        solicitudAplicativoId = dto.SolicitudAplicativoId,
        usuarioId = dto.UsuarioId,
        detalle = dto.Detalle
      });
      if (liquidacionResultDto == null)
        return (LiquidarMECOutDTO) null;
      if (liquidacionResultDto.codigoError != "0000")
        return new LiquidarMECOutDTO()
        {
          MensajeError = liquidacionResultDto.mensajeError
        };
      LiquidacionSirepMECDTO liquidacionSirepMecdto = SerializadorXml<LiquidacionSirepMECDTO>.Deserializar(string.Format("<Liquidacion>{0}</Liquidacion>", (object) liquidacionResultDto.detalle.Trim()), Encoding.UTF8, (XmlReaderSettings) null);
      return new LiquidarMECOutDTO()
      {
        IdMoneda = liquidacionResultDto.idMoneda,
        DescMoneda = liquidacionResultDto.moneda,
        TotalOperacion = (Decimal) liquidacionResultDto.valorTotal,
        LiquidacionSirep = liquidacionSirepMecdto,
        MensajeError = liquidacionResultDto.mensajeError,
        Detalle = liquidacionResultDto.detalle
      };
    }

    public LiquidarMPNOutDTO LiquidarMPN(LiquidarMPNDTO dto)
    {
      RealizarLiquidacionResultDTO liquidacionResultDto = ClienteSoap.ObtenerClienteLiquidaciones().liquidarServicios(new LiquidacionInDTO()
      {
        servicioId = dto.ServicioNegocioId,
        solicitudAplicativoId = dto.SolicitudAplicativoId,
        usuarioId = dto.UsuarioId,
        detalle = dto.Detalle
      });
      if (liquidacionResultDto == null)
        return (LiquidarMPNOutDTO) null;
      if (liquidacionResultDto.codigoError != "0000")
        return new LiquidarMPNOutDTO()
        {
          MensajeError = liquidacionResultDto.mensajeError
        };
      LiquidacionSirepMPNDTO liquidacionSirepMpndto = SerializadorXml<LiquidacionSirepMPNDTO>.Deserializar(string.Format("<Liquidacion>{0}</Liquidacion>", (object) liquidacionResultDto.detalle.Trim()), Encoding.UTF8, (XmlReaderSettings) null);
      return new LiquidarMPNOutDTO()
      {
        IdMoneda = liquidacionResultDto.idMoneda,
        DescMoneda = liquidacionResultDto.moneda,
        TotalOperacion = (Decimal) liquidacionResultDto.valorTotal,
        LiquidacionSirep = liquidacionSirepMpndto,
        MensajeError = liquidacionResultDto.mensajeError,
        Detalle = liquidacionResultDto.detalle
      };
    }
  }
}

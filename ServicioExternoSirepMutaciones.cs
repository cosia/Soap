﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.ServicioExternoSirepMutaciones
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using AgentesExternos.Sirepx.Dto;
using AgentesExternos.Sirepx.Soap.Nucleo;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AgentesExternos.Sirepx.Soap
{
  public class ServicioExternoSirepMutaciones : IServicioExternoSirepMutaciones
  {
    public LiquidarOutDto Liquidar(LiquidarMutacionDto dto)
    {
      LiquidaMutacionDTO liquidaMutacionDTO = new LiquidaMutacionDTO()
      {
        numMatricula = dto.NumMatricula
      };
      List<LibroActoDTO> list = dto.ListaActos.Select<AgentesExternos.Sirepx.Dto.ActoDTO, LibroActoDTO>((Func<AgentesExternos.Sirepx.Dto.ActoDTO, LibroActoDTO>) (acto => new LibroActoDTO()
      {
        idActo = acto.ActoId,
        idLibro = acto.LibroId,
        idImpuesto = new int?(Convert.ToInt32(acto.ImpuestoId))
      })).ToList<LibroActoDTO>();
      liquidaMutacionDTO.listaActos = list.ToArray();
      ServiciosRecaudosWSClient recaudosWsClient = ClienteSoap.ObtenerCliente();
      recaudosWsClient.Endpoint.Binding.OpenTimeout = new TimeSpan(0, 10, 0);
      recaudosWsClient.Endpoint.Binding.CloseTimeout = new TimeSpan(0, 10, 0);
      recaudosWsClient.Endpoint.Binding.SendTimeout = new TimeSpan(0, 10, 0);
      recaudosWsClient.Endpoint.Binding.ReceiveTimeout = new TimeSpan(0, 10, 0);
      LiquidarOutDTO liquidarOutDto = recaudosWsClient.liquidarServiciosMutacion(liquidaMutacionDTO);
      if (liquidarOutDto == null)
        return (LiquidarOutDto) null;
      if (liquidarOutDto.codigoError != "0000")
        return new LiquidarOutDto()
        {
          MensajeError = liquidarOutDto.mensajeError
        };
      return new LiquidarOutDto()
      {
        IdMoneda = liquidarOutDto.idMoneda,
        DescMoneda = liquidarOutDto.descMoneda,
        TotalOperacion = !liquidarOutDto.totalOperacion.HasValue ? Decimal.Zero : (Decimal) liquidarOutDto.totalOperacion.Value,
        Liquidacion = (IEnumerable<LiquidacionDto>) ((IEnumerable<LiquidacionDTO>) liquidarOutDto.liquidacion).Select(liquidacionDto => new
        {
          liquidacionDto = liquidacionDto,
          gastoAdministrativo = Convert.ToDecimal(liquidacionDto.gastoAdministrativo)
        }).Select(_param1 =>
        {
          LiquidacionDto liquidacionDto = new LiquidacionDto();
          liquidacionDto.AnoRenova = _param1.liquidacionDto.anoRenova;
          liquidacionDto.Cantidad = !_param1.liquidacionDto.cantidad.HasValue ? 0 : _param1.liquidacionDto.cantidad.Value;
          liquidacionDto.CntPersonal = _param1.liquidacionDto.cntPersonal;
          liquidacionDto.FechaBase = _param1.liquidacionDto.fechaBase;
          liquidacionDto.FechaEspecial = _param1.liquidacionDto.fechaEspecial;
          liquidacionDto.GastoAdministrativo = _param1.gastoAdministrativo;
          liquidacionDto.IdActo = _param1.liquidacionDto.idActo;
          liquidacionDto.IdCertificado = _param1.liquidacionDto.idCertificado;
          liquidacionDto.IdDescuento = _param1.liquidacionDto.idDescuento;
          liquidacionDto.IdFondo = _param1.liquidacionDto.idFondo;
          liquidacionDto.IdGastoAdmin = _param1.liquidacionDto.idGastoAdmin;
          liquidacionDto.IdLibro = _param1.liquidacionDto.idLibro;
          liquidacionDto.IdServicio = _param1.liquidacionDto.idServicio;
          liquidacionDto.IdServicioBase = _param1.liquidacionDto.idServicioBase;
          liquidacionDto.IdServicioPadre = _param1.liquidacionDto.idServicioPadre;
          liquidacionDto.IdTipoVenta = _param1.liquidacionDto.idTipoVenta;
          liquidacionDto.Item = !_param1.liquidacionDto.item.HasValue ? 0 : _param1.liquidacionDto.item.Value;
          int? itemPadre = _param1.liquidacionDto.itemPadre;
          int num;
          if (itemPadre.HasValue)
          {
            itemPadre = _param1.liquidacionDto.itemPadre;
            num = itemPadre.Value;
          }
          else
            num = 0;
          liquidacionDto.ItemPadre = num;
          liquidacionDto.NombreMatricula = _param1.liquidacionDto.nombreMatricula;
          liquidacionDto.NombreServicio = _param1.liquidacionDto.nombreServicio;
          liquidacionDto.NumMatricula = _param1.liquidacionDto.numMatricula;
          liquidacionDto.NumNotaCredito = _param1.liquidacionDto.numNotaCredito;
          liquidacionDto.NumRecibo = _param1.liquidacionDto.numRecibo;
          liquidacionDto.NumTramite = _param1.liquidacionDto.numTramite;
          liquidacionDto.ValorActivoTotal = !_param1.liquidacionDto.valorActivoTotal.HasValue ? Decimal.Zero : (Decimal) _param1.liquidacionDto.valorActivoTotal.Value;
          double? nullable = _param1.liquidacionDto.valorBase;
          Decimal zero1;
          if (nullable.HasValue)
          {
            nullable = _param1.liquidacionDto.valorBase;
            zero1 = (Decimal) nullable.Value;
          }
          else
            zero1 = Decimal.Zero;
          liquidacionDto.ValorBase = zero1;
          nullable = _param1.liquidacionDto.valorServicio;
          Decimal zero2;
          if (nullable.HasValue)
          {
            nullable = _param1.liquidacionDto.valorServicio;
            zero2 = (Decimal) nullable.Value;
          }
          else
            zero2 = Decimal.Zero;
          liquidacionDto.ValorServicio = zero2;
          return liquidacionDto;
        }).ToList<LiquidacionDto>()
      };
    }
  }
}

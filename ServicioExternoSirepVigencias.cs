﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.ServicioExternoSirepVigencias
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using AgentesExternos.Sirepx.Dto;
using AgentesExternos.Sirepx.Soap.Nucleo;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioVigencia;
using System;
using System.Linq;

namespace AgentesExternos.Sirepx.Soap
{
  public class ServicioExternoSirepVigencias : IServicioExternoSirepVigencias
  {
    public FechaVigenciaDto ObtenerFechaVigencia(ObtenerFechaVigenciaDTO dto)
    {
      obtenerFechaVigenciaResponse vigenciaResponse = ClienteSoap.ObtenerClienteVigencia().obtenerFechaVigenciaOrdenPago(new obtenerFechaVigenciaRequest()
      {
        actos = dto.Actos.Select<AgentesExternos.Sirepx.Dto.ActoDTO, AgentesExternos.Sirepx.Soap.Proxies.ServicioVigencia.ActoDTO>((Func<AgentesExternos.Sirepx.Dto.ActoDTO, AgentesExternos.Sirepx.Soap.Proxies.ServicioVigencia.ActoDTO>) (actoVigenciaDto => new AgentesExternos.Sirepx.Soap.Proxies.ServicioVigencia.ActoDTO()
        {
          idActo = actoVigenciaDto.ActoId,
          idLibro = actoVigenciaDto.LibroId,
          idImpuesto = actoVigenciaDto.ImpuestoId
        })).ToArray<AgentesExternos.Sirepx.Soap.Proxies.ServicioVigencia.ActoDTO>(),
        ctrEspecial = dto.CtrEspecial.ToString(),
        fecDocumento = dto.FechaDocumento.ToString("yyyy-MM-dd"),
        idOrigenDocumento = dto.IdOrigenDocumento,
        idServicioNegocio = dto.IdServicioNegocio
      });
      if (vigenciaResponse == null)
        return (FechaVigenciaDto) null;
      if (!string.IsNullOrEmpty(vigenciaResponse.mensajeError))
        return new FechaVigenciaDto()
        {
          CodigoError = vigenciaResponse.codigoError,
          MensajeError = vigenciaResponse.mensajeError
        };
      return new FechaVigenciaDto()
      {
        FechaVigencia = Convert.ToDateTime(vigenciaResponse.fecVigencia)
      };
    }
  }
}

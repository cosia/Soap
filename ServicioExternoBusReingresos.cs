﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.ServicioExternoBusReingresos
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using AgentesExternos.Sirepx.Dto;
using AgentesExternos.Sirepx.Soap.Nucleo;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioReingresos;
using System;

namespace AgentesExternos.Sirepx.Soap
{
  public class ServicioExternoBusReingresos : IServicioExternoBusReingresos
  {
    public RegistroPagoVirtualDto RegistrarPago(RegistrarPagoReingresosDto registrarPagoReingresosDto)
    {
      registrarPagoReingresoType registrarPagoReingreso1 = new registrarPagoReingresoType()
      {
        data = new dataReqReingresoType()
        {
          registrarPagoInDTO = new registrarPagoInDTOReingresoType()
          {
            formaPago = new formaPagoReingresoType()
            {
              numAutoriza = registrarPagoReingresosDto.NumeroAutorizacion,
              idFormaPago = registrarPagoReingresosDto.FormaPagoId,
              idMoneda = registrarPagoReingresosDto.MonedaId,
              numDocumento = registrarPagoReingresosDto.NumeroDocumento,
              idTipoTarjeta = registrarPagoReingresosDto.TipoTarjetaId,
              idBanco = registrarPagoReingresosDto.BancoId ?? string.Empty,
              valorPago = Convert.ToDecimal(registrarPagoReingresosDto.ValorPago)
            },
            numOrdenPago = registrarPagoReingresosDto.OrdenPago,
            numClientePago = Convert.ToInt32(registrarPagoReingresosDto.NumeroClientePago),
            numClientePagoSpecified = true,
            fecRecaudo = registrarPagoReingresosDto.FechaRecuado,
            numSolicitud = registrarPagoReingresosDto.NumeroSolicitud
          }
        },
        metadata = new metadataReingresoType()
      };
      if (!string.IsNullOrEmpty(registrarPagoReingresosDto.NumeroRecibo))
        registrarPagoReingreso1.data.registrarPagoInDTO.numRecibo = registrarPagoReingresosDto.NumeroRecibo;
      registrarPagoReingresoResponseType reingresoResponseType = ClienteSoap.ObtenerClienteReingresos().registrarPagoReingreso(registrarPagoReingreso1);
      return new RegistroPagoVirtualDto()
      {
        CodigoError = reingresoResponseType.data.registrarPagoOutDTO.resultado.codigoError,
        DescripcionError = reingresoResponseType.data.registrarPagoOutDTO.resultado.mensajeError
      };
    }
  }
}

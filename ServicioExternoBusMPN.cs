﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.ServicioExternoBusMPN
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using AgentesExternos.Sirepx.Dto;
using AgentesExternos.Sirepx.Soap.Nucleo;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioMPN;
using System;

namespace AgentesExternos.Sirepx.Soap
{
  public class ServicioExternoBusMPN : IServicioExternoBusMPN
  {
    public RegistroPagoVirtualDto RegistrarPago(RegistrarPagoMPNDto registrarPagoMpnDto)
    {
      registrarPagoMPNType registrarPagoMPN1 = new registrarPagoMPNType()
      {
        data = new dataReqMPNType()
        {
          registrarPagoInDTO = new registrarPagoInDTOMPNType()
          {
            numClientePago = Convert.ToInt32(registrarPagoMpnDto.NumeroClientePago),
            numClientePagoSpecified = true,
            fecRecaudo = registrarPagoMpnDto.FechaRecuado,
            numSolicitud = registrarPagoMpnDto.NumeroSolicitud,
            numOrdenPago = registrarPagoMpnDto.OrdenPago,
            formaPago = new formaPagoMPNType()
            {
              idMoneda = registrarPagoMpnDto.MonedaId,
              numDocumento = registrarPagoMpnDto.NumeroDocumento,
              idTipoTarjeta = registrarPagoMpnDto.TipoTarjetaId,
              idBanco = registrarPagoMpnDto.BancoId ?? string.Empty,
              numAutoriza = registrarPagoMpnDto.NumeroAutorizacion,
              idFormaPago = registrarPagoMpnDto.FormaPagoId,
              valorPago = Convert.ToDecimal(registrarPagoMpnDto.ValorPago)
            }
          }
        },
        metadata = new metadataMPNType()
      };
      if (!string.IsNullOrEmpty(registrarPagoMpnDto.NumeroRecibo))
        registrarPagoMPN1.data.registrarPagoInDTO.numRecibo = registrarPagoMpnDto.NumeroRecibo;
      registrarPagoMPNResponseType pagoMpnResponseType = ClienteSoap.ObtenerClienteMPN().registrarPagoMPN(registrarPagoMPN1);
      return new RegistroPagoVirtualDto()
      {
        CodigoError = pagoMpnResponseType.data.registrarPagoOutDTO.resultado.codigoError,
        DescripcionError = pagoMpnResponseType.data.registrarPagoOutDTO.resultado.mensajeError
      };
    }
  }
}

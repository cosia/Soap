﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.ServicioExternoBusCM
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using AgentesExternos.Sirepx.Dto;
using AgentesExternos.Sirepx.Soap.Nucleo;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioCM;
using System;

namespace AgentesExternos.Sirepx.Soap
{
  public class ServicioExternoBusCM : IServicioExternoBusCM
  {
    public RegistroPagoVirtualDto RegistrarPago(RegistrarPagoCMDto registrarPagoCMDto)
    {
      registrarPagoCMType registrarPagoCM1 = new registrarPagoCMType()
      {
        data = new dataReqCMType()
        {
          registrarPagoInDTO = new registrarPagoInDTOCMType()
          {
            numOrdenPago = registrarPagoCMDto.OrdenPago,
            fecRecaudo = registrarPagoCMDto.FechaRecuado,
            numSolicitud = registrarPagoCMDto.NumeroSolicitud,
            numClientePago = Convert.ToInt32(registrarPagoCMDto.NumeroClientePago),
            numClientePagoSpecified = true,
            formaPago = new formaPagoCMType()
            {
              numDocumento = registrarPagoCMDto.NumeroDocumento,
              idTipoTarjeta = registrarPagoCMDto.TipoTarjetaId,
              idBanco = registrarPagoCMDto.BancoId ?? string.Empty,
              numAutoriza = registrarPagoCMDto.NumeroAutorizacion,
              idFormaPago = registrarPagoCMDto.FormaPagoId,
              idMoneda = registrarPagoCMDto.MonedaId,
              valorPago = Convert.ToDecimal(registrarPagoCMDto.ValorPago)
            }
          }
        },
        metadata = new metadataCMType()
      };
      if (!string.IsNullOrEmpty(registrarPagoCMDto.NumeroRecibo))
        registrarPagoCM1.data.registrarPagoInDTO.numRecibo = registrarPagoCMDto.NumeroRecibo;
      registrarPagoCMResponseType pagoCmResponseType = ClienteSoap.ObtenerClienteCM().registrarPagoCM(registrarPagoCM1);
      return new RegistroPagoVirtualDto()
      {
        CodigoError = pagoCmResponseType.data.registrarPagoOutDTO.resultado.codigoError,
        DescripcionError = pagoCmResponseType.data.registrarPagoOutDTO.resultado.mensajeError
      };
    }
  }
}

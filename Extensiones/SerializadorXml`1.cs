﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Extensiones.SerializadorXml`1
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Soap.Extensiones
{
  public class SerializadorXml<T> where T : class, new()
  {
    public static T Deserializar(string xml)
    {
      return SerializadorXml<T>.Deserializar(xml, Encoding.UTF8, (XmlReaderSettings) null);
    }

    public static T Deserializar(string xml, XmlReaderSettings configuraciones)
    {
      return SerializadorXml<T>.Deserializar(xml, Encoding.UTF8, configuraciones);
    }

    public static T Deserializar(string xml, Encoding codificacion, XmlReaderSettings configuraciones = null)
    {
      if (string.IsNullOrEmpty(xml))
        throw new ArgumentException("XML no puede ser nulo o vacío.", nameof (xml));
      using (MemoryStream memoryStream = new MemoryStream(codificacion.GetBytes(xml)))
        return (T) new XmlSerializer(typeof (T)).Deserialize(XmlReader.Create((Stream) memoryStream, configuraciones));
    }

    public static T DeserializarDesdeArchivo(string nombreArchivo)
    {
      return SerializadorXml<T>.DeserializarDesdeArchivo(nombreArchivo, new XmlReaderSettings());
    }

    public static T DeserializarDesdeArchivo(string nombreArchivo, XmlReaderSettings configuraciones)
    {
      if (string.IsNullOrEmpty(nombreArchivo))
        throw new ArgumentException("El archivo XML no puede ser nulo o vacío.", nameof (nombreArchivo));
      if (!File.Exists(nombreArchivo))
        throw new FileNotFoundException("No se ha encontrado el archivo XML a deserializar.", nombreArchivo);
      using (XmlReader xmlReader = XmlReader.Create(nombreArchivo, configuraciones))
        return (T) new XmlSerializer(typeof (T)).Deserialize(xmlReader);
    }

    public static string FormatXml(string sUnformattedXml)
    {
      XmlDocument xmlDocument = new XmlDocument();
      xmlDocument.LoadXml(sUnformattedXml);
      StringBuilder sb = new StringBuilder();
      StringWriter stringWriter = new StringWriter(sb);
      XmlTextWriter xmlTextWriter = (XmlTextWriter) null;
      try
      {
        xmlTextWriter = new XmlTextWriter((TextWriter) stringWriter)
        {
          Formatting = Formatting.Indented
        };
        xmlDocument.WriteTo((XmlWriter) xmlTextWriter);
      }
      finally
      {
        if (xmlTextWriter != null)
          xmlTextWriter.Close();
      }
      return sb.ToString();
    }

    public static string Serializar(T origen)
    {
      return SerializadorXml<T>.Serializar(origen, (XmlSerializerNamespaces) null, SerializadorXml<T>.ObtenerConfiguraciones());
    }

    public static string Serializar(T origen, XmlSerializerNamespaces espaciosDeNombre)
    {
      return SerializadorXml<T>.Serializar(origen, espaciosDeNombre, SerializadorXml<T>.ObtenerConfiguraciones());
    }

    public static string Serializar(T origen, XmlWriterSettings configuraciones)
    {
      return SerializadorXml<T>.Serializar(origen, (XmlSerializerNamespaces) null, configuraciones);
    }

    public static string Serializar(T origen, XmlSerializerNamespaces espaciosDeNombre, XmlWriterSettings configuraciones)
    {
      if ((object) origen == null)
        throw new ArgumentNullException(nameof (origen), "El objeto a serializar no puede ser nulo.");
      XmlSerializer xmlSerializer = new XmlSerializer(origen.GetType());
      using (MemoryStream memoryStream = new MemoryStream())
      {
        using (XmlWriter xmlWriter = XmlWriter.Create((Stream) memoryStream, configuraciones))
        {
          new XmlSerializer(typeof (T)).Serialize(xmlWriter, (object) origen, espaciosDeNombre);
          memoryStream.Position = 0L;
          return new StreamReader((Stream) memoryStream).ReadToEnd();
        }
      }
    }

    public static void SerializarHaciaArchivo(T origen, string nombreArchivo)
    {
      SerializadorXml<T>.SerializarHaciaArchivo(origen, nombreArchivo, (XmlSerializerNamespaces) null, SerializadorXml<T>.ObtenerConfiguraciones());
    }

    public static void SerializarHaciaArchivo(T origen, string nombreArchivo, XmlSerializerNamespaces espaciosDeNombre)
    {
      SerializadorXml<T>.SerializarHaciaArchivo(origen, nombreArchivo, espaciosDeNombre, SerializadorXml<T>.ObtenerConfiguraciones());
    }

    public static void SerializarHaciaArchivo(T origen, string nombreArchivo, XmlWriterSettings configuraciones)
    {
      SerializadorXml<T>.SerializarHaciaArchivo(origen, nombreArchivo, (XmlSerializerNamespaces) null, configuraciones);
    }

    public static void SerializarHaciaArchivo(T origen, string filename, XmlSerializerNamespaces espaciosDeNombre, XmlWriterSettings configuraciones)
    {
      if ((object) origen == null)
        throw new ArgumentNullException(nameof (origen), "El objeto a serializar no puede ser nulo.");
      XmlSerializer xmlSerializer = new XmlSerializer(origen.GetType());
      using (XmlWriter xmlWriter = XmlWriter.Create(filename, configuraciones))
        new XmlSerializer(typeof (T)).Serialize(xmlWriter, (object) origen, espaciosDeNombre);
    }

    private static XmlWriterSettings ObtenerConfiguraciones()
    {
      return new XmlWriterSettings()
      {
        Indent = true,
        IndentChars = "\t"
      };
    }
  }
}

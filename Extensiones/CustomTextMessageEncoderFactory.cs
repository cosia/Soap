﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Extensiones.CustomTextMessageEncoderFactory
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System.ServiceModel.Channels;

namespace AgentesExternos.Sirepx.Soap.Extensiones
{
  public class CustomTextMessageEncoderFactory : MessageEncoderFactory
  {
    private MessageEncoder encoder;
    private MessageVersion version;
    private string mediaType;
    private string charSet;

    internal CustomTextMessageEncoderFactory(string mediaType, string charSet, MessageVersion version)
    {
      this.version = version;
      this.mediaType = mediaType;
      this.charSet = charSet;
      this.encoder = (MessageEncoder) new CustomTextMessageEncoder(this);
    }

    public override MessageEncoder Encoder
    {
      get
      {
        return this.encoder;
      }
    }

    public override MessageVersion MessageVersion
    {
      get
      {
        return this.version;
      }
    }

    internal string MediaType
    {
      get
      {
        return this.mediaType;
      }
    }

    internal string CharSet
    {
      get
      {
        return this.charSet;
      }
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Extensiones.CustomTextMessageBindingElement
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Xml;

namespace AgentesExternos.Sirepx.Soap.Extensiones
{
  public class CustomTextMessageBindingElement : MessageEncodingBindingElement, IWsdlExportExtension
  {
    private MessageVersion msgVersion;
    private string mediaType;
    private string encoding;
    private XmlDictionaryReaderQuotas readerQuotas;

    private CustomTextMessageBindingElement(CustomTextMessageBindingElement binding)
      : this(binding.Encoding, binding.MediaType, binding.MessageVersion)
    {
      this.readerQuotas = new XmlDictionaryReaderQuotas();
      binding.ReaderQuotas.CopyTo(this.readerQuotas);
    }

    public CustomTextMessageBindingElement(string encoding, string mediaType, MessageVersion msgVersion)
    {
      if (encoding == null)
        throw new ArgumentNullException(nameof (encoding));
      if (mediaType == null)
        throw new ArgumentNullException(nameof (mediaType));
      if (msgVersion == null)
        throw new ArgumentNullException(nameof (msgVersion));
      this.msgVersion = msgVersion;
      this.mediaType = mediaType;
      this.encoding = encoding;
      this.readerQuotas = new XmlDictionaryReaderQuotas();
    }

    public CustomTextMessageBindingElement(string encoding, string mediaType)
      : this(encoding, mediaType, MessageVersion.Soap11WSAddressing10)
    {
    }

    public CustomTextMessageBindingElement(string encoding)
      : this(encoding, "text/xml")
    {
    }

    public CustomTextMessageBindingElement()
      : this("UTF-8")
    {
    }

    public override MessageVersion MessageVersion
    {
      get
      {
        return this.msgVersion;
      }
      set
      {
        if (value == null)
          throw new ArgumentNullException(nameof (value));
        this.msgVersion = value;
      }
    }

    public string MediaType
    {
      get
      {
        return this.mediaType;
      }
      set
      {
        if (value == null)
          throw new ArgumentNullException(nameof (value));
        this.mediaType = value;
      }
    }

    public string Encoding
    {
      get
      {
        return this.encoding;
      }
      set
      {
        if (value == null)
          throw new ArgumentNullException(nameof (value));
        this.encoding = value;
      }
    }

    public XmlDictionaryReaderQuotas ReaderQuotas
    {
      get
      {
        return this.readerQuotas;
      }
    }

    public override MessageEncoderFactory CreateMessageEncoderFactory()
    {
      return (MessageEncoderFactory) new CustomTextMessageEncoderFactory(this.MediaType, this.Encoding, this.MessageVersion);
    }

    public override BindingElement Clone()
    {
      return (BindingElement) new CustomTextMessageBindingElement(this);
    }

    public override IChannelFactory<TChannel> BuildChannelFactory<TChannel>(BindingContext context)
    {
      if (context == null)
        throw new ArgumentNullException(nameof (context));
      context.BindingParameters.Add((object) this);
      return context.BuildInnerChannelFactory<TChannel>();
    }

    public override bool CanBuildChannelFactory<TChannel>(BindingContext context)
    {
      if (context == null)
        throw new ArgumentNullException(nameof (context));
      return context.CanBuildInnerChannelFactory<TChannel>();
    }

    public override IChannelListener<TChannel> BuildChannelListener<TChannel>(BindingContext context)
    {
      if (context == null)
        throw new ArgumentNullException(nameof (context));
      context.BindingParameters.Add((object) this);
      return context.BuildInnerChannelListener<TChannel>();
    }

    public override bool CanBuildChannelListener<TChannel>(BindingContext context)
    {
      if (context == null)
        throw new ArgumentNullException(nameof (context));
      context.BindingParameters.Add((object) this);
      return context.CanBuildInnerChannelListener<TChannel>();
    }

    public override T GetProperty<T>(BindingContext context)
    {
      if (typeof (T) == typeof (XmlDictionaryReaderQuotas))
        return (T) this.readerQuotas;
      return base.GetProperty<T>(context);
    }

    void IWsdlExportExtension.ExportContract(WsdlExporter exporter, WsdlContractConversionContext context)
    {
    }

    void IWsdlExportExtension.ExportEndpoint(WsdlExporter exporter, WsdlEndpointConversionContext context)
    {
      TextMessageEncodingBindingElement encodingBindingElement = new TextMessageEncodingBindingElement();
      encodingBindingElement.MessageVersion = this.msgVersion;
      ((IWsdlExportExtension) encodingBindingElement).ExportEndpoint(exporter, context);
    }
  }
}

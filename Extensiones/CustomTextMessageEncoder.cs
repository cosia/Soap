﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.Extensiones.CustomTextMessageEncoder
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using System;
using System.IO;
using System.ServiceModel.Channels;
using System.Text;
using System.Xml;

namespace AgentesExternos.Sirepx.Soap.Extensiones
{
  public class CustomTextMessageEncoder : MessageEncoder
  {
    private CustomTextMessageEncoderFactory factory;
    private XmlWriterSettings writerSettings;
    private string contentType;

    public CustomTextMessageEncoder(CustomTextMessageEncoderFactory factory)
    {
      this.factory = factory;
      this.writerSettings = new XmlWriterSettings();
      this.writerSettings.Encoding = Encoding.GetEncoding(factory.CharSet);
      this.contentType = string.Format("{0}; charset={1}", (object) this.factory.MediaType, (object) this.writerSettings.Encoding.HeaderName);
    }

    public override string ContentType
    {
      get
      {
        return this.contentType;
      }
    }

    public override string MediaType
    {
      get
      {
        return this.factory.MediaType;
      }
    }

    public override MessageVersion MessageVersion
    {
      get
      {
        return this.factory.MessageVersion;
      }
    }

    public override Message ReadMessage(ArraySegment<byte> buffer, BufferManager bufferManager, string contentType)
    {
      byte[] buffer1 = new byte[buffer.Count];
      Array.Copy((Array) buffer.Array, buffer.Offset, (Array) buffer1, 0, buffer1.Length);
      bufferManager.ReturnBuffer(buffer.Array);
      return this.ReadMessage((Stream) new MemoryStream(buffer1), int.MaxValue);
    }

    public override Message ReadMessage(Stream stream, int maxSizeOfHeaders, string contentType)
    {
      return Message.CreateMessage(XmlReader.Create(stream), maxSizeOfHeaders, this.MessageVersion);
    }

    public override ArraySegment<byte> WriteMessage(Message message, int maxMessageSize, BufferManager bufferManager, int messageOffset)
    {
      MemoryStream memoryStream = new MemoryStream();
      XmlWriter writer = XmlWriter.Create((Stream) memoryStream, this.writerSettings);
      message.WriteMessage(writer);
      writer.Close();
      byte[] buffer1 = memoryStream.GetBuffer();
      int position = (int) memoryStream.Position;
      memoryStream.Close();
      int bufferSize = position + messageOffset;
      byte[] buffer2 = bufferManager.TakeBuffer(bufferSize);
      Array.Copy((Array) buffer1, 0, (Array) buffer2, messageOffset, position);
      return new ArraySegment<byte>(buffer2, messageOffset, position);
    }

    public override void WriteMessage(Message message, Stream stream)
    {
      XmlWriter writer = XmlWriter.Create(stream, this.writerSettings);
      message.WriteMessage(writer);
      writer.Close();
    }

    public override bool IsContentTypeSupported(string contentType)
    {
      if (base.IsContentTypeSupported(contentType))
        return true;
      if (contentType.Length == this.MediaType.Length)
        return contentType.Equals(this.MediaType, StringComparison.OrdinalIgnoreCase);
      return contentType.StartsWith(this.MediaType, StringComparison.OrdinalIgnoreCase) && (int) contentType[this.MediaType.Length] == 59;
    }
  }
}

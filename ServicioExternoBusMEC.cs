﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.ServicioExternoBusMEC
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using AgentesExternos.Sirepx.Dto;
using AgentesExternos.Sirepx.Soap.Nucleo;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioMEC;
using System;

namespace AgentesExternos.Sirepx.Soap
{
  public class ServicioExternoBusMEC : IServicioExternoBusMEC
  {
    public RegistroPagoVirtualDto RegistrarPago(RegistrarPagoMECDto registrarPagoMecDto)
    {
      registrarPagoMECType registrarPagoMEC1 = new registrarPagoMECType()
      {
        data = new dataReqMECType()
        {
          registrarPagoInDTO = new registrarPagoInDTOMECType()
          {
            numOrdenPago = registrarPagoMecDto.OrdenPago,
            numClientePago = Convert.ToInt32(registrarPagoMecDto.NumeroClientePago),
            numClientePagoSpecified = true,
            fecRecaudo = registrarPagoMecDto.FechaRecuado,
            numSolicitud = registrarPagoMecDto.NumeroSolicitud,
            formaPago = new formaPagoMECType()
            {
              idFormaPago = registrarPagoMecDto.FormaPagoId,
              idMoneda = registrarPagoMecDto.MonedaId,
              numDocumento = registrarPagoMecDto.NumeroDocumento,
              idTipoTarjeta = registrarPagoMecDto.TipoTarjetaId,
              idBanco = registrarPagoMecDto.BancoId ?? string.Empty,
              numAutoriza = registrarPagoMecDto.NumeroAutorizacion,
              valorPago = Convert.ToDecimal(registrarPagoMecDto.ValorPago)
            }
          }
        },
        metadata = new metadataMECType()
      };
      if (!string.IsNullOrEmpty(registrarPagoMecDto.NumeroRecibo))
        registrarPagoMEC1.data.registrarPagoInDTO.numRecibo = registrarPagoMecDto.NumeroRecibo;
      registrarPagoMECResponseType pagoMecResponseType = ClienteSoap.ObtenerClienteMEC().registrarPagoMEC(registrarPagoMEC1);
      return new RegistroPagoVirtualDto()
      {
        CodigoError = pagoMecResponseType.data.registrarPagoOutDTO.resultado.codigoError,
        DescripcionError = pagoMecResponseType.data.registrarPagoOutDTO.resultado.mensajeError
      };
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.ServicioExternoSirepRecaudos
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using AgentesExternos.Sirepx.Dto;
using AgentesExternos.Sirepx.Soap.Nucleo;
using AgentesExternos.Sirepx.Soap.Proxies.ServicioRecaudo;
using System;
using System.Globalization;
using System.Linq;

namespace AgentesExternos.Sirepx.Soap
{
  public class ServicioExternoSirepRecaudos : IServicioExternoSirepRecaudos
  {
    public PagoVirtualDto GenerarOrdenDePago(GenerarOrdenPagoDto generarOrdenPagoDto)
    {
      GenerarOrdenPagoDTO generarOrdenPagoDTO = new GenerarOrdenPagoDTO()
      {
        fecVencimiento = generarOrdenPagoDto.FechaVencimiento,
        idServicioNegocio = generarOrdenPagoDto.ServicioNegocioId,
        idSucursal = generarOrdenPagoDto.SucursalId,
        idUsuario = generarOrdenPagoDto.UsuarioId,
        numClientePago = generarOrdenPagoDto.NumeroClientePago,
        idSolicitudBPM = generarOrdenPagoDto.SolicitudBpmId
      };
      generarOrdenPagoDTO.datosLiquidacion = new LiquidarOutDTO()
      {
        descMoneda = generarOrdenPagoDto.DatosLiquidacion.DescMoneda,
        idMoneda = generarOrdenPagoDto.DatosLiquidacion.IdMoneda,
        totalOperacion = new double?((double) generarOrdenPagoDto.DatosLiquidacion.TotalOperacion),
        liquidacion = generarOrdenPagoDto.DatosLiquidacion.Liquidacion.Select<LiquidacionDto, LiquidacionDTO>((Func<LiquidacionDto, LiquidacionDTO>) (liquidacionDTO => new LiquidacionDTO()
        {
          anoRenova = liquidacionDTO.AnoRenova,
          cantidad = new int?(liquidacionDTO.Cantidad),
          cntPersonal = liquidacionDTO.CntPersonal,
          idCertificado = liquidacionDTO.IdCertificado,
          fechaBase = liquidacionDTO.FechaBase,
          fechaEspecial = liquidacionDTO.FechaEspecial,
          gastoAdministrativo = liquidacionDTO.GastoAdministrativo.ToString((IFormatProvider) CultureInfo.InvariantCulture),
          idActo = liquidacionDTO.IdActo,
          idDescuento = liquidacionDTO.IdDescuento,
          idFondo = liquidacionDTO.IdFondo,
          idGastoAdmin = liquidacionDTO.IdGastoAdmin,
          idLibro = liquidacionDTO.IdLibro,
          idServicio = liquidacionDTO.IdServicio,
          idServicioBase = liquidacionDTO.IdServicioBase,
          idServicioPadre = liquidacionDTO.IdServicioPadre,
          idTipoVenta = liquidacionDTO.IdTipoVenta,
          item = new int?(liquidacionDTO.Item),
          itemPadre = new int?(liquidacionDTO.ItemPadre),
          nombreMatricula = liquidacionDTO.NombreMatricula,
          nombreServicio = liquidacionDTO.NombreServicio,
          numMatricula = liquidacionDTO.NumMatricula,
          numNotaCredito = liquidacionDTO.NumNotaCredito,
          numRecibo = liquidacionDTO.NumRecibo,
          numTramite = liquidacionDTO.NumTramite,
          valorActivoTotal = new double?((double) liquidacionDTO.ValorActivoTotal),
          valorBase = new double?((double) liquidacionDTO.ValorBase),
          valorServicio = new double?((double) liquidacionDTO.ValorServicio)
        })).ToArray<LiquidacionDTO>()
      };
      GenerarOrdenPagoResponseDTO ordenPagoResponseDto = ClienteSoap.ObtenerCliente().registrarOrdenPago(generarOrdenPagoDTO);
      if (ordenPagoResponseDto == null)
        return (PagoVirtualDto) null;
      if (ordenPagoResponseDto.codigoError == "0000")
        return new PagoVirtualDto()
        {
          Numero = ordenPagoResponseDto.codigo
        };
      if (!string.IsNullOrEmpty(ordenPagoResponseDto.mensajeError))
        return new PagoVirtualDto()
        {
          MensajeError = ordenPagoResponseDto.mensajeError
        };
      return new PagoVirtualDto()
      {
        Numero = ordenPagoResponseDto.codigo
      };
    }
  }
}

﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Soap.ServicioExternoSirepClientes
// Assembly: AgentesExternos.Sirepx.Soap, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 584729D1-41AB-4E74-A6C2-DAE600EB3DE5
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.Soap.dll

using AgentesExternos.Sirepx.Dto;
using AgentesExternos.Sirepx.Soap.Nucleo;
using AgentesExternos.Sirepx.Soap.Proxies.Notarias;
using System.Collections.Generic;
using System.Linq;

namespace AgentesExternos.Sirepx.Soap
{
  public class ServicioExternoSirepClientes : IServicioExternoSirepClientes
  {
    public string ConsultarCliente(DatosBasicosClienteDto datosBasicosClienteDto)
    {
      DatosBasicosClienteDTO basicosClienteDto = ClienteSoap.ObtenerClienteNotarias().consultarDatosBasicosCliente(new ConsultarDatosBasicosClienteInDTO()
      {
        idClase = datosBasicosClienteDto.TipoIdentificacion,
        numId = datosBasicosClienteDto.NumeroIdentificacion
      });
      return (basicosClienteDto != null ? basicosClienteDto.numCliente.ToString() : (string) null) ?? string.Empty;
    }

    public ClienteDto ConsultarClientePorId(DatosBasicosClienteDto datosBasicosClienteDto)
    {
      ClienteDto clienteDto = new ClienteDto();
      DatosBasicosClienteDTO basicosClienteDto = ClienteSoap.ObtenerClienteNotarias().consultarDatosBasicosCliente(new ConsultarDatosBasicosClienteInDTO()
      {
        numCliente = datosBasicosClienteDto.NumeroCliente
      });
      if (basicosClienteDto == null)
        return clienteDto;
      clienteDto.TipoIdentificacion = int.Parse(basicosClienteDto.idClase);
      clienteDto.Identificacion = basicosClienteDto.numId;
      clienteDto.Nombres = string.Format("{0} {1}", (object) basicosClienteDto.primerNombre, (object) basicosClienteDto.segundoNombre).Trim();
      clienteDto.Apellidos = string.Format("{0} {1}", (object) basicosClienteDto.primerApellido, (object) basicosClienteDto.segundoApellido).Trim();
      clienteDto.CiudadId = basicosClienteDto.direccion != null ? basicosClienteDto.direccion.idMunip : string.Empty;
      clienteDto.Ciudad = ((IEnumerable<DireccionVO>) basicosClienteDto.direcciones).Any<DireccionVO>() ? basicosClienteDto.direcciones[0].descIdMunip : string.Empty;
      clienteDto.Direccion = basicosClienteDto.direccion != null ? basicosClienteDto.direccion.direccion : string.Empty;
      clienteDto.Telefono = basicosClienteDto.direccion != null ? basicosClienteDto.direccion.numTel1 : string.Empty;
      clienteDto.Correo = basicosClienteDto.direccion != null ? basicosClienteDto.direccion.email : string.Empty;
      return clienteDto;
    }
  }
}
